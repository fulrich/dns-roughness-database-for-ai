import pandas as pd
import tensorflow as tf
import tensorflow.keras as keras
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
import os

import numpy as np
import tensorflow as tf
from tensorflow import keras

# Display
from IPython.display import Image, display
import matplotlib.pyplot as plt
import matplotlib.cm as cm



from DatabaseGeneration.DatabaseGenerator import getTrainingsDataHeightsDatabase

#Code adapted form https://keras.io/examples/vision/grad_cam/

def get_img_array(img_path, size):
    # `img` is a PIL image of size 299x299
    img = keras.preprocessing.image.load_img(img_path, target_size=size)
    # `array` is a float32 Numpy array of shape (299, 299, 3)
    array = keras.preprocessing.image.img_to_array(img)
    # We add a dimension to transform our array into a "batch"
    # of size (1, 299, 299, 3)
    array = np.expand_dims(array, axis=0)
    return array


def make_gradcam_heatmap(img_array, model, last_conv_layer_name, pred_index=None):
    # First, we create a model that maps the input image to the activations
    # of the last conv layer as well as the output predictions
    grad_model = tf.keras.models.Model(
        [model.inputs], [model.get_layer(last_conv_layer_name).output, model.output]
    )

    # Then, we compute the gradient of the top predicted class for our input image
    # with respect to the activations of the last conv layer
    with tf.GradientTape() as tape:
        last_conv_layer_output, preds = grad_model(np.array([img_array]))
        if pred_index is None:
            pred_index = tf.argmax(preds[0])
        class_channel = preds[:, pred_index]

    # This is the gradient of the output neuron (top predicted or chosen)
    # with regard to the output feature map of the last conv layer
    grads = tape.gradient(class_channel, last_conv_layer_output)

    # This is a vector where each entry is the mean intensity of the gradient
    # over a specific feature map channel
    pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

    # We multiply each channel in the feature map array
    # by "how important this channel is" with regard to the top predicted class
    # then sum all the channels to obtain the heatmap class activation
    last_conv_layer_output = last_conv_layer_output[0]
    heatmap = last_conv_layer_output @ pooled_grads[..., tf.newaxis]
    heatmap = tf.squeeze(heatmap)

    # For visualization purpose, we will also normalize the heatmap between 0 & 1
    #heatmap = tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)
    #return heatmap

    numpyArray = heatmap.numpy()
    scaler = MinMaxScaler()
    normalizedArray = scaler.fit_transform(np.squeeze(numpyArray))
    transformedAsImage = normalizedArray.reshape((3,3, 1))


    return transformedAsImage

def save_and_display_gradcam(img_path, heatmap, cam_path="cam.jpg", alpha=0.4):
    # Load the original image
    img = keras.preprocessing.image.load_img(img_path)
    img = keras.preprocessing.image.img_to_array(img)

    # Rescale heatmap to a range 0-255
    heatmap = np.uint8(255 * heatmap)

    # Use jet colormap to colorize heatmap
    jet = cm.get_cmap("jet")

    # Use RGB values of the colormap
    jet_colors = jet(np.arange(256))[:, :3]
    jet_heatmap = jet_colors[heatmap]

    # Create an image with RGB colorized heatmap
    jet_heatmap = keras.preprocessing.image.array_to_img(jet_heatmap)
    jet_heatmap = jet_heatmap.resize((img.shape[1], img.shape[0]))
    jet_heatmap = keras.preprocessing.image.img_to_array(jet_heatmap)

    # Superimpose the heatmap on original image
    superimposed_img = jet_heatmap * alpha + img
    superimposed_img = keras.preprocessing.image.array_to_img(superimposed_img)

    # Save the superimposed image
    superimposed_img.save(cam_path)

    # Display Grad CAM
    display(Image(cam_path))


#save_and_display_gradcam(img_path, heatmap)


os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

domainResolution = 16

modelName = "TransformedData"
pathToHeightsFile = "/home/fulrich/Promotion/CEAS-Journal/vorticity_prediction-main/CNN_Training_Fritz//Heights_16_16.csv"
pathToMaxVorticityFile = "/home/fulrich/Promotion/CEAS-Journal/vorticity_prediction-main/CNN_Training_Fritz//1_6_0_All.csv"
xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution)
model = tf.keras.models.load_model("../Models/HeightModels/" + "BestConvModel4")

for i, layer in enumerate(model.layers):
    layer._name = 'layer_' + str(i)


heatmap = make_gradcam_heatmap(xTrain[8], model, "layer_6")

# Display heatmap
plt.matshow(heatmap)
plt.show()







