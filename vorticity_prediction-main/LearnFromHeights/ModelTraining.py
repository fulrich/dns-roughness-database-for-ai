import sys
sys.path.insert(0,"../../vorticity_prediction-main/")
from concurrent.futures import ThreadPoolExecutor

import pandas as pd
import tensorflow as tf
import numpy as np
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
import os
from PIL import Image
# How to use this program and what it is about:
# The code trains a neuronal network to predict the maximum vorticity which
# Therefore we need a resolution with which the grid is resolved
from DataVisualization.DataSetPlot import saveRoughnessXdataToImage
from DataVisualization.Modelperformance import plotModelPerformanceInPercent, plot_Errors3
from DatabaseGeneration.DatabaseGenerator import getTrainingsDataHeightsDatabase, coverHeightAreaWithMinusOne, \
    getTrainingDataDNN, saveDataSetAsNumpyArrs, loadNumpyDataSet, getTestDataHeightsDatabase
from DatabaseGeneration.RoughnessTransformer import transformWholeDataSet
from GlobalCode.ImportantGlobalPaths import pathToDataFolder
from LearnFromHeights.UsedCNNs import createConvModelBest
from LearnFromParameters.ModelEvaluation import evaluatePredictions

from tensorflow.keras.utils import plot_model
from LearnFromParameters.TrainModule import trainModel
epochs = 40
# The resolution of the quadratic grid
domainResolution = 32
# If we use a convolutional network the data has to be reshaped
useConvolutionalModel = True

def readHeights(pathToHeightsFile):
    dataSet = pd.read_csv(pathToHeightsFile, header=None, skipinitialspace=True)
    return dataSet


def normalize2dArrayToValuesBetweenMinus1and1(arrayInput):
    print(arrayInput.shape)

    for i in range(len(arrayInput)):
        minValue = np.min(arrayInput[i])
        maxValue = np.max(arrayInput[i])
        valueRange = maxValue - minValue
        for j in range(len(arrayInput[i])):
            arrayInput[i][j] = 2 * ((arrayInput[i][j] - minValue) / valueRange) - 1
    return arrayInput


def reshapeArray(array):
    if not useConvolutionalModel:
        return array
    # For convolutionalModels we have to reshape the array
    return array.reshape(len(array), domainResolution, domainResolution, 1)



def imgModel():
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    model.add(layers.Conv2D(128, (3, 3), activation='relu', input_shape=(domainResolution, domainResolution, 1)))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((3, 3)))

    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.BatchNormalization())
    # model.add(layers.MaxPooling2D((3, 3)))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    # model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.BatchNormalization())
    model.add(layers.Conv2D(3, (1, 1), activation='linear'))
    model.summary()

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
                  loss=tf.keras.losses.MeanSquaredLogarithmicError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError()])
    return model

def createBiggerModel():
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    model.add(layers.Conv2D(128, (3, 3), activation='relu', input_shape=(domainResolution, domainResolution, 1)))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((3, 3)))

    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.BatchNormalization())
    # model.add(layers.MaxPooling2D((3, 3)))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    # model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.BatchNormalization())
    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.BatchNormalization())
    model.add(layers.Flatten())
    model.add(layers.Dense(500, activation='relu'))
    model.add(layers.Dense(200, activation='relu'))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Dense(1))
    model.summary()

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
                  loss=tf.keras.losses.MeanSquaredLogarithmicError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError()])
    return model


def createConvModel():
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    model.add(layers.Conv2D(128, (3, 3), activation='relu', input_shape=(domainResolution, domainResolution, 1)))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    # model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.BatchNormalization())
    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.BatchNormalization())
    model.add(layers.Flatten())
    model.add(layers.Dense(500, activation='relu'))
    model.add(layers.Dense(200, activation='relu'))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Dense(1))
    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
                  loss=tf.keras.losses.MeanSquaredLogarithmicError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError()])

    return model

def createSimpleModel():
    model = models.Sequential()
    model.add(layers.Dense(1000, activation='relu', input_shape=(domainResolution * domainResolution,)))
    model.add(layers.BatchNormalization())
    # model.add(layers.Dropout(0.5))
    model.add(layers.Dense(4000, activation='relu'))
    model.add(layers.BatchNormalization())
    # model.add(layers.Dropout(0.25))
    model.add(layers.Dense(1000, activation='relu'))
    model.add(layers.Dense(1000, activation='relu'))
    # model.add(layers.Dropout(0.25))
    model.add(layers.Dense(1, activation='relu'))
    model.summary()

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
                  loss=tf.keras.losses.MeanSquaredLogarithmicError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError()])
    return model


def evaluateModelOwnFunction(model, xTest, yTest):
    predictions = model.predict(xTest)
    sum = 0

    for i in range(len(yTest)):
        error = abs(predictions[i] - yTest[i])
        print("model: " + str(predictions[i]) + "   Y_test: " + str(yTest[i]) + " Error: " + str(error))
        sum += error
    # The Mean error
    return sum / len(yTest)


def evaluateModelUsingMean(yTrain, yTest):
    mean = np.mean(yTrain)
    sum = 0

    print("Mean: " + str(mean))
    for i in range(len(yTest)):
        error = abs(mean - yTest[i])
        # print("model: " + str(mean) + "   Y_test: " + str(yTest[i]) +  " Error: " + str(error))
        sum += error
    # The Mean error
    return sum / len(yTest)

def plot_loss(history):
    """
    Prints the loss of the trainings history of a model
    :param history: the history of the model
    :return: nothing
    """
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    # plt.ylim([0, 10])
    plt.xlabel('Epoch')
    plt.ylabel('Error [maxVorticity]')
    plt.legend()
    plt.grid(True)
    plt.show()


def plot_Errors(test_predictions, yTest):
    """
    Plots the error of the
    :param yTest:
    :param test_predictions:
    :return:
    """
    error = (test_predictions - yTest) / yTest
    plt.hist(error, bins=25)
    plt.xlabel('Prediction Error')
    _ = plt.ylabel('Count')
    plt.show()


def plot_Errors2(test_predictions):
    plt.scatter(yTest * 1000, test_predictions * 1000, label='Data')
    plt.xlabel('True Values max. Vorticity')
    plt.ylabel('Predictions max. Vorticity')
    lims = [20000, 200000]
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims, color='g')
    x = tf.linspace(20000, 200000, 351)
    y = x + x * 0.5
    x2 = tf.linspace(20000, 200000, 351)
    y2 = x - x * 0.5
    plt.plot(x, y, color='k', label='+ 50%')
    plt.plot(x2, y2, color='grey', label='- 50%')
    x3 = tf.linspace(20000, 200000, 351)
    y3 = x + x * 0.3
    x4 = tf.linspace(20000, 200000, 351)
    y4 = x - x * 0.3
    plt.plot(x3, y3, color='red', label='+ 30%')
    plt.plot(x4, y4, color='m', label='- 30%')
    plt.legend()
    plt.show()


# plot_Errors2(test_predictions)
# plot_Errors(test_predictions)

def plotTrainingHistory(history):
    """
    Prints the errors measured during the trainingsprocess
    :param history: the history of the model which had stored the errors
    :return: nothing
    """
    plt.plot(history.history['mean_absolute_error'])
    plt.plot(history.history['val_mean_absolute_error'])
    #plt.title('Durchschnittlicher mittlerer Fehler')
    plt.ylabel('Durchschnittlicher mittlerer Fehler')
    plt.xlabel('Anzahl Epochen')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_MAF.png", dpi=400)
    plt.show()


    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    #plt.title('Loss')
    plt.ylabel('Loss')
    plt.xlabel('Anzahl Epochen')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_Loss.png", dpi=400)
    plt.show()


def generateFritzModel():
    model = models.Sequential()
    model.add(layers.Dense(140, input_shape=(30,), activation='elu'))
    #model.add(layers.BatchNormalization())
    layers.Dropout(0.3),
    #layers.Dense(800, activation='relu'),
    #layers.Dropout(0.5),

    model.add(layers.Dense(460, activation='elu'))
    #model.add(layers.Dense(200, activation=tf.keras.layers.LeakyReLU()))
    #model.add(layers.BatchNormalization())
    layers.Dropout(0.3)

    model.add(layers.Dense(360, activation='elu'))
    #model.add(layers.Dense(200, activation=tf.keras.layers.LeakyReLU()))
    #model.add(layers.BatchNormalization())
    layers.Dropout(0.3)

    model.add(layers.Dense(1))

    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0025),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError(), tf.keras.metrics.MeanAbsolutePercentageError()])
    model.summary()
    return model

def getHeatMap():
    pathToHeightsFile = "/home/fulrich/Promotion/CEAS-Journal/vorticity_prediction-main/CNN_Training_Fritz/Heights_32_32.csv"
    pathToMaxVorticityFile = "/home/fulrich/Promotion/CEAS-Journal/vorticity_prediction-main/CNN_Training_Fritz/1_6_0_All.csv"
    xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution)
    setValuesForReproducibility()
    model = createConvModel2()
    xData = coverHeightAreaWithMinusOne(xTrain, 12, 12, 4, 4)

def setValuesForReproducibility():
    seed_value = 10
    os.environ['PYTHONHASHSEED'] = str(seed_value)

    # 2. Set the `python` built-in pseudo-random generator at a fixed value
    import random
    random.seed(seed_value)

    # 3. Set the `numpy` pseudo-random generator at a fixed value
    import numpy as np
    np.random.seed(seed_value)

    # 4. Set the `tensorflow` pseudo-random generator at a fixed value
    import tensorflow as tf
    tf.random.set_seed(seed_value)
    # for later versions:
    # tf.compat.v1.set_random_seed(seed_value)

    # 5. Configure a new global `tensorflow` session
    #from keras import backend as K
    #session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
    #sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
    #K.set_session(sess)



    #test_predictions = model.predict(xTest)
    # savingCallback = tf.keras.callbacks.ModelCheckpoint(
    #    filepath=pathToModel,
    #    verbose=1,
    #    save_weights_only=True,
    #    monitor='mean_absolute_error',
    #    mode='min',
    #    save_best_only=True)
    # load best model
    # latestCheckpoint = tf.train.latest_checkpoint(pathToModel)
    # model = createSimpleModel()
    # model.load_weights(latestCheckpoint)

def trainModelWorker():
    modelName = "ninthModel"
    #pathToHeightsFile = "/home/thomas/Dokumente/HiWi/vorticity_prediction/Databases/HeightsProfiles/Heights_16_16.csv"
    #pathToMaxVorticityFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"

    xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile,
                                                                   domainResolution)
    model, score = trainModelWithSpecificData(modelName, xTrain, xTest, yTrain, yTest)
    return score


def trainModelInParallel():
    executor = ThreadPoolExecutor(max_workers=4)
    resultList = []
    for i in range(4):
        future = executor.submit(trainModelWorker)
        resultList.append(future.result())

    executor.shutdown()
    print(resultList)


def heatMapDataGeneration():
    pathToHeightsFile = "/home/fulrich/Promotion/CEAS-Journal/vorticity_prediction-main/CNN_Training_Fritz/Heights_32_32.csv"
    pathToMaxVorticityFile = "/home/fulrich/Promotion/CEAS-Journal/vorticity_prediction-main/CNN_Training_Fritz/1_6_0_All.csv"

    with open("logfile.csv", "a") as logFile:
        #Write Header
        logFile.write("xPos,yPos,MAE,MAXE,MPE\n")
        #xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution)
        for i in range(4):
            xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile,
                                                                           pathToHeightsFile, domainResolution)
            xDataTrain = coverHeightAreaWithMinusOne(xTrain, i * 4, 0, 4, 16)
            xDataTest = coverHeightAreaWithMinusOne(xTest, i * 4, 0, 4, 16)
            #result = trainModelMultipleTimes(xDataTrain, xDataTest, yTrain, yTest)
            #logFile.write(str(i) + "," + str(0) + "," + str(result["MAE"]) + "," +
            #              str(result["MAXE"]) +  "," + str(result["MPE"]) + "\n")
            #saveRoughnessXdataToImage(xDataTrain[0], str(i) + "_" + str(0) + "covered")



if __name__ == '__main__':
    #Paths
    os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
    setValuesForReproducibility()
    modelOutputPath = "../savedModels/HeightModels/32_32_model_std"
    pathToHeightsFile = "../Databases/Heights_32_32.csv"
    pathToMaxVorticityFile = "../Databases/Vorticities.csv"
    #TODO Wichtig!!!!
    #Wenn du den Autoencoder trainierst muss du alles ab hier auskommentieren. Wegen dem Import von Methoden wird dieser
    #nachfolgende Code ansonsten auskommentiert


    # TODO Diese 5 Zeilen brauchst du immer nur dann wenn du einen neuen Parameter trainierst. Dann wird die Datenbank als
    # abgespeichert. Den Namen zur Abspeicherung gibst du in der 5. Zeile an
    #targetScaler, xTrain, xTest, yTrain, yTest = \
    #    getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution,
    #                                    regressionParameter="maxVorticityParabola", inputNormalization="std",
    #                                   outputNormalization="std")
    #saveDataSetAsNumpyArrs(xTrain, xTest, yTrain, yTest, "max_omegaX_vort_std", targetScaler)

    #TODO lädt die Numpydatenbank mit dem entsprechenden Namen
    #xTrain, xTest, yTrain, yTest, targetScaler = loadNumpyDataSet("max_omegaX_vort_std")
    # Train model
    #modelWhichShouldBeTrained = createConvModelBest(lrRate=0.00025)
    #model, score = trainModelWithSpecificData(modelWhichShouldBeTrained, xTrain, xTest, yTrain, yTest,
    #                                          modelOutputPath=modelOutputPath, targetScaler=targetScaler, batchSize=26)




    #analysisOfDataBaseSize()
    #plotAnalysisOfDataSize("LogFileAnalysisDBsizeMulti.csv")


    #heatMapDataGeneration()
    #xTrain = transformWholeDataSet(xTrain)
    #xTest = transformWholeDataSet(xTest)
    #evaluateModelUsingMean(yTrain, yTest)
    #model = tf.keras.models.load_model(modelOutputPath)
    #predictions = model.predict(xTest)
    #modelScore = evaluatePredictions(yTest, predictions, targetScaler)

    experimentName = "cnn_test"
    pathWhereModelsShouldBeSaved = "../savedModels/"
    model = createConvModelBest(lrRate=0.00025)
    targetScaler, xTrain, xTest, yTrain, yTest = \
    getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution,
                              regressionParameter="maxVorticityParabola", inputNormalization="std",
                              outputNormalization="std")

    model = trainModel(experimentName + "_model", model, xTrain, yTrain, targetScaler=targetScaler,
               pathToModel=pathWhereModelsShouldBeSaved)
    # Load already trained model
    #model = tf.keras.models.load_model("../savedModels/32_32_model_std/" )
    #plotModelPerformanceInPercent(yTest, predictions)
    predictions = model.predict(xTest)
    predictions = targetScaler.inverse_transform(predictions)
    evaluatePredictions(yTest, predictions)
    #print( targetScaler.inverse_transform(predictions))
    plot_Errors3(predictions, yTest)


    # save Predictions / true values
    #predictions2 = predictions.flatten()
    #yTestFlattened = yTest.flatten()
    #df = pd.DataFrame({
    #    "Predictions" : predictions2,
    #    "trueValues" : yTestFlattened
    #})
    #df.to_csv('predictions-true-values.csv')


    #model.summary()
    #plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)
    #model.evaluate(xTest, yTest)
    #xData = coverHeightAreaWithMinusOne(xTrain, 12,12, 4, 4)
    #for i in range(10,20):
    #    coveredImage = xTrain[i]
    #    saveXdataToImage(coveredImage, str(i))



    #coveredImage = coverHeightAreaWithMinusOne(xTrain[1], 12, 12, 4, 4)
    #convertXdataToImage(coveredImage, 1)

    #trainModel(modelName)
    #model = tf.keras.models.load_model("../Models/" + modelName)
    #model.summary()
    #(xTrain, xTest, yTrain, yTest) = createTestAndTrainingsDataSet2("../Databases/HeightsDatabases/First10BatchesHeightsDB.csv")
    #evaluateModelOwnFunction(model, xTest, yTest)

