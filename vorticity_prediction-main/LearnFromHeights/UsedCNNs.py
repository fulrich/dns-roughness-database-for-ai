from concurrent.futures import ThreadPoolExecutor

import pandas as pd
import tensorflow as tf
import numpy as np
from tensorflow.keras import datasets, layers, models


epochs = 40
# The resolution of the quadratic grid
domainResolution = 32
# If we use a convolutional network the data has to be reshaped
useConvolutionalModel = True

"""
Hier sind ein paar Vorschläge für Modelle

"""

def createConvModelBest(lrRate=0.00025):
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    model.add(layers.Conv2D(64, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), input_shape=(domainResolution, domainResolution, 1), padding="same",name="firstConv"))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))
    model.add(layers.MaxPooling2D((2, 2)))


    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(128, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding="same", name="secondConv"))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Dropout(0.3))

    model.add(layers.Conv2D(256, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding="same", name="thirdConv"))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Dropout(0.3))
# added by Fritzl
    model.add(layers.Conv2D(512, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding="same", name="forthConv"))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Dropout(0.3))

    model.add(layers.Flatten())

    model.add(layers.Dense(512, activation=tf.keras.layers.LeakyReLU(alpha=0.1)))
    model.add(layers.Dropout(0.3))
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dropout(0.3))

    model.add(layers.Dense(1))

    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=lrRate),
                  loss=tf.keras.losses.MeanSquaredError())
    #model.compile(optimizer=tf.keras.optimizers.Nadam(), loss=tf.keras.losses.MeanSquaredError())
    return model


def createConvModelSmaller(lrRate=0.00025):
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    model.add(layers.Conv2D(32, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), input_shape=(domainResolution, domainResolution, 1), padding="same",name="firstConv"))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))
    model.add(layers.MaxPooling2D((2, 2)))


    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding="same", name="secondConv"))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Dropout(0.3))

    model.add(layers.Conv2D(128, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding="same", name="thirdConv"))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Dropout(0.3))

    model.add(layers.Flatten())

    model.add(layers.Dense(512, activation=tf.keras.layers.LeakyReLU(alpha=0.1)))
    model.add(layers.Dropout(0.3))
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dropout(0.3))

    model.add(layers.Dense(1))

    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=lrRate),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError()])
    return model


def createConvModelSmallest(lrRate=0.00025):
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    model.add(layers.Conv2D(32, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), input_shape=(domainResolution, domainResolution, 1), padding="same",name="firstConv"))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))
    model.add(layers.MaxPooling2D((2, 2)))


    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding="same", name="secondConv"))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Dropout(0.3))

    model.add(layers.Conv2D(64, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding="same", name="thirdConv"))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Dropout(0.3))

    model.add(layers.Flatten())

    model.add(layers.Dense(256, activation=tf.keras.layers.LeakyReLU(alpha=0.1)))
    model.add(layers.Dropout(0.3))
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dropout(0.3))

    model.add(layers.Dense(1))

    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=lrRate),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError()])
    return model