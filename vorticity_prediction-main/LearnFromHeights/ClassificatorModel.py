import math

import keras
from sklearn.preprocessing import StandardScaler
from tensorflow.python.keras.layers import Normalization

from DataVisualization.DataSetPlot import saveRoughnessXdataToImage
from DatabaseGeneration.DatabaseGenerator import getTrainingsDataForAutoEncoder, getTrainingsDataHeightsDatabase, \
    reshapeArray, generateTrainingDataForVorticityClassification
from concurrent.futures import ThreadPoolExecutor

import pandas as pd
import tensorflow as tf
import numpy as np
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
import os
import math
from PIL import Image
from keras.layers import LeakyReLU

from LearnFromHeights.TrainAutoEncoder import extractModelFromPretrainedAutoEncoder
from LearnFromParameters.KerasNN import bestNetHyperparameterOptimization, trainModel
from LearnFromParameters.ModelEvaluation import evaluatePredictions


os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

def trainClassificatorModelWithSpecificData(modelName, xTrain, xTest, yTrain, yTest):
    # xTrain, xTest, yTrain, yTest = createTestAndTrainingsDataSet2("../Databases/HeightsDatabases/First10BatchesHeightsDB.csv")

    autoEncoderModel = tf.keras.models.load_model("../Models/Autoencoders/" + "FourthModel")
    model = addRegressionPartToEncoderPart(autoEncoderModel)
    #model = generateFritzModel()
    earlyStoppingCallback = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy', patience=10,
                                                             restore_best_weights=True, mode="max")
    # Can be used for saving checkpoints, so we need the whole structure of the network etc. So I prefer saving
    # the whole model using model save

    history = model.fit(xTrain, yTrain, epochs=100, validation_freq=1, batch_size=26,
                        validation_split=0.1, callbacks=[earlyStoppingCallback])

    # Save the best model with whole structure
    model.save("../Models/ClassificatorModels/" + modelName)

    #model.evaluate(xTest, yTest)
    predictions = model.predict(xTest)
    modelScore = evaluatePredictions(yTest, predictions)
    return model, modelScore


def addRegressionPartToEncoderPart(autoEncoderModel):
    newModel = extractModelFromPretrainedAutoEncoder(autoEncoderModel, setEncoderWeightsTrainable=False)

    newModel.add(layers.Dense(500, activation='relu', name="dsf1"))
    newModel.add(layers.BatchNormalization(name="dsf2"))
    newModel.add(layers.Dropout(0.3, name="dsf3"))
    newModel.add(layers.Dense(200, activation='relu', name="dsf4"))
    newModel.add(layers.BatchNormalization(name="dsf5"))
    newModel.add(layers.Dropout(0.3, name="dsf6"))
    newModel.add(layers.Dense(3, activation="softmax", name="dsf7"))

    newModel.summary()
    newModel = compileClassificatorModel(newModel)

    return newModel


def compileClassificatorModel(model):
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
                  loss=tf.keras.losses.categorical_crossentropy,
                  metrics=[tf.keras.metrics.categorical_crossentropy, "accuracy"])
    return model

domainResolution = 32
modelName = "TransformedData"
pathToHeightsFile = "/home/fulrich/Promotion/CEAS-Journal/vorticity_prediction-main/CNN_Training_Fritz/Heights_32_32.csv"
pathToMaxVorticityFile = "/home/fulrich/Promotion/CEAS-Journal/vorticity_prediction-main/CNN_Training_Fritz/1_6_0_All.csv"

#analysisOfDataBaseSize()
#plotAnalysisOfDataSize("LogFileAnalysisDBsizeMulti.csv")
xTrain, xTest, yTrain, yTest = generateTrainingDataForVorticityClassification(
                                        0.5, pathToMaxVorticityFile,
                                        pathToHeightsFile=pathToHeightsFile, domainResolution=domainResolution)

trainClassificatorModelWithSpecificData("FirstTry", xTrain, xTest, yTrain, yTest)






