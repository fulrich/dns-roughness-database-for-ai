import tensorflow as tf
from keras.applications import EfficientNetB0
from keras.models import Sequential
from keras import layers
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

print("test")
print(tf.version.VERSION)

img_augmentation = Sequential(
    [
        layers.RandomRotation(factor=0.15),
        layers.RandomTranslation(height_factor=0.1, width_factor=0.1),
        layers.RandomFlip(),
        layers.RandomContrast(factor=0.1),
    ],
    name="img_augmentation",
)

def build_model(numberClasses):
    inputs = layers.Input(shape=(224, 224, 3))
    #x = img_augmentation(inputs)
    model = EfficientNetB0(include_top=False, weights="imagenet")

    # Freeze the pretrained weights
    model.trainable = False

    # Rebuild top
    #x = layers.GlobalAveragePooling2D(name="avg_pool")(model.output)
    #x = layers.BatchNormalization()(x)

    #top_dropout_rate = 0.2
    #x = layers.Dropout(top_dropout_rate, name="top_dropout")(x)
    #outputs = layers.Dense(numberClasses, activation="softmax", name="pred")(x)

    # Compile
    model = tf.keras.Model(inputs, name="EfficientNet")
    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-2)
    model.compile(
        optimizer=optimizer, loss=tf.keras.losses.MeanSquaredLogarithmicError(), metrics=[tf.keras.metrics.MeanAbsoluteError()]
    )
    return model

def unfreeze_model(model):
    # We unfreeze the top 20 layers while leaving BatchNorm layers frozen
    for layer in model.layers[-20:]:
        if not isinstance(layer, layers.BatchNormalization):
            layer.trainable = True

    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-4)
    model.compile(
        optimizer=optimizer, loss=tf.keras.losses.MeanSquaredLogarithmicError(), metrics=[tf.keras.metrics.MeanAbsoluteError()]
    )

def plot_hist(hist):
    plt.plot(hist.history["accuracy"])
    plt.plot(hist.history["val_accuracy"])
    plt.title("model accuracy")
    plt.ylabel("accuracy")
    plt.xlabel("epoch")
    plt.legend(["train", "validation"], loc="upper left")
    plt.show()



model = build_model(numberClasses=1)

trainX = np.random.rand(100,224,224,3)
trainY = np.random.rand(100)
epochs = 25  # @param {type: "slider", min:8, max:80}
hist = model.fit(trainX, trainY, epochs=epochs)
plot_hist(hist)

#unfreeze_model(model)

#epochs = 10  # @param {type: "slider", min:8, max:50}
#hist = model.fit(ds_train, epochs=epochs, validation_data=ds_test, verbose=2)
#plot_hist(hist)