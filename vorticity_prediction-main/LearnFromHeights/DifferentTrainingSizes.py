import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from DatabaseGeneration.DatabaseGenerator import getTrainingsDataParameterFile, loadNumpyDataSet
from LearnFromHeights.ModelTraining import trainModelWithSpecificData
from LearnFromHeights.TrainAutoEncoder import addRegressionPartToEncoderPart
from LearnFromHeights.UsedCNNs import createConvModelBest, createConvModelSmallest, createConvModelSmaller
import tensorflow as tf

def analysisOfDataBaseSize(nameOfNumpyDataSet, pathToLogFile="LogFileAnalysisDBsize.csv", trainAutoEncoder=False):
    """
    Trains a CNN model using different sizes of data of the training set and save the result in the csv logfile
    Args:
        nameOfNumpyDataSet: The name of the dataset which should be used
        pathToLogFile: The name of the output Logfile. The new content is always append to the end of the file
        trainAutoEncoder: Indicates if the autoencoder should be used for training

    Returns:

    """
    # Path to a saved autoencoder
    autoEncoderModel = tf.keras.models.load_model("../Models/Autoencoders/autoEncoder")
    listNumberTrainingsSamples = [100, 250,500,1000,4000,8000]
    #dependent on the the number of trainingsamples we maybe need differnt batchsizes
    batchSizeList = [26, 26, 26]
    xTrain, xTest, yTrain, yTest, targetScaler = loadNumpyDataSet(nameOfNumpyDataSet)

    with open(pathToLogFile, "a") as logFile:
        #Write Header
        logFile.write("numberTrainingsSamples,MAE,MAXE,MPE\n")
        #Here we loop over the different training sizes
        for numberTrainingsSamples in listNumberTrainingsSamples:
            if numberTrainingsSamples < 500:
                # TODO ich habe keine Ahnung welche Werte wir für die Batchsizes und lernraten verwenden sollen. Ich denke
                # aber wir sollten die nicht immer gleich lassen. Je weniger daten desto größer die Lernrate und desto
                # kleiner die Batchsize würde ich denken. Aber das müsstest du ausprobieren
                batchSize = batchSizeList[0]
                if trainAutoEncoder:
                    #freezConvLayers gibt an ob die Conv-layer mittrainiert werden sollen oder nicht
                    model = addRegressionPartToEncoderPart(autoEncoderModel, freezConvLayers=False, learnRate=0.002)
                else:
                    model = createConvModelSmallest(lrRate=0.002)
            elif numberTrainingsSamples <= 2000:
                # TODO ich habe keine Ahnung welche Werte wir für die Batchsizes und lernraten verwenden sollen. Ich denke
                # aber wir sollten die nicht immer gleich lassen. Je weniger daten desto größer die Lernrate und desto
                # kleiner die Batchsize würde ich denken. Aber das müsstest du ausprobieren
                batchSize = batchSizeList[1]
                if trainAutoEncoder:
                    model = addRegressionPartToEncoderPart(autoEncoderModel, freezConvLayers=False, learnRate=0.001)
                else:
                    model = createConvModelSmallest(lrRate=0.001)
            else:
                # TODO ich habe keine Ahnung welche Werte wir für die Batchsizes und lernraten verwenden sollen. Ich denke
                # aber wir sollten die nicht immer gleich lassen. Je weniger daten desto größer die Lernrate und desto
                # kleiner die Batchsize würde ich denken. Aber das müsstest du ausprobieren
                batchSize = batchSizeList[2]
                if trainAutoEncoder:
                    model = addRegressionPartToEncoderPart(autoEncoderModel, freezConvLayers=False, learnRate=0.00025)
                else:
                    model = createConvModelSmallest(lrRate=0.00025)

            #numberIterations gibt an wie viele Modelle mit dem Datensatz trainiert werden sollen pro Interation
            #werden zufällige Trainingsdaten verwendet und dann alle Fehler gemittelt. Für die Veröffentlichung würde ich
            #den Wert auf 5 stellen.
            result = trainCertainModelMultipleTimes(model, xTrain, xTest, yTrain, yTest, targetScaler, numberTrainingsSamples,
                                             numberIterations=1, batchSize=batchSize)
            logFile.write(str(numberTrainingsSamples) + "," + str(result["MAE"]) + "," +
                          str(result["MAXE"]) + "," + str(result["MPE"]) + "\n")


def getRandomTrainData(xTrain, yTrain, numberTrainingData):
    indices = np.random.choice(xTrain.shape[0], numberTrainingData, replace=False)
    xNewTrain = xTrain[indices]
    yNewTrain = yTrain[indices]

    return xNewTrain, yNewTrain


def trainCertainModelMultipleTimes(model, xTrain, xTest, yTrain, yTest, targetScaler, numberTrainingData,
                                   numberIterations=3, batchSize=26):
    resultList = []
    for i in range(numberIterations):
        xTrain, yTrain = getRandomTrainData(xTrain, yTrain, numberTrainingData)
        model, score = trainModelWithSpecificData(model, xTrain, xTest, yTrain, yTest, modelOutputPath="",
                                                  targetScaler=targetScaler, batchSize=26)
        resultList.append(score)

    print(resultList)
    return dict_mean(resultList)


def dict_mean(dict_list):
    mean_dict = {}
    for key in dict_list[0].keys():
        mean_dict[key] = sum(d[key] for d in dict_list) / len(dict_list)
    return mean_dict

def plotAnalysisOfDataSize(fileName):
    """
    Plots the result of a model which was trained with different sizes of trainingsdata
    :param fileName: The path to the stored data
    :return:
    """
    df = pd.read_csv(fileName, skipinitialspace=True)
    #df.rename(columns={'$MAF$': 'MAF [1/s]', "$AF_{max}$": '$AF_{max}$ [10.000 $\cdot$ 1/s]', 'MPF': 'MAF [%]'}, inplace=True)

    df["MPE"] = df["MPE"]*100

    #ax = df.set_index('numberTrainingsSamples').plot(figsize=(10, 5), grid=True)

    ax = df.plot(kind='line', x='numberTrainingsSamples',
                      y='MAE', figsize=(10, 5))
    ax2 = df.plot(kind='line', x='numberTrainingsSamples',secondary_y = True,
                      y='MPE', ax = ax)
    ax2 = df.plot(kind='line', x='numberTrainingsSamples',secondary_y = True,
                      y='MAXE', ax = ax2)

    ax.xaxis.grid(True)
    ax.yaxis.grid(True)
    ax.set_ylabel('Prozentualer Fehler [%]')
    ax2.set_ylabel('Fehler [$10^{4}$ $\cdot$ 1/s]')

    #plt.ylabel('Prozentualer Fehler [%]')
    ax.set_xlabel('Anzahl Trainingsdaten [-]')


    #ax = df['$MPF$'].plot(ylabel='Prozentualer Fehler [%]', fontsize=10)
    #df['$MAF$'].plot(ax=ax, secondary_y=True)
    #df['$AF_{max}$'].plot(ax=ax, secondary_y=True)
    #plt.ylabel('Fehler [$10^{4}$ $\cdot$ 1/s]', fontsize=10, rotation=-90)

    plt.tight_layout()
    plt.savefig("FehlerInAbhaengigkeitDerDatenbankGroeße.png", dpi=400)
    plt.show()

# TODO auch hier wieder wichtig das in ModelTraining alles auskommentiert ist sonst wird das davor ausgeführt!!!


np.random.seed(2022)
nameOfNumpyDataSet = "max_omegaX_vort_std"
# TODO mit trainAutoEncoder=True wird ein vortrainierter Autoencoder verwendet. Mit False wird ein untrainiertes
# Netz verwendet
analysisOfDataBaseSize(nameOfNumpyDataSet, pathToLogFile="LogFileAnalysisDBsize.csv", trainAutoEncoder=True)
plotAnalysisOfDataSize("LogFileAnalysisDBsize.csv")