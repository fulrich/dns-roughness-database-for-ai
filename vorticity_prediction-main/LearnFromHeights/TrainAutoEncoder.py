import math

import keras
from sklearn.preprocessing import StandardScaler
from tensorflow.python.keras.layers import Normalization

from DataVisualization.DataSetPlot import saveRoughnessXdataToImage
from DatabaseGeneration.DatabaseGenerator import getTrainingsDataForAutoEncoder, getTrainingsDataHeightsDatabase, \
    getTrainingsDataForAutoEncoder2, saveDataSetAsNumpyArrs, loadNumpyDataSet

import pandas as pd
import tensorflow as tf
import numpy as np
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
import os
import math
from PIL import Image
from keras.layers import LeakyReLU

from GlobalCode.ImportantGlobalPaths import pathToDataFolder
from LearnFromParameters.KerasNN import bestNetHyperparameterOptimization, trainModel
from LearnFromParameters.ModelEvaluation import evaluatePredictions

from tensorflow.keras import regularizers

domainResolution = 32

sizeOfLatentSpace = 512
sizeOfBottleNeck = 64
sizeOfFirstUpSamplingLayer = 256
rootOfSizeOfLatentSpace = int(math.sqrt(sizeOfFirstUpSamplingLayer))



os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

print("GPUs: ", len(tf.config.experimental.list_physical_devices('GPU')))

def createAutoEncoderModel(xTrain):
    #normLayer = Normalization()
    #normLayer.adapt(xTrain)


    model = models.Sequential()
    #model.add(normLayer)
    model.add(layers.Conv2D(32, (3, 3), activation='relu', strides=2, input_shape=(domainResolution, domainResolution, 1), name="firstConv", padding="same"))
    #model.add(layers.Conv2D(64, (3, 3), activation='relu', name="secondConv", padding="same"))

    #model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))


    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu', strides=2, name="secondConv", padding="same"))
    #model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))

    model.add(layers.Conv2D(128, (3, 3), activation='relu', name="thirdConv", padding="same"))
    model.add(layers.BatchNormalization())

    model.add(layers.Conv2D(256, (2, 2), activation='relu',strides=2, name="fourthConv", padding="same"))
    model.add(layers.BatchNormalization())

    model.add(layers.Conv2D(64, (1, 1), activation='relu', name="fifthConv", padding="same"))
    model.add(layers.BatchNormalization())

    model.add(layers.AveragePooling2D((2,2)))
    #model.add(layers.Dropout(0.3))

    #Was down sampled to the space of 8 by 8

    model.add(layers.Flatten())
    #model.add(layers.Dropout(0.3))
    #model.add(layers.Dense(sizeOfLayerBeforeLatentSpace, activation=tf.keras.layers.LeakyReLU(alpha=0.3)))
    #model.add(layers.Dropout(0.3))

    #model.add(layers.Dense(sizeOfLatentSpace, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name="latentSpace"))
    #model.add(layers.Dropout(0.3))

    model.add(layers.Dense(sizeOfFirstUpSamplingLayer, activation=tf.keras.layers.LeakyReLU(alpha=0.1), name="firstDecoderLayer"))

    #This layer has the name reshape I dont know why I can not rename it
    model.add(tf.keras.layers.Reshape((rootOfSizeOfLatentSpace, rootOfSizeOfLatentSpace, 1), name="reshapeLayer"))
    model.add(layers.Conv2DTranspose(128, (3, 3), strides=2, activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding='same', name="firstConvDecoderLayer"))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))
    #'reshape'
    model.add(layers.Conv2DTranspose(64, (3, 3), strides=1, activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding='same'))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))

    model.add(layers.Conv2DTranspose(32, (3, 3), strides=1, activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding='same'))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))

    model.add(layers.Conv2D(1, (3, 3), activation='sigmoid', padding='same'))
    #model.add(layers.Dropout(0.3))

    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanSquaredError()])
                  #loss=tf.keras.losses.MeanAbsoluteError(),
                  #metrics=[tf.keras.metrics.MeanAbsoluteError(), tf.keras.metrics.MeanSquaredError()])

    return model


def createAutoEncoderModel2(xTrain):
    #normLayer = Normalization()
    #normLayer.adapt(xTrain)


    model = models.Sequential()
    model.add(layers.Conv2D(64, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), input_shape=(domainResolution, domainResolution, 1), padding="same",name="firstConv"))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))
    model.add(layers.MaxPooling2D((2, 2)))


    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(128, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding="same", name="secondConv"))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    #model.add(layers.Dropout(0.3))

    model.add(layers.Conv2D(256, (3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding="same", name="thirdConv"))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    #model.add(layers.Dropout(0.3))


    model.add(layers.Flatten())
    model.add(layers.Dense(sizeOfLatentSpace, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name="latentSpace"))
    #model.add(layers.Dropout(0.3))

    model.add(layers.Dense(sizeOfLatentSpace, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name="bottleNeck"))
    #model.add(layers.Dropout(0.3))

    model.add(layers.Dense(sizeOfFirstUpSamplingLayer, activation=tf.keras.layers.LeakyReLU(alpha=0.1), name="firstDecoderLayer"))

    #This layer has the name reshape I dont know why I can not rename it
    model.add(tf.keras.layers.Reshape((rootOfSizeOfLatentSpace, rootOfSizeOfLatentSpace, 1), name="reshapeLayer"))
    model.add(layers.Conv2DTranspose(256, (3, 3), strides=2, activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding='same', name="firstConvDecoderLayer"))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))
    #'reshape'
    model.add(layers.Conv2DTranspose(128, (3, 3), strides=1, activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding='same'))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))

    model.add(layers.Conv2DTranspose(64, (3, 3), strides=1, activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding='same'))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))

    model.add(layers.Conv2D(1, (3, 3), activation='sigmoid', padding='same'))
    #model.add(layers.Dropout(0.3))

    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanSquaredError()])
                  #loss=tf.keras.losses.MeanAbsoluteError(),
                  #metrics=[tf.keras.metrics.MeanAbsoluteError(), tf.keras.metrics.MeanSquaredError()])

    return model

def extractModelFromPretrainedAutoEncoder(autoEncoderModel, freezeConvLayers=True):
    """
    Extracts the convolutional layer from a pretrained autoencoder and sets all
    Conv-layers to trainable = False

    Args:
        autoEncoderModel: the pretrained autoencoder model

    Returns:
        returns a sequential model containing the Conv-layers of the pretrained
        autoencoder
    """

    newModel = tf.keras.Sequential()

    for layer in autoEncoderModel.layers:
        # The first layer which should be excluded
        if layer._name == "bottleNeck":
            break
        # We do not want to retrain the CON-layers
        if freezeConvLayers:
            layer.trainable = False
        newModel.add(layer)
        # After the batchnormalization_layer dropout layers are added
        if layer._name == "batch_normalization":
            #newModel.add(layers.Dropout(0.5))
            print("Dropout_layer was added")

    return newModel


def setAllLayersFromModelToTrainable(model):
    """
    Sets all layers of a Keras model to trainable and also compiles the model
    Args:
        model: The model which layers should be set to trainable

    Returns:
        The compiled model
    """

    for layer in model.layers:
        layer.trainable = True

    #sgd = tf.keras.optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)


    adam = tf.keras.optimizers.Adam(learning_rate=0.00025)
    #adam = tf.keras.optimizers.Adam(learning_rate=lr_schedule)
    model.compile(optimizer=adam,
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanSquaredError(), tf.keras.metrics.MeanAbsoluteError()])
    model.summary()
    return model


def compileModel(model, learnRate=0.00025):
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=learnRate),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError(), tf.keras.metrics.MeanSquaredError()])
    return model

def addRegressionPartToEncoderPart(autoEncoderModel, freezConvLayers=True, learnRate=0.00025):
    """
    Extracts the Conv-Layer part of the encoder part of the auto encoder model
    and adds a regression part on top of it.

    Args:
        autoEncoderModel: A pretrained autoencoder

    Returns:
        newModel: The new model which could be used to do the regression task
    """

    newModel = extractModelFromPretrainedAutoEncoder(autoEncoderModel, freezConvLayers)
    #newModel.add(layers.Dropout(0.3, name="reg_1"))
    #newModel.add(layers.Dense(512, activation=tf.keras.layers.LeakyReLU(alpha=0.1), name="reg_2", kernel_regularizer=regularizers.L2(0.0001)))
    #newModel.add(layers.BatchNormalization(name="reg_3"))
    newModel.add(layers.Dropout(0.3, name="reg_4"))
    newModel.add(layers.Dense(256, activation="relu", name="reg_5", kernel_regularizer=regularizers.L2(0.0001)))
    newModel.add(layers.BatchNormalization(name="reg_6"))
    newModel.add(layers.Dropout(0.3, name="reg_7"))
    newModel.add(layers.Dense(1, name="reg_8", kernel_regularizer=regularizers.L2(0.0001)))

    #newModel.add(layers.Dense(250, activation='relu', name="dsf"))
    #newModel.add(layers.Dropout(0.3, name="dsf1"))
    #newModel.add(layers.BatchNormalization(name="dsf3"))

    #newModel.add(layers.Dense(500, activation="relu", name="dsf4"))
    #newModel.add(layers.Dropout(0.3, name="dsf5"))
    #newModel.add(layers.BatchNormalization(name="dsf6"))

    #newModel.add(layers.Dense(1, name="dsf10"))

    newModel.summary()
    newModel = compileModel(newModel, learnRate=learnRate)

    return newModel


def trainAutoEncoderWithSpecificData(modelName, xTrain, xTest, yTrain, yTest):
    """
    Trains the autoencoder model with specific data
    Args:
        modelName: The name und which the model should be saved
        xTrain: The train data for the auto encoder
        xTest: The test data of the autoencoder
        yTrain: The y train data of the autoencoder

    Returns:

    """

    os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
    model = createAutoEncoderModel2(xTrain)


    earlyStoppingCallback = tf.keras.callbacks.EarlyStopping(monitor='val_mean_squared_error', patience=1,
                                                             restore_best_weights=True, mode="min")
    # Can be used for saving checkpoints, so we need the whole structure of the network etc. So I prefer saving
    # the whole model using model save

    history = model.fit(xTrain, yTrain, validation_freq=1, epochs=5,  batch_size=64,
                        validation_split=0.1, callbacks=[earlyStoppingCallback])
    #plotTrainingHistory(history)

    model.save("../Models/Autoencoders/" + modelName)

    #modelScore = evaluatePredictions(yTest, predictions)
    visualizeAutoencoderResults(model, xTest, yTest)
    model.evaluate(xTest, yTest)
    return model


def trainPretrainedAutoEncoderWithSpecificData(autoEncoderModel, xTrain, xTest, yTrain, yTest,
                                               modelName="retrainedAutoEncoder_2", freezeConvLayers=True,
                                               learnRate=0.00025):

    # xTrain, xTest, yTrain, yTest = createTestAndTrainingsDataSet2("../Databases/HeightsDatabases/First10BatchesHeightsDB.csv")

    model = addRegressionPartToEncoderPart(autoEncoderModel, freezeConvLayers, learnRate=learnRate)
    model = compileModel(model)
    #model = createConvModel2()
    #model = createConvModel2()

    earlyStoppingCallback = tf.keras.callbacks.EarlyStopping(monitor='val_mean_absolute_error', patience=5,
                                                             restore_best_weights=True, mode="min")
    tbCallBack = tf.keras.callbacks.TensorBoard(log_dir='TensorboardLogs', histogram_freq=0,
                                             write_graph=True, write_images=True)
    # Can be used for saving checkpoints, so we need the whole structure of the network etc. So I prefer saving
    # the whole model using model save

    history = model.fit(xTrain, yTrain, epochs=100, validation_freq=1, batch_size=26,
                        validation_split=0.1, callbacks=[earlyStoppingCallback, tbCallBack])
    #plotTrainingHistory(history)

    model.save("../Models/Autoencoders/" + modelName)

    #model.evaluate(xTest, yTest)
    predictions = model.predict(xTest)
    modelScore = evaluatePredictions(yTest, predictions, targetScaler=targetScaler)
    return model, modelScore

#def scheduler(epoch, lr):
#    print(lr)
#    return lr * tf.math.exp(-0.1)

def fineTunePretrainedAutoEncoderWithSpecificData(model, xTrain, xTest, yTrain, yTest, modelName="fineTunedModel", numberEpochs=300):
    earlyStoppingCallback = tf.keras.callbacks.EarlyStopping(monitor='val_mean_absolute_error', patience=10,
                                                             restore_best_weights=True, mode="min")

    #lr_scheduleCallback = tf.keras.callbacks.LearningRateScheduler(scheduler)

    # Can be used for saving checkpoints, so we need the whole structure of the network etc. So I prefer saving
    # the whole model using model save
    predictions = model.predict(xTest)
    modelScore = evaluatePredictions(yTest, predictions, targetScaler=targetScaler)

    history = model.fit(xTrain, yTrain, epochs=numberEpochs, validation_freq=1, batch_size=56,
                        validation_split=0.1, callbacks=[earlyStoppingCallback])
    #plotTrainingHistory(history)

    model.save("../Models/Autoencoders/" + modelName)

    #model.evaluate(xTest, yTest)
    predictions = model.predict(xTest)
    modelScore = evaluatePredictions(yTest, predictions, targetScaler=targetScaler)
    return model, modelScore


def visualizeAutoencoderResults(autoEncoderModel, xTest, yTest):
    indices = [1,34,45,67,70]
    predictions = autoEncoderModel.predict(xTest)

    #print("xTest: " + str(xTest[45]))
    #print("prediction: " + str(predictions[45]))

    for index in indices:
        saveRoughnessXdataToImage(predictions[index], "../Output/autoencoder_" + str(index))
        saveRoughnessXdataToImage(yTest[index], "../Output/autoencoder_" + str(index) + "_y")


def trainModelAutoEncoderModel(modelName):
    #os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
    #pathToTestFile = pathToDataFolder + "HeightsProfiles/HeightsProfiles_15k/Heights_32_32.csv"
    #xTrainAutoEncoderSmall, xTestAutoEncoderSmall, yTrainAutoEncoderSmall, yTestAutoEncoderSmall = \
    #    getTrainingsDataForAutoEncoder(pathToTestFile, domainResolution, inputNormalization=inputNormalization)

    #saveDataSetAsNumpyArrs(xTrainAutoEncoderSmall, xTestAutoEncoderSmall, yTrainAutoEncoderSmall, yTestAutoEncoderSmall,
    #                      nameOfDataSet="autoEncoderSmall_32_32_std")
    #xTrainAutoEncoderSmall, xTestAutoEncoderSmall, yTrainAutoEncoderSmall, yTestAutoEncoderSmall = \
    #    loadNumpyDataSet("autoEncoderSmall")

    #TODO nach einmaligen Ausführen diese 5 Zeilen auskommentieren
    pathToTrainingFile = pathToDataFolder + "HeightsProfiles/HeightsProfiles_100k/Heights_32_32.csv"
    xTrainAutoEncoderBig, xTestAutoEncoderSmallBig, yTrainAutoEncoderBig, yTestAutoEncoderBig = \
    getTrainingsDataForAutoEncoder(pathToTrainingFile, domainResolution, inputNormalization=inputNormalization)
    saveDataSetAsNumpyArrs(xTrainAutoEncoderBig, xTestAutoEncoderSmallBig, yTrainAutoEncoderBig, yTestAutoEncoderBig,
                          nameOfDataSet="autoEncoderBig_32_32_std")

    #TODO Hier wird der Datensatz als numpy array eingelesen
    xTrainAutoEncoderBig, xTestAutoEncoderSmallBig, yTrainAutoEncoderBig, yTestAutoEncoderBig = \
        loadNumpyDataSet("autoEncoderBig_32_32_std")
    model = trainAutoEncoderWithSpecificData(modelName, xTrainAutoEncoderBig, xTestAutoEncoderSmallBig, yTrainAutoEncoderBig, yTestAutoEncoderBig)
    return model

def retrainAutoEncoderModel(xTrain, xTest, yTrain, yTest, modelNameBase, modelNameResult, freezeConvLayers=True,
                            learnRate=0.00025):
    """

    Args:
        modelNameBase: The name of the model which should be retrained
        modelNameResult: The output name of the retrained model

    Returns:

    """
    os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

    autoEncoderModel = tf.keras.models.load_model("../Models/Autoencoders/" + modelNameBase)
    #autoEncoderModel = tf.keras.models.load_model("../Models/Autoencoders/" + "retrainedAutoEncoder")
    #autoEncoderModel = setAllLayersFromModelToTrainable(autoEncoderModel)

    trainPretrainedAutoEncoderWithSpecificData(autoEncoderModel, xTrain, xTest, yTrain, yTest, modelName=modelNameResult,
                                               freezeConvLayers=freezeConvLayers,learnRate=learnRate)


def fineTuneModel(xTrain, xTest, yTrain, yTest, modelNameBase, modelNameResult, numberEpochs=300):
    os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
    autoEncoderModel = tf.keras.models.load_model("../Models/Autoencoders/" + modelNameBase)
    autoEncoderModel = setAllLayersFromModelToTrainable(autoEncoderModel)

    fineTunePretrainedAutoEncoderWithSpecificData(autoEncoderModel, xTrain, xTest, yTrain, yTest,
                                                  modelName=modelNameResult, numberEpochs=numberEpochs)

def getExtractedParametersFromLatentSpace(trainedEncoderModel, xTrain, xTest):
    predictionsTrain = trainedEncoderModel.predict(xTrain)
    predictionsTest = trainedEncoderModel.predict(xTest)

    scaler = StandardScaler()
    #scaler = MinMaxScaler()

    xTrain = scaler.fit_transform(predictionsTrain)
    xTest = scaler.transform(predictionsTest)
    xTestTransposed = xTrain.transpose()

    #v4 = np.std(xTestTransposed)
    #v1 = np.std(xTestTransposed, axis=-1)
    #v3 = np.std(xTestTransposed, axis=1)

    return xTrain, xTest

def getNewParameters():
    pathToDBfile = "/home/thomas/Dokumente/HiWi/vorticity_prediction/Databases/HeightsProfiles/Heights_32_32.csv"
    pathToMaxVorticityFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"

    autoEncoderModel = tf.keras.models.load_model("../Models/Autoencoders/" + "FourthModel")
    #autoEncoderModel = addRegressionPartToEncoderPart(autoEncoderModel)
    autoEncoderModel = extractModelFromPretrainedAutoEncoder(autoEncoderModel)
    autoEncoderModel = compileModel(autoEncoderModel)

    xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToDBfile,
                                                                   domainResolution)

    xTrainLatentSpace, xTestLatentSpace = getExtractedParametersFromLatentSpace(autoEncoderModel, xTrain, xTest)

    return xTrainLatentSpace, xTestLatentSpace

def trainModelUsingLatentSpaceAsInput():
    xTrain = np.load("../Databases/LatentSpaceDB/latentSpace_x_train_77.npy")
    xTest = np.load("../Databases/LatentSpaceDB/latentSpace_x_test_77.npy")

    pathToDBfile = "/home/thomas/Dokumente/HiWi/vorticity_prediction/Databases/HeightsProfiles/Heights_64_64.csv"
    pathToMaxVorticityFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"
    _, _, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToDBfile,
                                                                   domainResolution)

    model = bestNetHyperparameterOptimization()

    simpleModel = bestNetHyperparameterOptimization()
    trainModel("simpleModel", simpleModel, xTrain, xTest, yTrain, yTest)
    simpleModel.evaluate(xTest,yTest)

# TODO fuer Fritz: Den Pfad der Datenbank angeben

#tf.config.set_visible_devices([], 'GPU')
#trainModelUsingLatentSpaceAsInput()


#xTrainLatentSpace, xTestLatentSpace = getNewParameters()
#np.save( "../Databases/LatentSpaceDB/latentSpace_x_train_77.npy", xTrainLatentSpace)
#np.save( "../Databases/LatentSpaceDB/latentSpace_x_test_77.npy", xTestLatentSpace)




#bestNetHyperparameterOptimization()
inputNormalization = "std"
autoEncoderName = "autoEncoder"
pathToHeightsFile = pathToDataFolder + "HeightsProfiles/HeightsProfiles_15k/Heights_32_32.csv"
pathToMaxVorticityFile = pathToDataFolder + "MaxVorticityDatabases/1_6_0_omega_x_all.csv"

#TODO Überprüfen, dass in ModelTraining alles ab 474 auskommentiert ist sonst wird das wegen importen ausgeführt!!!!
#TODO nach einmaligen Ausführen diese 5 Zeilen auskommentieren dann läuft es schneller
#targetScaler, xTrain, xTest, yTrain, yTest = \
#getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution,
#                                    regressionParameter="maxVorticityParabola", inputNormalization=inputNormalization,
#                                    outputNormalization="std")
#saveDataSetAsNumpyArrs(xTrain, xTest, yTrain, yTest, "max_omegaX_vort_std", targetScaler)


#xTrain, xTest, yTrain, yTest, targetScaler = loadNumpyDataSet("max_omegaX_vort_std")
#autoEncoderModel = trainModelAutoEncoderModel(autoEncoderName)
#retrainAutoEncoderModel(xTrain, xTest, yTrain, yTest, autoEncoderName, autoEncoderName + "transfer", learnRate=0.00025)
#fineTuneModel(xTrain, xTest, yTrain, yTest, autoEncoderName + "transfer", autoEncoderName + "fine_tune",
#              numberEpochs=300)











#model = createAutoEncoderModel(None)
#model = tf.keras.models.load_model("../Models/Autoencoders/" + "notTrainedAutoEncoder")
#model = addRegressionPartToEncoderPart(model)
#model.save("../Models/Autoencoders/" + "notPretrainedModel")

#autoEncoderModel = createAutoEncoderModel(xTrain)

#visualizeAutoencoderResults(autoEncoderModel, xTestAutoEncoder)

#print("Training started")
