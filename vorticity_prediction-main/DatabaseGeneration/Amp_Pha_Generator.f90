program calculatePlane
    implicit none 
    real*8, allocatable :: Amp(:,:)
    real*8, allocatable :: Phi(:,:)
    real*8, allocatable :: heights(:,:),dhx(:,:) ,dhz(:,:),dh(:,:)
    !"dx2","dz2","dxdz"
    real*8, allocatable :: dx2(:,:) ,dz2(:,:),dxdz(:,:),curvature(:,:),curvature2(:,:)
    character(len = 128) :: ampPhiFileName, ampPhiFileAfterCalculation, heightsFileName, dateiname    
    integer :: i, j, k, mMax, nMax, m, n, resolution, maxAmpValue, currentFileIndex, numberOfNewCreatedFiles
    integer:: fullrun
    real*8, parameter:: PI = 2*asin(1.) !Konstante
    real*8 :: lambda0, xMin, xMax, zMin, zMax, x, z, maxHeight,alpha,  maxAmp,amp1,amp2,amp3,meanAmp
    real*8::k1,k2,maxCurv,maxCurv2,maxDeriz,maxDerix,meanCurv,meanDerix,meanDeriz
    real*8 :: theta,phis,radius,D_phi,thetar_end,D_thetar,x2,z2,delta1,delta2,delta3,waveamp(3),wavephi(3)
    real*8 :: sumHeights, sumHeightsAbs, averageHeight, averageHeightAbs, proportion, proportionAbs
    character(*), parameter :: pathToSavedAmplitudes = ""



    currentFileIndex = 0

    !Kompilieren: ifort -o amp_pha_generator.x Amp_Pha_Generator.f90 
    lambda0 = 270. !Einheit in mm
    xMin = 3755. !Einheit in mm
    xMax = 3925. !Einheit in mm
    resolution = 200
    zMax = lambda0 / 2.
    zMin = -1 * zMax
    mMax = 3
    nMax = 3
    maxAmpValue = 10
    numberOfNewCreatedFiles = 3
    print *," calculatePlane V1.0"
    print *, " TUM AER 2020"
    print *, " How many files with amplitudes an phases sould be generated?"
    read *, numberOfNewCreatedFiles

    !open (unit=25, file="output-database-gridss.csv")
    !write (25,*) "Gridname,maxAmp,meanAmp,maxCurv,maxCurv2,maxDerix,maxDeriz,&
    !meanCurv,meanDerix,meanDeriz"

    allocate(heights(resolution,resolution))
    allocate(dhx(resolution,resolution))
    allocate(dhz(resolution,resolution))
    allocate(dh(resolution,resolution))
    allocate(dx2(resolution,resolution))
    allocate(dz2(resolution,resolution))
    allocate(dXDZ(resolution,resolution))
    allocate(curvature(resolution,resolution))
    allocate(curvature2(resolution,resolution))


    call generateNewAmpAndPhiFilesWithHeights()


contains

    !Calculates the heights profile from the amplitudes and phases file
    subroutine calculateHeights()
        do i=1,resolution
            z = zMin
            do j=1,resolution
                x = xMin+1.*(i-1)/(1.*resolution-1.)*(xMax-xMin)
                z = zMin+1.*(j-1)/(1.*resolution-1.)*lambda0
                heights(i,j) = 0
                do m=1,mMax
                    do n=1,nMax
                        x2=x/1000.
                        z2=z/1000.
                        radius=5.98172331
                        theta=(x2+5.98172331*0.615729034)/radius
                        phis=z2/(radius*sin(theta))
                        D_phi = (2*pi)/130.
                        thetar_end = 0.6576
                        D_thetar = 1*(2*pi)/130 * sin(thetar_end)
                        heights(i,j) = heights(i,j) + Amp(n,m)*SIN((2.*PI*m*phis/D_phi)+&
                        (2.*PI*n*(theta-thetar_end)/D_thetar) + Phi(n,m))
                    end do
                end do
            end do
        end do
    end subroutine calculateHeights

    subroutine readAmpAndPhiFromFile(fileName)
        character (*), intent(in)  :: fileName
        open (unit=14, file=fileName, status='old', action='read')
        read(14, *) mMax, nMax
        allocate(Amp(mMax,nMax))
        allocate(Phi(mMax,nMax))
        do m=1,mMax
            do n=1,nMax        
                read (14,*) Amp(n,m), Phi(n,m)   
            enddo        
        enddo 
        close(14)
    end subroutine readAmpAndPhiFromFile

    subroutine writeAmpPhiToFile(fileName)
        character (*), intent(in)  :: fileName
        open(10,file=fileName,action='write')
        write(10,'(I3, X, I3)') mMax, nMax
        !print *, mMax, nMax
        do n=1, nMax
            do m=1, mMax
                write(10,'(0PE22.11, X, 0PE22.11)') Amp(m,n), Phi(m,n)
            end do
        end do
        close(10)
    end subroutine writeAmpPhiToFile

    subroutine setMaxHeight()
        !	print *, heights
        maxHeight = heights(1,1)
        do i=1,resolution
            do j=1,resolution
                if (heights(i,j) > maxHeight) then
                    maxHeight = heights(i,j)
                end if            
            end do
        end do
    end subroutine setMaxHeight

    subroutine divideAmp()
        call setMaxHeight() 
	    !print *, "max-Height:",maxHeight
        do n=1, nMax
            do m=1, mMax
                Amp(m,n) = Amp(m,n) / ABS(maxHeight)
            end do
        end do            
    end subroutine divideAmp

    subroutine writeHeightsToFile(fileName)
        character (*), intent(in)  :: fileName
        open(11, file=filename,action='write')
        !write(10,'(i1, X, i1)') mMax, nMax
	    write (11,*) 'TITLE ="hght"'
        write (11,*) 'VARIABLES = "x", "y", "z","dhx","dhz","dx2","dz2","dxdz","dh"'
        write (11,*)  'ZONE F=BLOCK, T= "Block      1", I=', resolution,', J=     1, K=', resolution
        do j=1, resolution
            do i=1, resolution
                x = 0.081*1000 +1.*(i-1)/(1.*resolution-1.)*lambda0  
!                z = zMin+1.*(j-1)/(1.*resolution-1.)*lambda0 
                write (11,*) 1.*x/1000.*0.7+0.01
            end do
        end do
        do j=1, resolution
            do i=1, resolution
 !               x = 1.*(i-1)/(1.*resolution-1.)*lambda0 
 !               z = zMin+1.*(j-1)/(1.*resolution-1.)*lambda0 
                write (11,*)  1*heights(i,j)/10000.
            end do
        end do
        do j=1, resolution
            do i=1, resolution
                z = zMin+1.*(j-1)/(1.*resolution-1.)*lambda0 
                write (11,*)  1.*z/1000.
            end do
        end do
        do j=1, resolution
           do i=1, resolution
                write (11,*)  dhx(i,j)
            end do
        end do
        do j=1, resolution
           do i=1, resolution
                write (11,*)  dhz(i,j)
            end do
        end do
        do j=1, resolution
            do i=1, resolution
                 write (11,*)  dx2(i,j)
             end do
         end do
         do j=1, resolution
            do i=1, resolution
                 write (11,*)  dz2(i,j)
             end do
         end do
         do j=1, resolution
            do i=1, resolution
                 write (11,*)  dxdz(i,j)
             end do
         end do
        do j=1, resolution
           do i=1, resolution
                write (11,*)  dh(i,j)
            end do
        end do
        close(11)    
    end subroutine writeHeightsToFile 

    subroutine printHeightsAverages()
        sumHeights = 0
        sumHeightsAbs = 0
        do i=1,resolution
            do j=1,resolution  
                sumHeights = sumHeights + heights(i,j)
                sumHeightsAbs = sumHeightsAbs + abs (heights(i, j))                          
            end do
        end do
        !Prints
        averageHeight = sumHeights / (resolution*resolution)
        averageHeightAbs = sumHeightsAbs / (resolution*resolution)   
        proportion = maxHeight / averageHeight
        proportionAbs = maxHeight / averageHeightAbs
        print *, "Sum heights:",  sumHeights, " Avg-Height: ", averageHeight, " H_Max / avg-Height: ", proportion
        print *, "Sum heights Abs:",  sumHeightsAbs, " Avg-Height Abs: ", averageHeightAbs, " H_Max / avg-Height: ", proportionAbs
    end subroutine printHeightsAverages

    subroutine generateAmpAndPhi()
        !Initialisieren von Amp und Phi mit zufälligen Werten, die im Bereich zwischen [0,1[ sind
        call random_number(Amp)
        call random_number(Phi)
        do m=1,mMax
            do n=1,nMax
                !Wenn Bendingung erfüllt ist dann ist die Amplitude 0
                if((n*n + m*m > nMax*nMax + 1)) then
                    Amp(m,n) = 0 
                else
                    Amp(m,n) = maxAmpValue * Amp(m,n)
                end if 
                Phi(m,n) = 2*PI * Phi(m, n)
            enddo        
        enddo 
    end subroutine generateAmpAndPhi    

    !Das war die Ursprüngliche Aufgabe 
    subroutine calculateAmpAndPhi()
        call readAmpAndPhiFromFile(trim('AmPh-3-3/AmplitudenUndPhasen_0074.txt'))
        call calculateHeights()
!        call divideAmp()
!        call writeAmpPhiToFile('AmplitudenUndPhasenNachBerechnung.txt')
        call writeHeightsToFile('Hoehen.txt')
        call printHeightsAverages()
	call writeHeightsToFile(trim('OUTPUT.plt'))
    end subroutine calculateAmpAndPhi 

    !Neue Aufgabe hier werden number
    subroutine generateNewAmpAndPhiFilesWithHeights()
        allocate(Amp(mMax,nMax))
        allocate(Phi(mMax,nMax))

        do currentFileIndex=1, numberOfNewCreatedFiles
	        print *, "Erzeuge Datei:",currentFileIndex
            !ErzeugenDerDateiNamen
            write (ampPhiFileName, "(A8,I0.5,A4)") "Amp_Pha_", currentFileIndex, ".txt"
            write (ampPhiFileAfterCalculation, "(A8,I0.5,A4)") "Amp_Pha_", currentFileIndex, ".txt"
            write (heightsFileName, "(A8,I0.5,A4)") "heights_", currentFileIndex, ".plt"

            call generateAmpAndPhi()
            call writeAmpPhiToFile(trim(pathToSavedAmplitudes // 'Amplitudes_Phases/' // ampPhiFileName))
            call calculateHeights()
            call divideAmp()
            call writeAmpPhiToFile(trim(pathToSavedAmplitudes // &
            'Amplitudes_Phases_After_Calculation/' // ampPhiFileAfterCalculation))
        end do
        print *, "Es wurde alle Dateien erfolgreich erstellt"
    end subroutine generateNewAmpAndPhiFilesWithHeights

end program calculatePlane


