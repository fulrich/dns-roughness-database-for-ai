program calculatePlane
    implicit none
    real*8, allocatable :: Amp(:, :)
    real*8, allocatable :: Phi(:, :)
    real*8, allocatable :: heights(:, :)
    !derivations
    real*8, allocatable :: dhdx(:, :), dhdxdx(:, :), dhdxdz(:, :), dhdz(:, :), dhdzdz(:, :)
    character(len = 128) :: fileNameResult
    integer :: i, j, mMax, nMax, m, n, resolution, currentFileIndex, numberOfNewCreatedFiles
    real*8, parameter :: PI = 2 * asin(1.) !Konstante
    real*8 :: theta, phis, radius, D_phi, thetar_end, D_thetar
    real*8 zMin, zMax, xMin, xMax, lambda0, stepSizeX, stepSizeZ
    real*8 :: maxAmpValue

    !Statistical parameters
    real*8 :: maxHeight, minHeight, meanHeight, MAHeight, AADHeight, rmsHeight, skewnessHeight, kurtosisHeight
    real*8 :: maxDhdx, minDhdx, meanDhdx, MADhdx, AADDhdx, rmsDhdx, skewnessDhdx, kurtosisDhdx
    real*8 :: maxDhdz, minDhdz, meanDhdz, MADhdz, AADDhdz, rmsDhdz, skewnessDhdz, kurtosisDhdz
    real*8 :: maxDhdxdx, minDhdxdx, meanDhdxdx, MADhdxdx, AADDhdxdx, rmsDhdxdx, skewnessDhdxdx, kurtosisDhdxdx
    real*8 :: maxDhdzdz, minDhdzdz, meanDhdzdz, MADhdzdz, AADDhdzdz, rmsDhdzdz, skewnessDhdzdz, kurtosisDhdzdz
    real*8 :: maxDhdxdz, minDhdxdz, meanDhdxdz, MADhdxdz, AADDhdxdz, rmsDhdxdz, skewnessDhdxdz, kurtosisDhdxdz

    real*8 :: maxSlopeRightFromMaximum, maxSlopeBeforeMaximum, maxSlopeLeftFromMaximum, maxSlopeAfterMaximum
    real*8 :: maxSlopeRightFromMinimumAfterMaximum, maxSlopeBeforeMinimumAfterMaximum,&
              maxSlopeLeftFromMinimumAfterMaximum, maxSlopeAfterMinimumAfterMaximum
    real*8 :: minimumAfterMaximumInAreaBehind
    real*8 :: minimumSumSlopeChanges, maximumSumSlopeChanges
    

    integer :: xPosMaxHeight, zPosMaxHeight, xPosMinHeight, zPosMinHeight
    integer :: xPosMaxDhdx, zPosMaxDhdx, xPosMinDhdx, zPosMinDhdx
    integer :: xPosMaxDhdz, zPosMaxDhdz, xPosMinDhdz, zPosMinDhdz
    integer :: xPosOfMinimumAfterMaximum 
    integer :: xPosOfMinimumAfterMaximumInAreaBehind, yPosOfMinimumAfterMaximumInAreaBehind
    integer :: numberSlopeChanges

    

    !The area which is overlown 
    real*8 :: crossSectionalAreaX, crossSectionalAreaZ, Ix, Iz

    real*8 :: surfaceRatioX, surfaceRatioZ

    currentFileIndex = 0

    !compile with gfortran -O3  ParameterGenerator.f90  -o parameter.x
    !Einheit in mm
    lambda0 = 270.
    xMin = 3755. !Einheit in mm
    xMax = 3925. !Einheit in mm
    resolution = 200
    zMax = lambda0 / 2.
    zMin = -1 * zMax
    mMax = 3
    nMax = 3
    maxAmpValue = 1.6e-4
    numberOfNewCreatedFiles = 1

    stepSizeX = abs(xMin - xMax) * 1.0 / resolution
    stepSizeZ = abs(zMin - zMax) * 1.0 / resolution

    print *, " ParameterGenerator V1.0"
    print *, " TUM AER 2022"
    fileNameResult = "Parameters.csv"
    call calculateParameters()

contains

    subroutine calculateParameters()
        character(len = 128) :: fileName
        integer :: i
        call allocateAllocatables()
        call writeHeader()
        
        do i = 1, 1000
            !Reading the correct file
            if(i<10)then
                write (fileName, "(A12,I1,A4)") "Amp_Pha_0000", i, ".txt"
            elseif(i<100)then
                write (fileName, "(A11,I2,A4)") "Amp_Pha_000", i, ".txt"
            elseif(i<1000)then
                write (fileName, "(A10,I3,A4)") "Amp_Pha_00", i, ".txt"
            elseif(i<10000)then
                write (fileName, "(A9,I4,A4)") "Amp_Pha_0", i, ".txt"
            else
                write (fileName, "(A8,I5,A4)") "Amp_Pha_", i, ".txt"
            endif

            print *, "current file:", fileName
            call readAmpAndPhifromFile(trim('../Amp3/' // trim(fileName)))         
            call calculateHeightsAndDerivations()

            !Heights
            maxHeight = getMaxValue(heights)
            minHeight = getMinValue(heights)
            meanHeight = getMeanValue(heights)
            MAHeight = getMeanAbsoluteValue(heights)
            AADHeight = calculateAverageAbsoluteDeviation(heights, meanHeight)
            rmsHeight = calculateRMS(heights, meanHeight)
            skewnessHeight = calculateSkewness(heights, meanHeight, rmsHeight)
            kurtosisHeight = calculateKurtosis(heights, meanHeight, rmsHeight)

            !Special Positions
            xPosMaxHeight = getValuePositionI(heights, maxHeight)
            xPosMinHeight = getValuePositionI(heights, minHeight)
            zPosMaxHeight = getValuePositionJ(heights, maxHeight)
            zPosMinHeight = getValuePositionJ(heights, minHeight)
            call findMinimumAfterMaximumInAreaBehind(heights, xPosMaxHeight, zPosMaxHeight)
            
            !Derivations dhdx
            maxDhdx = getMaxValue(dhdx)
            minDhdx = getMinValue(dhdx)
            meanDhdx = getMeanValue(dhdx)
            MADhdx = getMeanAbsoluteValue(dhdx)
            AADDhdx = calculateAverageAbsoluteDeviation(dhdx, meanDhdx)
            rmsDhdx = calculateRMS(dhdx, meanDhdx)
            skewnessDhdx = calculateSkewness(dhdx, meanDhdx, rmsDhdx)
            kurtosisDhdx = calculateKurtosis(dhdx, meanDhdx, rmsDhdx)
            !Special Positions
            xPosMaxDhdx = getValuePositionI(dhdx, maxDhdx)
            xPosMinDhdx = getValuePositionI(dhdx, minDhdx)
            zPosMaxDhdx = getValuePositionJ(dhdx, maxDhdx)
            zPosMinDhdx = getValuePositionJ(dhdx, minDhdx)

            !Derivations dhdz
            maxDhdz = getMaxValue(dhdz)
            minDhdz = getMinValue(dhdz)
            meanDhdz = getMeanValue(dhdz)
            MADhdz = getMeanAbsoluteValue(dhdz)
            AADDhdz = calculateAverageAbsoluteDeviation(dhdz, meanDhdz)
            rmsDhdz = calculateRMS(dhdz, meanDhdz)
            skewnessDhdz = calculateSkewness(dhdz, meanDhdz, rmsDhdz)
            kurtosisDhdz = calculateKurtosis(dhdz, meanDhdz, rmsDhdz)
            !Special Positions
            xPosMaxDhdz = getValuePositionI(dhdz, maxDhdz)
            xPosMinDhdz = getValuePositionI(dhdz, minDhdz)
            zPosMaxDhdz = getValuePositionJ(dhdz, maxDhdz)
            zPosMinDhdz = getValuePositionJ(dhdz, minDhdz)

            !Derivations dhdxdx
            maxDhdxdx = getMaxValue(dhdxdx)
            minDhdxdx = getMinValue(dhdxdx)
            meanDhdxdx = getMeanValue(dhdxdx)
            MADhdxdx = getMeanAbsoluteValue(dhdxdx)
            AADDhdxdx = calculateAverageAbsoluteDeviation(dhdxdx, meanDhdxdx)
            rmsDhdxdx = calculateRMS(dhdxdx, meanDhdxdx)
            skewnessDhdxdx = calculateSkewness(dhdxdx, meanDhdxdx, rmsDhdxdx)
            kurtosisDhdxdx = calculateKurtosis(dhdxdx, meanDhdxdx, rmsDhdxdx)

            !Derivations dhdzdz
            maxDhdzdz = getMaxValue(dhdzdz)
            minDhdzdz = getMinValue(dhdzdz)
            meanDhdzdz = getMeanValue(dhdzdz)
            MADhdzdz = getMeanAbsoluteValue(dhdzdz)
            AADDhdzdz = calculateAverageAbsoluteDeviation(dhdzdz, meanDhdzdz)
            rmsDhdzdz = calculateRMS(dhdzdz, meanDhdzdz)
            skewnessDhdzdz = calculateSkewness(dhdzdz, meanDhdzdz, rmsDhdzdz)
            kurtosisDhdzdz = calculateKurtosis(dhdzdz, meanDhdzdz, rmsDhdzdz)

            !Derivations dhdxdz
            maxDhdxdz = getMaxValue(dhdxdz)
            minDhdxdz = getMinValue(dhdxdz)
            meanDhdxdz = getMeanValue(dhdxdz)
            MADhdxdz = getMeanAbsoluteValue(dhdxdz)
            AADDhdxdz = calculateAverageAbsoluteDeviation(dhdxdz, meanDhdxdz)
            rmsDhdxdz = calculateRMS(dhdxdz, meanDhdxdz)
            skewnessDhdxdz = calculateSkewness(dhdxdz, meanDhdxdz, rmsDhdxdz)
            kurtosisDhdxdz = calculateKurtosis(dhdxdz, meanDhdxdz, rmsDhdxdz)

            !Slopes
            maxSlopeRightFromMaximum = getMaxSlopeRightFromMaximum(heights, xPosMaxHeight, zPosMaxHeight)
            maxSlopeBeforeMaximum = getMaxSlopeBeforeMaximum(heights, xPosMaxHeight, zPosMaxHeight)
            maxSlopeLeftFromMaximum = getMaxSlopeLeftFromMaximum(heights, xPosMaxHeight, zPosMaxHeight)
            maxSlopeAfterMaximum = getMaxSlopeAfterMaximum(heights, xPosMaxHeight, zPosMaxHeight)

            xPosOfMinimumAfterMaximum = getXPosOfMinimumAfterMaximum(heights, xPosMaxHeight, zPosMaxHeight)

            maxSlopeBeforeMinimumAfterMaximum = getMaxSlopeBeforeMinimum(heights, xPosOfMinimumAfterMaximum, zPosMaxHeight)
            maxSlopeAfterMinimumAfterMaximum = getMaxSlopeAfterMinimum(heights, xPosOfMinimumAfterMaximum, zPosMaxHeight)

            !extra values
            call calculateCrossSectionalAreas()
            Ix = atan(kurtosisDhdx) !The angle in x-direction
            Iz = atan(kurtosisDhdz) !The angle in z-direction

            numberSlopeChanges = countSlopeChangesX(dhdx)
            minimumSumSlopeChanges = getMinLengthOfStreamLine(heights)
            maximumSumSlopeChanges = getMaxLengthOfStreamLine(heights)

            surfaceRatioX = calculateSurfaceRatioX(heights)
            surfaceRatioZ = calculateSurfaceRatioZ(heights)

            call writeResults(i)
            !call writeResultsToPltFile("", i)
            
            deallocate(Amp)
            deallocate(Phi)

        end do        
    end subroutine calculateParameters

    subroutine allocateAllocatables()
        allocate(heights(resolution, resolution))

        !Derivations
        allocate(dhdx(resolution, resolution))
        allocate(dhdz(resolution, resolution))
        allocate(dhdxdx(resolution, resolution))
        allocate(dhdxdz(resolution, resolution))
        allocate(dhdzdz(resolution, resolution))

    end subroutine allocateAllocatables

    subroutine writeHeader()
        open(unit = 25, file = fileNameResult)
        write(25, *) &
        trim("simulationName,crossSectionalAreaX,crossSectionalAreaZ,Ix,Iz,&
        numberSlopeChanges,minimumSumSlopeChanges,maximumSumSlopeChanges,&
        maxHeight,minHeight,meanHeight,meanAbsoluteHeight,AADHeight,rmsHeight,skewnessHeight,kurtosisHeight,&
        maxDhdx,minDhdx,meanDhdx,meanAbsoluteDhdx,AADDhdx,rmsDhdx,skewnessDhdx,kurtosisDhdx,&
        maxDhdz,minDhdz,meanDhdz,meanAbsoluteDhdz,AADDhdz,rmsDhdz,skewnessDhdz,kurtosisDhdz,&
        maxDhdxdx,minDhdxdx,meanDhdxdx,meanAbsoluteDhdxdx,AADDhdxdx,rmsDhdxdx,skewnessDhdxdx,kurtosisDhdxdx,&
        maxDhdzdz,minDhdzdz,meanDhdzdz,meanAbsoluteDhdzdz,AADDhdzdz,rmsDhdzdz,skewnessDhdzdz,kurtosisDhdzdz,&
        maxDhdxdz,minDhdxdz,meanDhdxdz,meanAbsoluteDhdxdz,AADDhdxdz,rmsDhdxdz,skewnessDhdxdz,kurtosisDhdxdz,&
        xPosMaxHeight,jPosMaxHeight,xPosMinHeight,jPosMinHeight,&
        xPosMaxDhdx,zPosMaxDhdx,xPosMinDhdx,zPosMinDhdx,&
        xPosMaxDhdz,zPosMaxDhdz,xPosMinDhdz,zPosMinDhdz,&
        maxSlopeRightFromMaximum,slopeBeforeMaximum,maxSlopeLeftFromMaximum,maxSlopeAfterMaximum,&
        maxSlopeAfterMinimumAfterMaximum,&
        surfaceRatioX, surfaceRatioY,&
        xPosOfMinimumAfterMaximumInAreaBehind,yPosOfMinimumAfterMaximumInAreaBehind,minimumAfterMaximumInAreaBehind")
    end subroutine writeHeader

    subroutine writeResults(simulationID)
        integer, intent(in) :: simulationID

        open(unit = 25, file = fileNameResult)
        write(25, '(A6)', advance = "no")  "CEQ-3-"
        write(25, '(I0.5, a)', advance = "no")  simulationID, ","
        write (25, *) crossSectionalAreaX, ",", crossSectionalAreaZ,",",Ix,",",Iz,",",&
        numberSlopeChanges,",",minimumSumSlopeChanges,",",maximumSumSlopeChanges,",",&
        maxHeight,",",minHeight,",",meanHeight,",",MAHeight,",",AADHeight,",",rmsHeight,",",skewnessHeight,",",kurtosisHeight,",",&
        maxDhdx,",",minDhdx,",",meanDhdx,",",MADhdx,",",AADDhdx,",",rmsDhdx,",",skewnessDhdx,",",kurtosisDhdx,",",&
        maxDhdz,",",minDhdz,",",meanDhdz,",",MADhdz,",",AADDhdz,",",rmsDhdz,",",skewnessDhdz,",",kurtosisDhdz,",",&
        maxDhdxdx,",",minDhdxdx,",",meanDhdxdx,",",MADhdxdx,",",AADDhdxdx,",",rmsDhdxdx,",",skewnessDhdxdx,",",kurtosisDhdxdx,",",&
        maxDhdzdz,",",minDhdzdz,",",meanDhdzdz,",",MADhdzdz,",",AADDhdzdz,",",rmsDhdzdz,",",skewnessDhdzdz,",",kurtosisDhdzdz,",",&
        maxDhdxdz,",",minDhdxdz,",",meanDhdxdz,",",MADhdxdz,",",AADDhdxdz,",",rmsDhdxdz,",",skewnessDhdxdz,",",kurtosisDhdxdz,",",&
        xPosMaxHeight,",",zPosMaxHeight,",",xPosMinHeight,",",zPosMinHeight,",",&
        xPosMaxDhdx,",",zPosMaxDhdx,",",xPosMinDhdx,",",zPosMinDhdx,",",&
        xPosMaxDhdz,",",zPosMaxDhdz,",",xPosMinDhdz,",",zPosMinDhdz,",",&
        maxSlopeRightFromMaximum,",",maxSlopeBeforeMaximum,",",maxSlopeLeftFromMaximum,",",maxSlopeAfterMaximum,",",&
        maxSlopeAfterMinimumAfterMaximum,",",&
        surfaceRatioX,",",surfaceRatioZ,",",&
        xPosOfMinimumAfterMaximumInAreaBehind,",",yPosOfMinimumAfterMaximumInAreaBehind,",",minimumAfterMaximumInAreaBehind

    end subroutine writeResults
    
    subroutine calculateHeightsAndDerivations()
        real*8 :: t2e, t2i, t1e, t1i, thetar_in, N_hperiod_stream
        real*8 :: z2, x2, z, x
        integer :: i

        N_hperiod_stream = 2

        D_phi = (2 * pi) / 130.
        thetar_end = 0.6576
        D_thetar = 1 * (2 * pi) / 130 * sin(thetar_end)

        ! START end
        t2e = thetar_end

        t2i = t2e - D_thetar / 10.
        thetar_in = thetar_end - 0.5 * N_hperiod_stream * D_thetar
        t1e = thetar_in
        t1i = t1e + D_thetar / 10.

        !print *, t2e, t2i, t1e, t1i
        do i = 1, resolution
            z = zMin
            do j = 1, resolution
                x = xMin + 1. * (i - 1) / (1. * resolution - 1.) * (xMax - xMin)
                z = zMin + 1. * (j - 1) / (1. * resolution - 1.) * lambda0
                heights(i, j) = 0
                dhdx(i, j) = 0
                dhdz(i, j) = 0
                do m = 1, mMax
                    do n = 1, nMax
                        x2 = x / 1000.
                        z2 = z / 1000.
                        radius = 5.98172331
                        theta = (x2 + 5.98172331 * 0.615729034) / radius
                        phis = z2 / (radius * sin(theta))
                        heights(i, j) = heights(i, j) + Amp(n, m) * SIN((2. * PI * m * phis / D_phi) + &
                                (2. * PI * n * (theta - thetar_end) / D_thetar) + Phi(n, m))
                        
                        !First derivative
                        dhdx(i, j) = dhdx(i, j) + Amp(n, m) * COS((2. * PI * m * phis / D_phi) + &
                                (2. * PI * n * (theta - thetar_end) / D_thetar) + Phi(n, m)) * (2. * PI * m / D_phi)


                        !Second derivative dh dx dx
                        dhdxdx(i, j) = dhdxdx(i, j) + Amp(n, m) * -1 * SIN((2. * PI * m * phis / D_phi) + &
                                (2. * PI * n * (theta - thetar_end) / D_thetar) + Phi(n, m)) * (2. * PI * m / D_phi)**2
                       
                        !Second derivative dh dx dz
                        dhdxdz(i, j) = dhdxdz(i, j) + Amp(n, m) * -1 * SIN((2. * PI * m * phis / D_phi) + &
                                (2. * PI * n * (theta - thetar_end) / D_thetar) + Phi(n, m)) * (2. * PI * m / D_phi) * &
                                (2. * PI * n / D_thetar)

                        !First derivative dh dz
                        dhdz(i, j) = dhdz(i, j) + Amp(n, m) * COS((2. * PI * m * phis / D_phi) + &
                                (2. * PI * n * (theta - thetar_end) / D_thetar) + Phi(n, m)) * &
                                (2. * PI * n / D_thetar)

                        !Second derivative dh dz dz
                        dhdzdz(i, j) = dhdzdz(i, j) + Amp(n, m) * -1 * SIN((2. * PI * m * phis / D_phi) + &
                                (2. * PI * n * (theta - thetar_end) / D_thetar) + Phi(n, m)) * &
                                (2. * PI * n / D_thetar)**2

                    end do
                end do
            end do
        end do
    end subroutine calculateHeightsAndDerivations


    subroutine calculateCrossSectionalAreas()
        real * 8 :: maxHeight = 0

        crossSectionalAreaX = 0.0
        do i = 1, resolution
            maxHeight = -100000
            do j = 1, resolution
                if (heights(i, j) > maxHeight) then 
                    maxHeight = heights(i, j)
                end if
            end do
            crossSectionalAreaX = crossSectionalAreaX + maxHeight
        end do

        crossSectionalAreaZ = 0.0
        do j = 1, resolution
            maxHeight = -100000
            do i = 1, resolution
                if (heights(i, j) > maxHeight) then 
                    maxHeight = heights(i, j)
                end if
            end do
            crossSectionalAreaZ = crossSectionalAreaZ + maxHeight
        end do

    end subroutine calculateCrossSectionalAreas

    function calculateRMS(values, averageValue)result(rms)
        real*8, allocatable, intent(in) :: values(:,:)
        real*8, intent(in) :: averageValue
        real*8 :: rms
        rms = 0.0

        do i = 1, resolution
            do j = 1, resolution
                rms = rms + (values(i, j) - averageValue)**2
            end do
        end do
        rms = sqrt(rms/ (resolution ** 2))
    end function calculateRMS

    function calculateSkewness(values, averageValue, rms)result(skewness)
        real*8, allocatable, intent(in) :: values(:,:)
        real*8, intent(in) :: averageValue
        real*8, intent(in) :: rms

        real*8 :: skewness
        skewness= 0.0

        do i = 1, resolution
            do j = 1, resolution
                skewness = skewness + (values(i, j) - averageValue)**3 / (rms ** 3)
            end do
        end do
        skewness = skewness / ((resolution ** 2))
    end function calculateSkewness

    function calculateKurtosis(values, averageValue, rms)result(kurtosis)
        real*8, allocatable, intent(in) :: values(:,:)
        real*8, intent(in) :: averageValue
        real*8, intent(in) :: rms

        real*8 :: kurtosis
        kurtosis = 0.0

        do i = 1, resolution
            do j = 1, resolution
                kurtosis = kurtosis + ((values(i, j) - averageValue)**4) / (rms **4 )
            end do
        end do
        kurtosis = kurtosis / ((resolution ** 2))
        
    end function calculateKurtosis

    function calculateAverageAbsoluteDeviation(values, averageValue)result(averageAbsoluteDeviation)
        real*8, allocatable, intent(in) :: values(:,:)
        real*8, intent(in) :: averageValue
        real*8 :: averageAbsoluteDeviation 
        
        averageAbsoluteDeviation = 0.0

        do i = 1, resolution
            do j = 1, resolution
                averageAbsoluteDeviation = averageAbsoluteDeviation + abs(values(i, j) - averageValue)
            end do
        end do

        averageAbsoluteDeviation = averageAbsoluteDeviation / (resolution**2)
    end function calculateAverageAbsoluteDeviation 

    function getMeanAbsoluteValue(values)result(meanValue)        
        real*8, allocatable, intent(in) :: values(:,:)
        real*8 :: meanValue 
        
        meanValue = 0.0

        do i = 1, resolution
            do j = 1, resolution
                meanValue = meanValue + abs(values(i, j) / (resolution**2))
            end do
        end do       
    end function getMeanAbsoluteValue

    function getMeanValue(values)result(meanValue)        
        real*8, allocatable, intent(in) :: values(:,:)
        real*8 :: meanValue 
        
        meanValue = 0.0

        do i = 1, resolution
            do j = 1, resolution
                meanValue = meanValue + (values(i, j) / (resolution**2))
            end do
        end do       
    end function getMeanValue

    function getMaxValue(values)result(maxValue)        
        real*8, allocatable, intent(in) :: values(:,:)
        real*8 :: maxValue 
        maxValue = -10000000
        
        do i = 1, resolution
            do j = 1, resolution
                !maxValues
                if(values(i, j) > maxValue)then
                    maxValue = values(i, j)
                endif
            end do
        end do
    end function getMaxValue

    function getValuePositionI(values, valueWithSearchedPosition)result(iPosition)        
        real*8, allocatable, intent(in) :: values(:,:)
        real*8, intent(in) :: valueWithSearchedPosition
        integer :: iPosition 
        iPosition = -1
        
        do i = 1, resolution
            !For performance reasons we jump out of the loop if the searched value was found
            if(iPosition .ne. -1) then
                exit
            end if
            do j = 1, resolution
                if(values(i, j) .eq. valueWithSearchedPosition)then
                    iPosition = i
                    exit 
                endif
            end do
        end do
    end function getValuePositionI

    function getValuePositionJ(values, valueWithSearchedPosition)result(jPosition)        
        real*8, allocatable, intent(in) :: values(:,:)
        real*8, intent(in) :: valueWithSearchedPosition
        integer :: jPosition 
        jPosition = -1
        
        do i = 1, resolution
            !For performance reasons we jump out of the loop if the searched value was found
            if(jPosition .ne. -1) then
                exit
            end if
            do j = 1, resolution
                if(values(i, j) .eq. valueWithSearchedPosition)then
                    !print *, values(i, j) ," ", valueWithSearchedPosition, " ", i, " ", j
                    jPosition = j
                    exit 
                endif
            end do
        end do
    end function getValuePositionJ

    function getMinValue(values) result(minValue)        
        real*8, allocatable, intent(in) :: values(:,:)
        real*8 :: minValue
        minValue = 1000000
        
        do i = 1, resolution
            do j = 1, resolution
                !minValues
                if(values(i, j) < minValue)then
                    minValue = values(i, j)
                endif
            end do
        end do
    end function getMinValue

    function getMaxSlopeAfterMaximum(heights, xPosMaximum, zPosMaximum)result(maxSlope)
        real*8, allocatable, intent(in) :: heights(:,:)
        integer, intent(in) :: xPosMaximum
        integer, intent(in) :: zPosMaximum
        real*8 :: maxSlope
        real*8 :: heightBefore
        integer :: i
        
        maxSlope = 0.0
        heightBefore = heights(xPosMaximum, zPosMaximum)
        do i = xPosMaximum+1, resolution
            if(heightBefore < heights(i, zPosMaximum)) then
                exit
            end if
            if(abs(maxSlope) < abs(dhdx(i, zPosMaximum))) then
                maxSlope = (dhdx(i, zPosMaximum))
            end if
            heightBefore = heights(i, zPosMaximum) 
        end do 
    end function getMaxSlopeAfterMaximum

    function getMaxSlopeBeforeMaximum(heights, xPosMaximum, zPosMaximum)result(maxSlope)
        real*8, allocatable, intent(in) :: heights(:,:)
        integer, intent(in) :: xPosMaximum
        integer, intent(in) :: zPosMaximum
        real*8 :: maxSlope
        real*8 :: heightBefore
        integer :: i
        
        maxSlope = 0.0
        heightBefore = heights(xPosMaximum, zPosMaximum)
        do i = xPosMaximum-1, 1, -1
            if(heightBefore < heights(i, zPosMaximum)) then
                exit
            end if
            if(abs(maxSlope) < abs(dhdx(i, zPosMaximum))) then
                maxSlope = (dhdx(i, zPosMaximum))
            end if
            heightBefore = heights(i, zPosMaximum) 
        end do 
    end function getMaxSlopeBeforeMaximum

    function getMaxSlopeRightFromMaximum(heights, xPosMaximum, zPosMaximum)result(maxSlope)
        real*8, allocatable, intent(in) :: heights(:,:)
        integer, intent(in) :: xPosMaximum
        integer, intent(in) :: zPosMaximum
        real*8 :: maxSlope
        real*8 :: heightBefore
        integer :: j
        
        maxSlope = 0.0
        heightBefore = heights(xPosMaximum, zPosMaximum)
        do j = zPosMaximum+1, resolution
            if(heightBefore < heights(xPosMaximum, j)) then
                exit
            end if
            if(abs(maxSlope) < abs(dhdz(xPosMaximum, j))) then
                maxSlope = (dhdz(xPosMaximum, j))
            end if
            heightBefore = heights(xPosMaximum, j) 
        end do 
    end function getMaxSlopeRightFromMaximum

    function getMaxSlopeLeftFromMaximum(heights, xPosMaximum, zPosMaximum)result(maxSlope)
        real*8, allocatable, intent(in) :: heights(:,:)
        integer, intent(in) :: xPosMaximum
        integer, intent(in) :: zPosMaximum
        real*8 :: maxSlope
        real*8 :: heightBefore
        integer :: j
        
        maxSlope = 0.0
        heightBefore = heights(xPosMaximum, zPosMaximum)
        do j = zPosMaximum-1, 1, -1
            if(heightBefore < heights(xPosMaximum, j)) then
                exit
            end if
            if(abs(maxSlope) < abs(dhdz(xPosMaximum, j))) then
                maxSlope = (dhdz(xPosMaximum, j))
            end if
            heightBefore = heights(xPosMaximum, j) 
        end do 
    end function getMaxSlopeLeftFromMaximum

    function getXPosOfMinimumAfterMaximum(heights, xPosMaximum, zPosMaximum)result(currentXposition)
        real*8, allocatable, intent(in) :: heights(:,:)
        integer, intent(in) :: xPosMaximum
        integer, intent(in) :: zPosMaximum
        real*8 :: heightBefore
        integer :: currentXposition

        currentXposition = xPosMaximum + 1
        heightBefore = heights(xPosMaximum, zPosMaximum)
        do i = xPosMaximum+1, resolution
            if(heightBefore < heights(i, zPosMaximum)) then
                exit
            end if
            heightBefore = heights(i, zPosMaximum) 
            currentXposition = i
        end do 
    end function getXPosOfMinimumAfterMaximum

    function getMaxSlopeAfterMinimum(heights, xPosMaximum, zPosMaximum)result(maxSlope)
        real*8, allocatable, intent(in) :: heights(:,:)
        integer, intent(in) :: xPosMaximum
        integer, intent(in) :: zPosMaximum
        real*8 :: maxSlope
        real*8 :: heightBefore
        integer :: i
        
        maxSlope = 0.0
        heightBefore = heights(xPosMaximum, zPosMaximum)
        do i = xPosMaximum+1, resolution
        !print * , heights(i, zPosMaximum)
            if(heightBefore > heights(i, zPosMaximum)) then
                !print * , "Test"
                exit
            end if
            if(abs(maxSlope) < abs(dhdx(i, zPosMaximum))) then
                maxSlope = (dhdx(i, zPosMaximum))
            end if
            heightBefore = heights(i, zPosMaximum) 
        end do 
    end function getMaxSlopeAfterMinimum

    function getMaxSlopeBeforeMinimum(heights, xPosMaximum, zPosMaximum)result(maxSlope)
        real*8, allocatable, intent(in) :: heights(:,:)
        integer, intent(in) :: xPosMaximum
        integer, intent(in) :: zPosMaximum
        real*8 :: maxSlope
        real*8 :: heightBefore
        integer :: i
        
        maxSlope = 0.0
        heightBefore = heights(xPosMaximum, zPosMaximum)
        do i = xPosMaximum-1, 1, -1
            if(heightBefore > heights(i, zPosMaximum)) then
                exit
            end if
            if(abs(maxSlope) < abs(dhdx(i, zPosMaximum))) then
                maxSlope = (dhdx(i, zPosMaximum))
            end if
            heightBefore = heights(i, zPosMaximum) 
        end do 
    end function getMaxSlopeBeforeMinimum

    subroutine findMinimumAfterMaximumInAreaBehind(heights, xPosMaximum, yPosMaximum)
        real*8, allocatable, intent(in) :: heights(:,:)
        integer, intent(in) :: xPosMaximum
        integer, intent(in) :: yPosMaximum
        !real*8 :: minimum = 100000000
        integer :: searchAreaInPoints = 50
        integer :: startSearchPosX, endSearchPosX, endSearchPosY

        startSearchPosX = max(xPosMaximum - searchAreaInPoints, 0)
        endSearchPosX = min(xPosMaximum + searchAreaInPoints, resolution)
        endSearchPosY = min(yPosMaximum + searchAreaInPoints, resolution)
        minimumAfterMaximumInAreaBehind = 1000000000.0

        do i = startSearchPosX, endSearchPosX
            do j = yPosMaximum, endSearchPosY
                if(heights(i, j) < minimumAfterMaximumInAreaBehind)then
                    minimumAfterMaximumInAreaBehind = heights(i, j)
                    xPosOfMinimumAfterMaximumInAreaBehind = i
                    yPosOfMinimumAfterMaximumInAreaBehind = j 
                end if 
            end do
        end do
    end subroutine findMinimumAfterMaximumInAreaBehind
  
    function countSlopeChangesX(dhdx)result(sumOfChanges)
        real*8, allocatable, intent(in) :: dhdx(:,:)
        integer :: sumOfChanges
        logical :: wasPositive, isPositive
        
        wasPositive = .False.
        sumOfChanges = 0
        do i = 1, resolution
            do j = 1, resolution
                if(j == 1) then
                    wasPositive = dhdx(i,j) > 0
                else 
                    isPositive = dhdx(i,j) > 0
                    if(isPositive .neqv. wasPositive)then
                        sumOfChanges = sumOfChanges + 1
                        wasPositive = isPositive
                    end if 
                end if
            enddo
        enddo
    end function countSlopeChangesX

    function getMinLengthOfStreamLine(heights)result(minimumSumChanges)
        real*8, allocatable, intent(in) :: heights(:,:)
        real*8 :: minimumSumChanges
        real*8 :: sumOfChanges
        integer, parameter :: searchAreaInPoints = 50

        minimumSumChanges = 100000000
        sumOfChanges = 0
        do i = 1, resolution
            do j = 2, resolution
                sumOfChanges = sumOfChanges + abs(heights(i, j-1)-heights(i, j))           
            end do
            !get minimum 
            if(sumOfChanges < minimumSumChanges) then
                minimumSumChanges = sumOfChanges
            end if
        end do
    end function getMinLengthOfStreamLine

    function getMaxLengthOfStreamLine(heights)result(maximumSumChanges)
        real*8, allocatable, intent(in) :: heights(:,:)
        real*8 :: maximumSumChanges
        real*8 :: sumOfChanges
        integer, parameter :: searchAreaInPoints = 50

        maximumSumChanges = 0
        sumOfChanges = 0
        do i = 1, resolution
            do j = 2, resolution
                sumOfChanges = sumOfChanges + abs(heights(i, j-1)-heights(i, j))           
            end do
            !get minimum 
            if(sumOfChanges > maximumSumChanges) then
                maximumSumChanges = sumOfChanges
            end if
        end do
    end function getMaxLengthOfStreamLine

   function calculateSurfaceRatioX(heights)result(surfaceSum)
        real*8, allocatable, intent(in) :: heights(:,:)
        real*8 :: surfaceSum,surfaceX
        real*8 :: deltaY
        real*8 :: deltaXSquared
        
        surfaceSum = 0
        surfaceX = 0
        deltaXSquared = stepSizeX*0.1 * stepSizeX*0.1

        do i = 2, resolution
            do j = 1, resolution
               ! print *, "dx:",stepSizeX*0.1,"dy",deltaY
                deltaY = abs(heights(i-1,j) - heights(i,j))
                surfaceSum = surfaceSum + sqrt(deltaXSquared + deltaY * deltaY)
                surfaceX = surfaceX + stepSizeX*0.1
            end do
        end do
        surfaceSum = surfaceSum / surfaceX 
    end function calculateSurfaceRatioX

    function calculateSurfaceRatioZ(heights)result(surfaceSum)
        real*8, allocatable, intent(in) :: heights(:,:)
        real*8 :: surfaceSum,surfaceX
        real*8 :: deltaY
        real*8 :: deltaZSquared
        
        surfaceSum = 0
        surfaceX = 0
        deltaZSquared = stepSizeZ*0.1 * stepSizeZ*0.1
        do i = 1, resolution
            do j = 2, resolution
                deltaY = abs(heights(i,j-1) - heights(i,j))
                surfaceSum = surfaceSum + sqrt(deltaZSquared + deltaY * deltaY)
                surfaceX = surfaceX +stepSizeZ*0.1
            end do
        end do
        surfaceSum = surfaceSum / surfaceX      
    end function calculateSurfaceRatioZ


    subroutine readAmpAndPhifromFile(fileName)
        character (*), intent(in)  :: fileName
        open (unit = 14, file = fileName, status = 'old', action = 'read')
        read(14, *) mMax, nMax
        allocate(Amp(mMax, nMax))
        allocate(Phi(mMax, nMax))
        do m = 1, mMax
            do n = 1, nMax
                read (14, *) Amp(n, m), Phi(n, m)
            enddo
        enddo
        close(14)
    end subroutine readAmpAndPhifromFile

    subroutine writeHeightsToFile(fileName)
        character (*), intent(in) :: fileName
        !real * 8 :: x 
        open(11, file = filename, action = 'write')
        !write(10,'(i1, X, i1)') mMax, nMax
        write (11, *) 'TITLE ="hght"'
        write (11, *) 'VARIABLES = "x", "y", "z","dhx","dhz","dx2","dz2","dxdz","dh"'
        write (11, *)  'ZONE F=BLOCK, T= "Block      1", I=', resolution, ', J=     1, K=', resolution
        do j = 1, resolution
            do i = 1, resolution
                !x = 0.081 * 1000 + 1. * (i - 1) / (1. * resolution - 1.) * lambda0
                !                z = zMin+1.*(j-1)/(1.*resolution-1.)*lambda0
                !write (11, *) 1. * x / 1000. * 0.7 + 0.01
            end do
        end do
        do j = 1, resolution
            do i = 1, resolution
                !               x = 1.*(i-1)/(1.*resolution-1.)*lambda0
                !               z = zMin+1.*(j-1)/(1.*resolution-1.)*lambda0
                !write (11, *)  1 * heights(i, j) / 10000.
            end do
        end do
        do j = 1, resolution
            do i = 1, resolution
                !z = zMin + 1. * (j - 1) / (1. * resolution - 1.) * lambda0
                !write (11, *)  1. * z / 1000.
            end do
        end do
        do j = 1, resolution
            do i = 1, resolution
                !write (11, *)  dhx(i, j)
            end do
        end do
        do j = 1, resolution
            do i = 1, resolution
                !write (11, *)  dhz(i, j)
            end do
        end do
        do j = 1, resolution
            do i = 1, resolution
                !write (11, *)  dx2(i, j)
            end do
        end do
        do j = 1, resolution
            do i = 1, resolution
                !write (11, *)  dz2(i, j)
            end do
        end do
        do j = 1, resolution
            do i = 1, resolution
                !write (11, *)  dhdx(i, j)
            end do
        end do
        do j = 1, resolution
            do i = 1, resolution
                !write (11, *)  dh(i, j)
            end do
        end do
        close(11)
    end subroutine writeHeightsToFile

    !Code from https://stackoverflow.com/questions/1262695/convert-integers-to-strings-to-create-output-filenames-at-run-time
    !Casts an integer to a char array
    function itoa(i) result(res)
        character(:),allocatable :: res
        integer,intent(in) :: i
        character(range(i)+2) :: tmp
        write(tmp,'(i0)') i
        res = trim(tmp)
    end function itoa

	!Saves the slice which is stored in the global variable "slice" to the plt-file "Slice.plt"
	subroutine writeResultsToPltFile(path, index)
		character(len=200), intent(in) :: path
        integer, intent(in) :: index 
		integer :: j,k

		character(len=200) :: pathToSlice
		
		pathToSlice = trim(trim("Parameter_") // trim(itoa(index)) // trim("_.plt"))
        !pathToSlice = trim(trim(path) // trim("Parameter_") // trim(_".plt"))

		open(unit=15,file=pathToSlice)			
	
		write (15,*) 'VARIABLES = "height", "dhdx", "dhdz","dhdxdx"'
		!,"derivationsW_Y_parabola","derivationsV_Z_parabola","vorticity_parabola",&
		!"derivationsW_Y_linear","derivationsV_Z_linear","vorticity_linear"'
	

		write (15,*) 'ZONE F=BLOCK, T= "Block',1,'",I=',1,', J=',resolution,', K=', resolution
		
        !
		do k=1, resolution			
			do j=1,resolution
				write (15,*)  heights(j,k)	
			enddo		
		enddo

        do k=1, resolution			
			do j=1,resolution
				write (15,*)  dhdx(j,k)	
			enddo		
		enddo

        do k=1, resolution			
			do j=1,resolution
				write (15,*)  dhdz(j,k)	
			enddo		
		enddo

        do k=1, resolution			
			do j=1,resolution
				write (15,*)  dhdxdx(j,k)	
			enddo		
		enddo

		close(15)
	end subroutine writeResultsToPltFile

end program calculatePlane