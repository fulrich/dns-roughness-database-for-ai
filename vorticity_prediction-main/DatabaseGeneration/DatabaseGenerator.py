import os
import pickle

import numpy
import pandas as pd
import glob
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures, StandardScaler, MinMaxScaler, RobustScaler

import numpy as np

from GlobalCode.ImportantGlobalPaths import pathToDataFolder

"""
This file can handle three kinds of CSV files:
fst. kind: MaxVorticitiesDataBaseFile containing the SimulationName and the maxVorticity and many other variables
scd. kind: HeightsProfileFile (fromat: simulationIndex, height1, height2,..., heightn
thrd. kind ParameterFile 
These two databases can be joined by the simulationIndex and the Simulation name. In most of the cases there will
be way more HeightsProfiles than maxVorticity entries because not all simulations convergenced
"""


def createMergedDataBase(targetDF, inputParameterDF, nameTargetRegressionValue="maxVorticity"):
    """
    Merges the dataFrame containing the regression parameter with the dataFrame containing the inputParameters. There
    might be much more rows in the inputParameterDF as we have rows in the targetDF. The merged data frame only contains
    as many rows as the targetDF. The rows are matched according the simulationName column, which has of course be
    contained in both dataFrames

    Args:
        targetDF: The dataframe containing the value which should be predicted
        inputParameterDF: The dataframe containing the parameters
        nameTargetRegressionValue: The column name of the value on which the regression should be done

    Returns:
        mergedDF: The merged DataFrame
    """

    nameAndTargetPair = targetDF[["simulationName", nameTargetRegressionValue]].copy()
    mergedDf = nameAndTargetPair.merge(inputParameterDF, how='left', on="simulationName")
    return mergedDf


def readHeightsFile(pathToHeightsFile):
    """
    Creates for a Pandas data frame containing mapping each simulationName to one heightProfile

    Args:
        pathToHeightsFile: The path to the CSV-file containing the simulationNames with their corresponding
        heightProfiles

    Returns:
        resultDF: A pandas data frame mapping each simulationName to its corresponding heightProfile
    """

    heightsDF = pd.read_csv(pathToHeightsFile, skipinitialspace=True, header=None)
    names = []
    heights = []
    for index, row in heightsDF.iterrows():
        names.append(row[0])
        heights.append(row[1:])
    heightsFloat = []
    for dataSet in heights:
        heightsFloat.append(list(map(float, dataSet)))

    print(heightsFloat[1])
    print(heights[0])
    resultDF = pd.DataFrame({'simulationName': names, 'heights': heightsFloat})
    print(resultDF.head())
    return resultDF


def readPandasFile(pathToParameterFile):
    parameterDf = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    return parameterDf


def createFeaturesForPolynomialRegression(xTrain, xTest, degree, interactionOnly=True):
    poly = PolynomialFeatures(degree, interaction_only=interactionOnly)
    xTrainTransformed = poly.fit_transform(xTrain)
    xTestTransformed = poly.transform(xTest)
    return xTrainTransformed, xTestTransformed


def getTrainingDataDNN(maxVorticitiesFile, pathToParameterFile, pathToHeightsFile, inputScaler, targetScaler):
    regressionDF = pd.read_csv(maxVorticitiesFile, skipinitialspace=True)
    parameterDF = None
    if pathToParameterFile != "":
        parameterDF = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    if pathToHeightsFile != "":
        heightsDf = pd.read_csv(pathToHeightsFile, skipinitialspace=True, header=None)
        columnNames = ["simulationName"] + [str(i) for i in range(1, 1025)]
        heightsDf.columns = columnNames
        if parameterDF is not None:
            print(parameterDF.columns)
            parameterDF = parameterDF.merge(heightsDf, how='left', on="simulationName")
        else:
            parameterDF = heightsDf
        print("Test")
    print(parameterDF.columns)
    # Now we join the regression variable
    mergedDf = createMergedDataBase(regressionDF, parameterDF, "maxVorticityParabola")
    mergedDf = mergedDf.rename(columns={"maxVorticityParabola": "maxVorticity"})
    y = mergedDf[["maxVorticity"]]
    mergedDf = mergedDf.drop(columns="maxVorticity")
    x = mergedDf.drop(columns="simulationName")
    #x = x.drop(columns="yPosition")
    xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size=0.1, random_state=0)
    xTrain, xTest, input_scaler = standardizationOfTrainAndTestInput(xTrain, xTest, inputScaler)
    yTrain, targetScaler = standardizationOfTrainY(yTrain, targetScaler)
    #if useStdScaler:
    #    Train, targetScaler = standardizationOfTrainY(yTrain)
    #    return xTrain.to_numpy(), xTest.to_numpy(), yTrain.to_numpy(), yTest.to_numpy(), targetScaler
    #else:
    #    yTrain = yTrain.to_numpy() / 10000
    #    return xTrain.to_numpy(), xTest.to_numpy(), yTrain, yTest.to_numpy(), targetScaler
    #return xTrain.to_numpy(), xTest.to_numpy(), yTrain, yTest, None
    return xTrain.to_numpy(), xTest.to_numpy(), yTrain.to_numpy(), yTest.to_numpy(), targetScaler




def scaleHeightProfile(inputNormalization, arrayX):
    if inputNormalization == "0and1":
        arrayX = normalize2dArrayToValuesBetweenZeroAndOne(arrayX)
    elif inputNormalization == "-1and1":
        arrayX = normalize2dArrayToValuesBetweenMinus1and1(arrayX)
    elif inputNormalization == "std":
        arrayX = normalize2dArrayUsingMeanAndStd(arrayX)
    else:
        print("The input normalization parameter was not correct specified!")
        return None

    return arrayX


def getTestDataHeightsDatabase(pathToMaxVorticitiesFile, pathToHeightsFile, domainResolution, regressionParameter=
                                    "maxVorticityParabola", inputNormalization="-1and1", outputNormalization="std"):

    maxVorticitiesDf = pd.read_csv(pathToMaxVorticitiesFile, skipinitialspace=True)
    heightsDf = readHeightsFile(pathToHeightsFile)
    mergedDf = createMergedDataBase(maxVorticitiesDf, heightsDf, regressionParameter)
    arrayX = mergedDf['heights'].to_list()
    arrayX = np.asarray(arrayX)

    # Scaling the heightProfile
    arrayX = scaleHeightProfile(inputNormalization, arrayX)
    arrayX = reshapeArray(arrayX, domainResolution)
    arrayY = np.asarray(maxVorticitiesDf[regressionParameter])
    xTest = arrayX
    yTest = arrayY
    
    
    #, xTest, yTrain, yTest = train_test_split(arrayX, arrayY, test_size=0.1, random_state=0)

    # Scaling the target regression variable
    if outputNormalization == "0and1":
        targetScaler = MinMaxScaler()
    elif outputNormalization == "-1and1":
        targetScaler = MinMaxScaler(feature_range=(-1,1))
    elif outputNormalization == "std":
        targetScaler = StandardScaler()
    else:
        print("The output normalization parameter was not correct specified!")
        return

    targetScaler.fit(np.expand_dims(yTest, axis=1))
    #yTrain = np.squeeze(targetScaler.transform(np.expand_dims(yTrain, axis=1)))
    #yTest = np.squeeze(targetScaler.transform(np.expand_dims(yTest, axis=1)))

    #print("Min regression value training: " + str(yTrain.min()))
    #print("Max regression value training: " + str(yTrain.max()))

    return targetScaler, xTest, yTest

def getTrainingsDataHeightsDatabase(pathToMaxVorticitiesFile, pathToHeightsFile, domainResolution, regressionParameter=
                                    "maxVorticityParabola", inputNormalization="-1and1", outputNormalization="std"):

    maxVorticitiesDf = pd.read_csv(pathToMaxVorticitiesFile, skipinitialspace=True)
    heightsDf = readHeightsFile(pathToHeightsFile)
    mergedDf = createMergedDataBase(maxVorticitiesDf, heightsDf, regressionParameter)
    arrayX = mergedDf['heights'].to_list()
    arrayX = np.asarray(arrayX)

    # Scaling the heightProfile
    arrayX = scaleHeightProfile(inputNormalization, arrayX)
    arrayX = reshapeArray(arrayX, domainResolution)
    arrayY = np.asarray(maxVorticitiesDf[regressionParameter])
    arrayY = arrayY.reshape(-1, 1)
    xTrain, xTest, yTrain, yTest = train_test_split(arrayX, arrayY, test_size=0.1, random_state=0)

    # Scaling the target regression variable
    if outputNormalization == "0and1":
        targetScaler = MinMaxScaler()
    elif outputNormalization == "-1and1":
        targetScaler = MinMaxScaler(feature_range=(-1,1))
    elif outputNormalization == "std":
        targetScaler = StandardScaler()
    else:
        print("The output normalization parameter was not correct specified!")
        return

    targetScaler.fit(yTrain)
    yTrain = targetScaler.transform(yTrain)
    #yTrain = np.squeeze(targetScaler.transform(np.expand_dims(yTrain, axis=1)))
    #yTest = np.squeeze(targetScaler.transform(np.expand_dims(yTest, axis=1)))

    print("Min regression value training: " + str(yTrain.min()))
    print("Max regression value training: " + str(yTrain.max()))

    return targetScaler, xTrain, xTest, yTrain, yTest


def getInvervallValues(inputString):
    indexOfAnd = inputString.index("and")
    return 0


def coverHeightAreaWithMinusOne(xData, xPosition, yPosition, sizeX, sizeY):
    for i in range(xData.shape[0]):
        for x in range(sizeX):
            for y in range(sizeY):
                xData[i][xPosition + x][yPosition + y] = -1
    return xData


def normalize2dArrayToValuesBetweenZeroAndOne(arrayInput):
    #print(arrayInput.shape)
    minValue = np.min(arrayInput)
    maxValue = np.max(arrayInput)
    valueRange = maxValue - minValue

    for i in range(len(arrayInput)):
        for j in range(len(arrayInput[i])):
            arrayInput[i][j] = ((arrayInput[i][j] - minValue) / valueRange)
    return arrayInput


def normalize2dArrayToValuesBetweenMinus1and1_2(arrayInput):
    #print(arrayInput.shape)
    minValue = np.min(arrayInput)
    maxValue = np.max(arrayInput)
    valueRange = maxValue - minValue

    for i in range(len(arrayInput)):
        for j in range(len(arrayInput[i])):
            arrayInput[i][j] = 2 * ((arrayInput[i][j] - minValue) / valueRange) - 1
    return arrayInput


def normalize2dArrayToValuesBetweenMinus1and1(arrayInput):
    #print(arrayInput.shape)

    for i in range(len(arrayInput)):
        minValue = np.min(arrayInput[i])
        maxValue = np.max(arrayInput[i])
        valueRange = maxValue - minValue
        for j in range(len(arrayInput[i])):
            arrayInput[i][j] = 2 * ((arrayInput[i][j] - minValue) / valueRange) - 1
    return arrayInput


def normalize2dArrayUsingMeanAndStd(arrayInput):
    stdDeviation = np.std(arrayInput)
    median = np.median(arrayInput)

    resultArray = (arrayInput - median) / stdDeviation

    return resultArray


def reshapeArray(array, domainResolution):
    print(array.shape[0])
    return array.reshape(array.shape[0], domainResolution, domainResolution, 1)

def standardizationOfTrainY(yTrain, targetScaler):
    if targetScaler == "min_max_scaler":
        scaler = MinMaxScaler(feature_range=(0,1))
    elif targetScaler == "std_scaler":
        scaler = StandardScaler()
    elif targetScaler == "robust_scaler":
        scaler = RobustScaler()
    else:
        scaler = None

    scaler = StandardScaler()
    yTrain = pd.DataFrame(
        scaler.fit_transform(yTrain),
        columns=yTrain.columns
    )
    return yTrain, scaler

def standardizationOfTrainAndTestInput(xTrain, xTest, inputScaler):
    if inputScaler == "min_max_scaler":
        scaler = MinMaxScaler(feature_range=(0,1))
    elif inputScaler == "std_scaler":
        scaler = StandardScaler()
    elif inputScaler == "robust_scaler":
        scaler = RobustScaler()
    else:
        scaler = None

    xTrain = pd.DataFrame(
        scaler.fit_transform(xTrain),
        columns=xTrain.columns)

    xTest = pd.DataFrame(
        scaler.transform(xTest),
        columns=xTest.columns)

    return xTrain, xTest, scaler


def getParameterNames(pathToParameterFile):
    parameterDf = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    parameterDf = parameterDf.drop(columns="simulationName")
    return parameterDf.columns

def getTrainingDataWithoutCertainParameters(maxVorticitiesFile, pathToParameterFile):
    omittedParameterListNegativeMPE = ['kurtosisDhdxdz', 'rmsDhdzdz', 'meanAbsoluteDhdzdz', 'AADDhdzdz',
                            'maxDhdzdz', 'maxHeight', 'minDhdzdz', 'rmsDhdxdz', 'AADDhdxdz',
                            'meanAbsoluteDhdxdz', 'minDhdxdx', 'maxDhdxdz', 'rmsDhdxdx', 'minDhdxdz',
                            'AADDhdxdx', 'maxDhdxdx', 'kurtosisDhdxdx', 'meanAbsoluteDhdxdx',
                            'kurtosisDhdzdz', 'meanDhdxdx']
    omittedParameterListNumberMoreThanHalfGotBad = ['meanDhdxdx', 'kurtosisDhdxdx', 'maxHeight',
                                        'kurtosisDhdzdz', 'minDhdxdz', 'maxDhdxdz', 'AADDhdxdx',
                                        'meanAbsoluteDhdxdz', 'AADDhdxdz', 'meanAbsoluteDhdxdx',
                                        'maxDhdxdx', 'rmsDhdxdz', 'rmsDhdxdx', 'minDhdxdx', 'kurtosisDhdxdz',
                                        'minDhdzdz', 'maxDhdzdz', 'meanDhdxdz', 'skewnessDhdxdx', 'meanDhdzdz',
                                        'AADDhdzdz', 'meanAbsoluteDhdzdz', 'rmsDhdzdz', 'meanAbsoluteDhdz',
                                        'AADHeight', 'AADDhdz', 'meanAbsoluteHeight']

    omittedParameterList = list(set(omittedParameterListNumberMoreThanHalfGotBad + omittedParameterListNegativeMPE))
    maxVorticitiesDf = pd.read_csv(maxVorticitiesFile, skipinitialspace=True)
    maxVorticitiesDf.rename(columns={'maxVorticityParabola': 'maxVorticity'}, inplace=True)
    parameterDf = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    mergedDf = createMergedDataBase(maxVorticitiesDf, parameterDf)
    mergedDf = mergedDf.drop(omittedParameterList, axis=1)

    y = mergedDf[["maxVorticity"]]
    mergedDf = mergedDf.drop(columns="maxVorticity")
    x = mergedDf.drop(columns="simulationName")



    y = np.asarray(y)
    y = np.asarray(y) / 10000
    xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size=0.1, random_state=0)
    xTrain, xTest = standardizationOfTrainAndTestInput(xTrain, xTest)
    #yTrain, yTest = standardizationOfTrainAndTestInput(yTrain, yTest)
    return xTrain, xTest, yTrain, yTest

def getTrainingsDataForAutoEncoder(pathToHeightsFile, domainResolution, inputNormalization="std"):
    '''
    Important the autoencoder should get output-values in the range between 0and1 !!!!!!!!
    Args:
        pathToHeightsFile:
        domainResolution:
        inputNormalization:

    Returns:

    '''

    heightsDf = readHeightsFile(pathToHeightsFile)
    arrayX = np.asarray(heightsDf['heights'].to_list())
    arrayX = scaleHeightProfile(inputNormalization, arrayX)
    arrayX = reshapeArray(arrayX, domainResolution)

    #We transform the y-values between ZeroAndOne
    arrayY = scaleHeightProfile("0and1", np.copy(arrayX))
    arrayY = reshapeArray(arrayY, domainResolution)
    xTrain, xTest, yTrain, yTest = train_test_split(arrayX, arrayY, test_size=0.1, random_state=0)

    return xTrain, xTest, yTrain, yTest

def getTrainingsDataForAutoEncoder2(pathToHeightsFile, domainResolution):
    heightsDf = readHeightsFile(pathToHeightsFile)
    arrayX = np.asarray(heightsDf['heights'].to_list())
    arrayX = normalize2dArrayToValuesBetweenMinus1and1(arrayX)
    arrayX = reshapeArray(arrayX, domainResolution)
    arrayY = (arrayX + 1.0)/2
    xTrain, xTest, yTrain, yTest = train_test_split(arrayX, arrayY, test_size=0.1, random_state=0)
    #xTrain, xTest = applyPreprocessingToNumpyInput(xTrain, xTest)
    return xTrain, xTest, yTrain, yTest

def applyPreprocessingToNumpyInput(xDataTrain, xDataTest):
    scaler = StandardScaler()
    xTrainTransformed = scaler.fit_transform(xDataTrain.reshape(-1, xDataTrain.shape[-1])).reshape(xDataTrain.shape)
    xTestTransformed = scaler.transform(xDataTest.reshape(-1, xDataTest.shape[-1])).reshape(xDataTest.shape)
    return xTrainTransformed, xTestTransformed

def getTrainsDataSetFromAmplitudesAndPhasesFiles(pathToMaxVortictyFile, pathToFolderOfAmpAndPhases):
    #TODO
    return 0

def generateTrainingDataForVorticityClassification(deviationFactor, pathToMaxVorticitiesFile, pathToHeightsFile, domainResolution):
    xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticitiesFile, pathToHeightsFile,
                                                                   domainResolution, True)

    stdDeviation = np.std(yTrain)
    median = np.median(yTrain)

    yTrain = calculateOneHotEncodingForVorticities(yTrain, deviationFactor, stdDeviation, median)
    yTest = calculateOneHotEncodingForVorticities(yTest, deviationFactor, stdDeviation, median)

    return xTrain, xTest, yTrain, yTest

def calculateOneHotEncodingForVorticities(vorticities, deviationFactor, stdDeviation, median):
    resultArray = np.zeros((len(vorticities), 3))
    counterArray = numpy.asarray([0,0,0,0])

    for i in range(len(vorticities)):
        #small vorticity
        if vorticities[i] < median - deviationFactor * stdDeviation:
            resultArray[i] = numpy.asarray([1,0,0])
            counterArray[0] += 1
        elif vorticities[i] > median - deviationFactor * stdDeviation:
            resultArray[i] = numpy.asarray([0,0,1])
            counterArray[2] += 1
        elif vorticities[i] < median + 0.75 * deviationFactor * stdDeviation and vorticities[i] > median - 0.75 * deviationFactor * stdDeviation:
            resultArray[i] = numpy.asarray([0,1,0])
            counterArray[1] += 1
        else:
            counterArray[3] += 1

    print("number small vorticity: " + str(counterArray[0]))
    print("number normal vorticity: " + str(counterArray[1]))
    print("number max vorticity: " + str(counterArray[2]))
    print("number not used vorticity: " + str(counterArray[3]))

    return resultArray

def saveDataSetAsNumpyArrs(xTrain, xTest, yTrain, yTest, nameOfDataSet, targetScaler=None):
    pathToDataSet = pathToDataFolder + "NumpyDatabases/" + nameOfDataSet + "/"
    if not os.path.exists(pathToDataSet):
        os.makedirs(pathToDataSet)

    np.save(pathToDataSet + "xTrain", xTrain)
    np.save(pathToDataSet + "yTrain", yTrain)
    np.save(pathToDataSet + "xTest", xTest)
    np.save(pathToDataSet + "yTest", yTest)

    if targetScaler != None:
        with open(pathToDataSet + 'targetScaler.pkl', 'wb') as outp:
            pickle.dump(targetScaler, outp, pickle.HIGHEST_PROTOCOL)


def loadNumpyDataSet(nameOfNumpyDataSet):
    pathToDataSet = pathToDataFolder + "NumpyDatabases/" + nameOfNumpyDataSet + "/"
    xTrain = np.load(pathToDataSet + "xTrain.npy")
    xTest = np.load(pathToDataSet + "xTest.npy")
    yTrain = np.load(pathToDataSet + "yTrain.npy")
    yTest = np.load(pathToDataSet + "yTest.npy")

    targetScaler = None
    if os.path.isfile(pathToDataSet + "targetScaler.pkl"):
        with open(pathToDataSet + "targetScaler.pkl", 'rb') as file:
            targetScaler = pickle.load(file)

            return xTrain, xTest, yTrain, yTest, targetScaler
    else:
        return xTrain, xTest, yTrain, yTest



#pathToParameterFile = "../Databases/ParameterDatabases/ParameterFileForAll15kSimulations.csv"
#maxVorticitiesDf = pd.read_csv("../Databases/MaxVorticityFiles/MaxVorticities_First_10_Batches.csv", skipinitialspace=True)

#heightsDf = readHeightsFile()
#parameterDf = readParameterFile()
#createMergedDataBase(maxVorticitiesDf, heightsDf)
#merged = createMergedDataBase(maxVorticitiesDf, heightsDf)
#print(merged.head())
#merged.to_csv("../Databases/ParameterDatabases/ParameterFirst10Batches.csv", index=False)
#merged.to_csv("../Databases/HeightsDatabases/First10BatchesHeightsDB.csv", index=False)




















