import pandas as pd
import glob
import sys

numberExpectedDataBaseFiles = 10

if len(sys.argv) > 2:
    print("Only one additional argument is expected")
    exit()
elif len(sys.argv) == 2:
    numberExpectedDataBaseFiles = int(sys.argv[1])

print("Number Expected files which should be concat.: " + str(numberExpectedDataBaseFiles))


def getAllDataBaseFiles(pathToFolder, nameOfDBfile):
    """
    This program is used for reading all the MaxVorticiesFile which were created by the parallel FortranAnalyzers
    and sorts them by their simulation name and Saves the resulting file
    :param pathToFolder: The path to the folder from which the CSV-files should be read
    :param nameOfDBfile: A name which should be contained by all the files which should be read
    :return: All the file paths which contain the nameOfDBfile and are csv-files
    """
    files = [f for f in glob.glob(pathToFolder + "*.csv")]
    files = [f for f in files if nameOfDBfile in f]
    return files


def readOneMaxVorticityDataBaseFile(pathToCSVfile):
    """
    Reads one maxVorticity file into a pandas data frame
    :param pathToCSVfile: The path were the csvDataBaseFile is stored
    :return: the resulting pandas dataframe
    """
    dataSet = pd.read_csv(pathToCSVfile, skipinitialspace=True)
    return dataSet


def concatDBfiles(pathToFolder, nameOfDBfiles):
    """
    Concatenates all the csv-MaxVorticityDataBaseFiles which were stored in the folder pathToFolder
    containing the name nameOfDBfiles. The resulting csv-file is also sorted by the simulationName
    :param pathToFolder: The path to the folder from which the CSV-files should be read
    :param nameOfDBfiles: A name which should be contained by all the files which should be read
    :return:
    """
    dataBasesPaths = getAllDataBaseFiles(pathToFolder, nameOfDBfiles)
    # Concatenating the PadasFrameWorks
    dataFrame = readOneMaxVorticityDataBaseFile(dataBasesPaths[0])
    for i in range(1, len(dataBasesPaths)):
        df = readOneMaxVorticityDataBaseFile(dataBasesPaths[i])
        dataFrame = pd.concat([dataFrame, df])
    dataFrame = dataFrame.sort_values("simulationName")
    dataFrame.to_csv(pathToFolder + nameOfDBfiles + "_concatenated.csv", index=False)


def methodeForFritz():
    with open('/home/thomas/Downloads/Not_Convergenced_Run_1_Batch_0_1_2_3_5_6.txt') as f:
        lines = f.readlines()
        lines.pop(0)
        numbers = []
        for line in lines:
            numberString = line[6:11]
            numbers.append(int(numberString))

        f = open('Delted_Simulations.txt', 'w')
        for n in numbers:
            f.write(str(n) + '\n')  # python will convert \n to os.linesep
        f.close()


#concatDBfiles("/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/test/", "1_6_0")