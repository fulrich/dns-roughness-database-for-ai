"""
This file is used for transforming the roughness in such way that the maximum is in the domain centre
"""
import numpy as np

from DataVisualization.DataSetPlot import saveRoughnessXdataToImage
from DatabaseGeneration.DatabaseGenerator import getTrainingsDataHeightsDatabase
domainResolution = 16


testArray = np.array([[16, 2, 3, 4, ],
                      [5, 6, 7, 8, ],
                      [9, 10, 20, 12],
                      [13, 14, 15, 4]])

def getMoveInstruction(domain, domainRes, useMaximum=True):
    """
    This function calculates the move instructions along the x- and y-direction to center
    the maximum or the minimum of the domain
    :param domain: The height profile of the domain
    :param domainRes: The domain Resolution in x- and y-diretion
    :param useMaximum: Determines if the position of the maximum or of the minimum is used
    for centering the domain
    :return:
        resultMoveX: the number of positions the domain has to be moved in x-direction
        resultMoveY: the number of positions the domain has to be moved in y-direction
    """
    if useMaximum:
        posXmax, posYmax = getMaxPosition(domain)
    else:
        posXmax, posYmax = getMinPosition(domain)
    resultMoveX = domainRes / 2 - posXmax
    resultMoveY = domainRes / 2 - posYmax
    #resultMoveX = 0
    resultMoveY = 0
    return resultMoveX, resultMoveY


def getMaxPosition(numpyArray):
    maxValue = -100000
    maxPosition = (0, 0)
    for i in range(numpyArray.shape[0]):
        for j in range(numpyArray.shape[1]):
            if numpyArray[i][j] > maxValue:
                maxPosition = (i, j)
                maxValue = numpyArray[i][j]
    return maxPosition


def getMinPosition(numpyArray):
    minValue = 100000
    minPosition = (0, 0)
    for i in range(numpyArray.shape[0]):
        for j in range(numpyArray.shape[1]):
            if numpyArray[i][j] < minValue:
                minPosition = (i, j)
                minValue = numpyArray[i][j]
    return minPosition


def moveDomain(numberStepsX, numberStepsY, domain):
    import tensorflow as tf

    #a = np.array([[1, 2, 3, 4, ],
    #              [5, 6, 7, 8, ],
    #              [9, 10, 11, 12]])
    result = tf.roll(domain, shift=[int(numberStepsX), int(numberStepsY)], axis=[0, 1])
    #print("Before:")
    #print(a)
    #print("After transformation:")
    #print(result)
    return result.numpy()

def transformWholeDataSet(wholeDataset):
    transformedDataSet = np.zeros(wholeDataset.shape)
    for i in range(wholeDataset.shape[0]):
        moveInstuctionsX, moveInstructionsY = getMoveInstruction(wholeDataset[i], domainResolution, True)
        transformedDataSet[i] = moveDomain(moveInstuctionsX, moveInstructionsY, wholeDataset[i])
        moveInstuctionsX, moveInstructionsY = getMoveInstruction(transformedDataSet[i], domainResolution, True)
        if(i < 10 ):
            saveRoughnessXdataToImage(transformedDataSet[i], "transformed_" + str(i) + ".png")
    return transformedDataSet


pathToHeightsFile = "/home/thomas/Dokumente/HiWi/vorticity_prediction/Databases/HeightsProfiles/Heights_16_16.csv"
pathToMaxVorticityFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"

#domainResolution = 4
#moveInstuctionsX, moveInstructionsY = getMoveInstruction(testArray, domainResolution, True)
#transformedArray = moveDomain(moveInstuctionsX, moveInstructionsY, testArray)


#moveDomain(-1, -2, testArray)
#transformWholeDataSet()

#analysisOfDataBaseSize()
#heatMapDataGeneration()

#xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution)
#transformWholeDataSet(xTrain)

#saveRoughnessXdataToImage(xData, nameOfImage)



