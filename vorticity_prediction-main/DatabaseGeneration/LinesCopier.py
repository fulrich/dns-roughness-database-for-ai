

def copyTheFirstNLinesFromFileToOther(file1, otherFile, numberLines):
    f = open(file1, "r")
    lines = f.readlines()

    print("Finished reading!")

    counter = 0
    with open(otherFile, 'w') as f:
        for line in lines:
            if counter == numberLines:
                break
            f.write("%s\n" % line)
            counter += 1

    print("Finished writing!")


pathToFile1 = "/home/thomas/Dokumente/HiWi/vorticity_prediction/Databases/HeightsProfiles_250k/Heights_32_32.csv"
pathToOtherFile = "/home/thomas/Dokumente/HiWi/vorticity_prediction/Databases/HeightsProfiles_100k/Heights_32_32.csv"
copyTheFirstNLinesFromFileToOther(pathToFile1, pathToOtherFile, 100000)