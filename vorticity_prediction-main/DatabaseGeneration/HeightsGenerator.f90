program calculatePlane
    implicit none
    !This programm can be used to create a certain number of random amplitudes and phases files.
    !It can also so be used to read such files and calculate the height profile of such files and save the to txt file
    real*8, allocatable :: Amp(:,:)
    real*8, allocatable :: Pha(:,:)
    real*8, allocatable :: heights(:,:),dhx(:,:) ,dhz(:,:),dh(:,:)
    real*8, allocatable :: dx2(:,:) ,dz2(:,:),dxdz(:,:),curvature(:,:),curvature2(:,:)
    character(len = 128) :: ampPhaFileName, ampPhaFileAfterCalculation, heightsFileName, dateiname
    integer :: i, j, k, mMax, nMax, m, n, resolution, maxAmpValue, currentFileIndex, numberOfNewCreatedFiles
    integer :: fullrun,skipper,skipnumber
    integer :: numberOfAmplitudesAndPhasesFiles
    integer, parameter :: MAX_LENGTH_OF_DATASET_LINE = 1000000
    real*8, parameter:: PI = 2*asin(1.) !constants
    real*8 :: lambda0, xMin, xMax, zMin, zMax, x, z, maxHeight,alpha,  maxAmp,amp1,amp2,amp3,meanAmp
    real*8::k1,k2,maxCurv,maxCurv2,maxDeriz,maxDerix,meanCurv,meanDerix,meanDeriz
    real*8 :: theta,phis,radius,D_phi,thetar_end,D_thetar,x2,z2,delta1,delta2,delta3,waveamp(3),wavePha(3)
    real*8 :: sumHeights, sumHeightsAbs, averageHeight, averageHeightAbs, proportion, proportionAbs
    real*8 :: mu_2,rms,rms2,meanA,RMSA

    character(*), parameter :: pathToSavedAmplitudes = "../Amp5/"

    currentFileIndex = 0
    lambda0 = 170. !in mm
    lambda0 = 270.
    xMin = 3755. !in mm
    xMax = 3925. !in mm
    zMax = lambda0 / 2.
    zMin = -1 * zMax
    mMax = 5
    nMax = 5
    maxAmpValue = 10
    numberOfNewCreatedFiles = 1


    resolution = 32
    numberOfAmplitudesAndPhasesFiles =50
    print *,"Database Creator V1.0"
    print *, " TUM AER 2021"
    call calculateHeightsDatabase("../Databases/HeightsSin5.csv")

contains
    !Creates a CSV-file containing the height-profiles of the amplitudes and phases file
    !
    !pathToDataBase: The path were the CSV-file will be stored
    subroutine calculateHeightsDatabase(pathToDataBase)
        character (*), intent(in) :: pathToDataBase

        integer, dimension(:), allocatable :: amplitudesAndPhasesIDs
        integer :: i

        allocate(heights(resolution, resolution))
        allocate(Amp(mMax,nMax))
        allocate(Pha(mMax,nMax))
        !amplitudesAndPhasesIDs = getAllSimulationNumbers(pathToAmplitudesAndPhasesIDs)

        do i=1, numberOfAmplitudesAndPhasesFiles
            call appendOneLineToDataSet(i, pathToDataBase)
            print *, "Created height-profile at index: ", i
        end do
    end subroutine calculateHeightsDatabase

    !Appends one height-profile to the CSV-file database
    !
    !indexAmpliutdesAndPhfases: The index of the AmpAndPhaFile
    !pathToDataBase: The path to the CSV-file were the heightProfile will be appended
    subroutine appendOneLineToDataSet(indexAmplitudesAndPhases, pathToDataBase)
        integer, intent(in) :: indexAmplitudesAndPhases
        character (*), intent(in) :: pathToDataBase

        character (200) :: fileName
        integer :: i=1, j=1

        write (fileName, "(A8,I0.5,A4)") "Amp_Pha_", indexAmplitudesAndPhases, ".txt"
        call readAmpAndPhaFile(trim(pathToSavedAmplitudes//trim(fileName)))
        call calculateHeights()

        open(11, file=pathToDataBase, action='write')
        write(11, '(A6)', advance="no")  "CEQ-3-"
        write(11, '(I0.5, a)', advance="no")  indexAmplitudesAndPhases, ","
        do i=1, resolution
            do j=1, resolution
                !After the last height there should be no colon
                if (i /= resolution .or. j /= resolution) then
                    write(11, '(E15.6, a)', advance="no") heights(j,i), ","
                else
                    write(11, '(E15.6)') heights(j,i)
                end if
            end do
        end do
    end subroutine appendOneLineToDataSet


    !Reads all numbers of simulation which were stored in a text file
    !
    !pathToFileWithNumbers: The path to a file containg the indices for the file which should be read
    function getAllSimulationNumbers(pathToFileWithNumbers) result(numbers)
        character (*), intent(in) :: pathToFileWithNumbers

        integer :: i
        integer, dimension(:), allocatable :: numbers
        character(24) :: dummy

        open(31,FILE=pathToFileWithNumbers, action="read")
		read(31, "(A24,I5)") dummy, numberOfAmplitudesAndPhasesFiles
        print *, dummy
        print *, numberOfAmplitudesAndPhasesFiles
        
        allocate(numbers(numberOfAmplitudesAndPhasesFiles))
		do i = 1, numberOfAmplitudesAndPhasesFiles
			read(31, *) numbers(i)
		end do

        close(31)
    end function getAllSimulationNumbers

    !Reads in a AmpAndPhaFile and stores the results in the global variables Amp and Pha
    !
    !fileName: the name of the AmpAndPhaFile which should be read
    subroutine readAmpAndPhaFile(fileName)
        character (*), intent(in)  :: fileName
        open (unit=14, file=fileName, status='old', action='read')
        read(14, *) mMax, nMax
        do m=1,mMax
            do n=1,nMax        
                read (14,*) Amp(n,m), Pha(n,m)
            enddo        
        enddo 
        close(14)
    end subroutine readAmpAndPhaFile

    !Creates a file containing the amplitudes and phases
    !
    !fileName: The name of the file which should be created
    subroutine writeAmpPhaToFile(fileName)
        character (*), intent(in)  :: fileName
        open(10,file=fileName,action='write')
        write(10,'(I3, X, I3)') mMax, nMax
        !print *, mMax, nMax
        do n=1, nMax
            do m=1, mMax
                write(10,'(0PE22.11, X, 0PE22.11)') Amp(m,n), Pha(m,n)
            end do
        end do
        close(10)
    end subroutine writeAmpPhaToFile

    !Calculates the biggest height of the heights profile and stores the
    !result in the global variable maxHeight
    subroutine setMaxHeight()
        maxHeight = heights(1,1)
        do i=1,resolution
            do j=1,resolution
                if (heights(i,j) > maxHeight) then
                    maxHeight = heights(i,j)
                end if            
            end do
        end do
    end subroutine setMaxHeight

    !Divides the amplitudes by the max amplitude
    subroutine divideAmplitudesByMaxAmplitude()
        call setMaxHeight()
        do n=1, nMax
            do m=1, mMax
                Amp(m,n) = Amp(m,n) / ABS(maxHeight)
            end do
        end do            
    end subroutine divideAmplitudesByMaxAmplitude

    !Prints the average heights on the console
    subroutine printHeightsAverages()
        sumHeights = 0
        sumHeightsAbs = 0
        do i=1,resolution
            do j=1,resolution  
                sumHeights = sumHeights + heights(i,j)
                sumHeightsAbs = sumHeightsAbs + abs (heights(i, j))                          
            end do
        end do

        averageHeight = sumHeights / (resolution*resolution)
        averageHeightAbs = sumHeightsAbs / (resolution*resolution)   
        proportion = maxHeight / averageHeight
        proportionAbs = maxHeight / averageHeightAbs
        print *, "Sum heights:",  sumHeights, " Avg-Height: ", averageHeight, " H_Max / avg-Height: ", proportion
        print *, "Sum heights Abs:",  sumHeightsAbs, " Avg-Height Abs: ", averageHeightAbs, " H_Max / avg-Height: ", proportionAbs
    end subroutine printHeightsAverages

    !Generates random amplitudes an phases
    subroutine generateRandomAmpAndPha()
        !Initalize with random values
        call random_number(Amp)
        call random_number(Pha)
        do m=1,mMax
            do n=1,nMax
                !If this statement is true the amplitude is set to zero
                if((n*n + m*m > n + m + 1) .and. (n==m)) then
                    Amp(m,n) = 0 
                else
                    Amp(m,n) = maxAmpValue * Amp(m,n)
                end if 
                Pha(m,n) = 2 * PI * Pha(m, n)
            enddo        
        enddo 
    end subroutine generateRandomAmpAndPha

    !Generates a certain number of random amplitudes and phases files
    subroutine generateNewAmpAndPhaFilesWithHeights()
        allocate(Amp(mMax,nMax))
        allocate(Pha(mMax,nMax))

        do currentFileIndex=1, numberOfNewCreatedFiles
	        print *, "File number: ", currentFileIndex, " was created"
            !ErzeugenDerDateiNamen
            write (ampPhaFileName, "(A20,I0.4,A4)") "AmplitudenUndPhasen_", currentFileIndex, ".txt"
            write (ampPhaFileAfterCalculation, "(A34,I0.4,A4)") "AmplitudenUndPhasenNachBerechnung_", currentFileIndex, ".txt"
            write (heightsFileName, "(A7,I0.4,A4)") "Hoehen_", currentFileIndex, ".plt"

            call generateRandomAmpAndPha()
            call writeAmpPhaToFile(trim('AmplitudenUndPhasen/'//ampPhaFileName))
            call calculateHeights()
            call divideAmp()
            call writeAmpPhaToFile(trim('AmplitudenUndPhasen/'//ampPhaFileAfterCalculation))
            call writeHeightsToFile(trim('Hoehen/'//heightsFileName))
        end do
        print *, "Es wurde alle Dateien erfolgreich erstellt"
    end subroutine generateNewAmpAndPhaFilesWithHeights

    !Calculates the heights profile from the amplitudes and phases file
    subroutine calculateHeights()	
        do i=1,resolution
            z = zMin
            do j=1,resolution        
                x = xMin+1.*(i-1)/(1.*resolution-1.)*(xMax-xMin)
                z = zMin+1.*(j-1)/(1.*resolution-1.)*lambda0 
                heights(i,j) = 0
                do m=1,mMax
                    do n=1,nMax
                        x2=x/1000.
                        z2=z/1000.
                        radius=5.98172331
                        theta=(x2+5.98172331*0.615729034)/radius
                        phis=z2/(radius*sin(theta))
                        D_phi = (2*pi)/130.
                        thetar_end = 0.6576
                        D_thetar = 1*(2*pi)/130 * sin(thetar_end)
                        ! sin function heights
                        heights(i,j) = heights(i,j) + Amp(n,m)*SIN((2.*PI*m*phis/D_phi)+&
                        (2.*PI*n*(theta-thetar_end)/D_thetar) + Pha(n,m))
                        ! tri
                       ! heights(i,j) = heights(i,j) + Amp(n,m)/PI*ASIN(SIN((2.*PI*m*phis/D_phi)+&
                         ! (2.*PI*n*(theta-thetar_end)/D_thetar) + Pha(n,m)))
                    end do
                end do 
            end do
        end do
    end subroutine calculateHeights

end program calculatePlane