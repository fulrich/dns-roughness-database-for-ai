#import DataBaseConcatenator as dc
#import DatabaseGenerator as dg
import pandas as pd


def concatFilesAndRemoveDuplicates():
    #dc.concatDBfiles("/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/", "1_6_0")
    maxVorticitiesDf = pd.read_csv("/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/1_6_0_concatenated.csv", skipinitialspace=True)
    dfWithoutDuplicates = maxVorticitiesDf.drop_duplicates(subset=['simulationName'])
    dfWithoutDuplicates.to_csv("/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/1_6_0_Without_Duplicates.csv", index=False)

concatFilesAndRemoveDuplicates()
