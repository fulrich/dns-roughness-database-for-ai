import pandas as pd
import glob
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures, StandardScaler, MinMaxScaler

import numpy as np

"""
This file can handle three kinds of CSV files:
fst. kind: MaxVorticitiesDataBaseFile containing the SimulationName and the maxVorticity and many other variables
scd. kind: HeightsProfileFile (fromat: simulationIndex, height1, height2,..., heightn
thrd. kind ParameterFile 
These two databases can be joined by the simulationIndex and the Simulation name. In most of the cases there will
be way more HeightsProfiles than maxVorticity entries because not all simulations convergenced
"""


def createMergedDataBase(maxVorticitiesDf, otherDf):
    maxVorticitiesDf = maxVorticitiesDf[["simulationName", "maxVorticity"]].copy()
    mergedDf = maxVorticitiesDf.merge(otherDf, how='left', on='simulationName')
    #print(mergedDf["simulationName"].iloc[0])
    #print(mergedDf["maxVorticity"].iloc[0])
    #print(mergedDf["heights"].iloc[0])
    return mergedDf


def readHeightsFile(pathToHeightsFile):
    heightsDF = pd.read_csv(pathToHeightsFile, skipinitialspace=True, header=None)
    names = []
    heights = []
    for index, row in heightsDF.iterrows():
        names.append(row[0])
        heights.append(row[1:])
    heightsFloat = []
    for dataSet in heights:
        heightsFloat.append(list(map(float, dataSet)))

    print(heightsFloat[1])
    print(heights[0])
    resultDF = pd.DataFrame({'simulationName': names, 'heights': heightsFloat})
    print(resultDF.head())
    return resultDF


def readPandasFile(pathToParameterFile):
    parameterDf = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    return parameterDf

def createPolynomialRegression(xTrain, xTest, degree, interactionOnly):
    poly = PolynomialFeatures(degree, interaction_only=True)
    transformedTrainingData = poly.fit_transform(xTrain)
    transformedTestingData = poly.transform(xTest)
    return xTrain, xTest

def getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile):
    maxVorticitiesDf = pd.read_csv(maxVorticitiesFile, skipinitialspace=True)
    maxVorticitiesDf.rename(columns={'maxVorticityParabola': 'maxVorticity'}, inplace=True)
    parameterDf = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    mergedDf = createMergedDataBase(maxVorticitiesDf, parameterDf)
    y = mergedDf[["maxVorticity"]]
    mergedDf = mergedDf.drop(columns="maxVorticity")
    x = mergedDf.drop(columns="simulationName")
    y = np.asarray(y)
    y = np.asarray(y) / 10000
    xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size=0.1, random_state=0)
    xTrain, xTest = standardizationOfTrainAndTestInput(xTrain, xTest)
    #yTrain, yTest = standardizationOfTrainAndTestInput(yTrain, yTest)
    return xTrain, xTest, yTrain, yTest

def readFritzDataBase(pathToDataBaseFile):
    maxVorticitiesDf = pd.read_csv(pathToDataBaseFile, skipinitialspace=True)
    x = maxVorticitiesDf.drop(columns="maxVorticity")
    y = maxVorticitiesDf[["maxVorticity"]]
    y = np.asarray(y) / 10000

    xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size=0.1, random_state=0)
    xTrain, xTest = standardizationOfTrainAndTestInput(xTrain, xTest)
    return xTrain, xTest, yTrain, yTest

def getDataBase(maxVorticitiesFile, pathToParameterFile):
    maxVorticitiesDf = pd.read_csv(maxVorticitiesFile, skipinitialspace=True)
    maxVorticitiesDf.rename(columns={'maxVorticityParabola': 'maxVorticity'}, inplace=True)
    parameterDf = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    mergedDf = createMergedDataBase(maxVorticitiesDf, parameterDf)
    y = mergedDf[["maxVorticity"]]
    mergedDf = mergedDf.drop(columns="maxVorticity")
    return mergedDf



def getTrainingsDataOfTwoParameterFiles(maxVorticitiesFile, pathToParameterFile1, pathToParameterFile2):
    maxVorticitiesDf = pd.read_csv(maxVorticitiesFile, skipinitialspace=True)
    maxVorticitiesDf.rename(columns={'maxVorticityParabola': 'maxVorticity'}, inplace=True)
    parameterDf1 = pd.read_csv(pathToParameterFile1, skipinitialspace=True)
    parameterDf2 = pd.read_csv(pathToParameterFile2, skipinitialspace=True)
    mergedDf = createMergedDataBase(maxVorticitiesDf, parameterDf1)
    mergedDf = mergedDf.merge(parameterDf2, how='left', on='simulationName')
    y = mergedDf[["maxVorticity"]]
    mergedDf = mergedDf.drop(columns="maxVorticity")
    x = mergedDf.drop(columns="simulationName")
    x = x.drop(columns="maxAmp")
    x = x.drop(columns="meanAmp")
    x = x.drop(columns="maxCurv")
    x = x.drop(columns="maxCurv2")
    x = x.drop(columns="maxDerix")
    x = x.drop(columns="maxDeriz")
    x = x.drop(columns="meanCurv")
    #x = x.drop(columns="meanDerix")
    #x = x.drop(columns="meanDeriz")

    x = x.drop(columns="RMSy")
    x = x.drop(columns="RMSdx")
    #x = x.drop(columns="RMSdz")
    x = x.drop(columns="RMS-A")
    x = x.drop(columns="Phi")
    #x = x.drop(columns="pushline")
    #x = x.drop(columns="pushlineb")
    #x = x.drop(columns="pushline2")
    #x = x.drop(columns="pushline2b")
    #x = x.drop(columns="pushline4")
    #x = x.drop(columns="pushline4b")
    #x = x.drop(columns="pushline5")
    #x = x.drop(columns="pushline5b")
    #x = x.drop(columns="amp11")
    #x = x.drop(columns="amp12")
    #x = x.drop(columns="amp13")
    #x = x.drop(columns="phi11")
    #x = x.drop(columns="phi12")
    #x = x.drop(columns="phi13")

    # xData = parameterDf.to_numpy()
    # xData = xData.transpose()
    xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size=0.2, random_state=0)
    xTrain, xTest = standardizationOfTrainAndTestInput(xTrain, xTest)
    return xTrain, xTest, yTrain, yTest

def getTrainingsDataHeightsDatabase(pathToMaxVorticitiesFile, pathToHeightsFile, domainResolution):
    maxVorticitiesDf = pd.read_csv(pathToMaxVorticitiesFile, skipinitialspace=True)
    maxVorticitiesDf.rename(columns={'maxVorticityParabola': 'maxVorticity'}, inplace=True)

    heightsDf = readHeightsFile(pathToHeightsFile)
    #heightsDf['heights'] = heightsDf['heights'].map(lambda heights: [float(x) for x in heights])
    mergedDf = createMergedDataBase(maxVorticitiesDf, heightsDf)

    #y = mergedDf[["maxVorticity"]]

    arrayX = mergedDf['heights'].to_list()
    arrayX = np.asarray(arrayX)
    arrayX = normalize2dArrayToValuesBetweenMinus1and1(arrayX)
    #arrayX = normalize2dArrayToValuesBetweenMinus1and1_2(arrayX)
    arrayX = reshapeArray(arrayX, domainResolution)

    # yData
    arrayY = mergedDf["maxVorticity"]
    arrayY = np.asarray(arrayY) / 10000
    #np.random.shuffle(arrayY)

    xTrain, xTest, yTrain, yTest = train_test_split(arrayX, arrayY, test_size=0.1, random_state=0)
    #yTrain, yTest = standardizationOfTrainAndTestInput(pd.DataFrame(yTrain), pd.DataFrame(yTest))
    return xTrain, xTest, yTrain, yTest


def coverHeightAreaWithMinusOne(xData, xPosition, yPosition, sizeX, sizeY):
    for i in range(xData.shape[0]):
        for x in range(sizeX):
            for y in range(sizeY):
                xData[i][xPosition + x][yPosition + y] = -1
    return xData



def normalize2dArrayToValuesBetweenMinus1and1_2(arrayInput):
    print(arrayInput.shape)
    minValue = np.min(arrayInput)
    maxValue = np.max(arrayInput)
    valueRange = maxValue - minValue

    for i in range(len(arrayInput)):
        for j in range(len(arrayInput[i])):
            arrayInput[i][j] = 2 * ((arrayInput[i][j] - minValue) / valueRange) - 1
    return arrayInput

def normalize2dArrayToValuesBetweenMinus1and1(arrayInput):
    print(arrayInput.shape)

    for i in range(len(arrayInput)):
        minValue = np.min(arrayInput[i])
        maxValue = np.max(arrayInput[i])
        valueRange = maxValue - minValue
        for j in range(len(arrayInput[i])):
            arrayInput[i][j] = 2 * ((arrayInput[i][j] - minValue) / valueRange) - 1
    return arrayInput


def reshapeArray(array, domainResolution):
    return array.reshape(len(array), domainResolution, domainResolution, 1)

def standardizationOfTrainAndTestInput(xTrain, xTest):
    scaler = StandardScaler()
    #scaler = MinMaxScaler()

    xTrain = pd.DataFrame(
        scaler.fit_transform(xTrain),
        columns=xTrain.columns
    )

    xTest = pd.DataFrame(
        scaler.transform(xTest),
        columns=xTest.columns
    )
    return xTrain, xTest

def getParameterNames(pathToParameterFile):
    parameterDf = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    parameterDf = parameterDf.drop(columns="simulationName")
    return parameterDf.columns

def getTrainingDataWithoutCertainParameters(maxVorticitiesFile, pathToParameterFile):
    omittedParameterListNegativeMPE = ['kurtosisDhdxdz', 'rmsDhdzdz', 'meanAbsoluteDhdzdz', 'AADDhdzdz',
                            'maxDhdzdz', 'maxHeight', 'minDhdzdz', 'rmsDhdxdz', 'AADDhdxdz',
                            'meanAbsoluteDhdxdz', 'minDhdxdx', 'maxDhdxdz', 'rmsDhdxdx', 'minDhdxdz',
                            'AADDhdxdx', 'maxDhdxdx', 'kurtosisDhdxdx', 'meanAbsoluteDhdxdx',
                            'kurtosisDhdzdz', 'meanDhdxdx']
    omittedParameterListNumberMoreThanHalfGotBad = ['meanDhdxdx', 'kurtosisDhdxdx', 'maxHeight',
                                        'kurtosisDhdzdz', 'minDhdxdz', 'maxDhdxdz', 'AADDhdxdx',
                                        'meanAbsoluteDhdxdz', 'AADDhdxdz', 'meanAbsoluteDhdxdx',
                                        'maxDhdxdx', 'rmsDhdxdz', 'rmsDhdxdx', 'minDhdxdx', 'kurtosisDhdxdz',
                                        'minDhdzdz', 'maxDhdzdz', 'meanDhdxdz', 'skewnessDhdxdx', 'meanDhdzdz',
                                        'AADDhdzdz', 'meanAbsoluteDhdzdz', 'rmsDhdzdz', 'meanAbsoluteDhdz',
                                        'AADHeight', 'AADDhdz', 'meanAbsoluteHeight']

    omittedParameterList = list(set(omittedParameterListNumberMoreThanHalfGotBad + omittedParameterListNegativeMPE))
    maxVorticitiesDf = pd.read_csv(maxVorticitiesFile, skipinitialspace=True)
    maxVorticitiesDf.rename(columns={'maxVorticityParabola': 'maxVorticity'}, inplace=True)
    parameterDf = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    mergedDf = createMergedDataBase(maxVorticitiesDf, parameterDf)
    mergedDf = mergedDf.drop(omittedParameterList, axis=1)

    y = mergedDf[["maxVorticity"]]
    mergedDf = mergedDf.drop(columns="maxVorticity")
    x = mergedDf.drop(columns="simulationName")



    y = np.asarray(y)
    y = np.asarray(y) / 10000
    xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size=0.1, random_state=0)
    xTrain, xTest = standardizationOfTrainAndTestInput(xTrain, xTest)
    #yTrain, yTest = standardizationOfTrainAndTestInput(yTrain, yTest)
    return xTrain, xTest, yTrain, yTest

#pathToParameterFile = "../Databases/ParameterDatabases/ParameterFileForAll15kSimulations.csv"
#maxVorticitiesDf = pd.read_csv("../Databases/MaxVorticityFiles/MaxVorticities_First_10_Batches.csv", skipinitialspace=True)

#heightsDf = readHeightsFile()
#parameterDf = readParameterFile()
#createMergedDataBase(maxVorticitiesDf, heightsDf)
#merged = createMergedDataBase(maxVorticitiesDf, heightsDf)
#print(merged.head())
#merged.to_csv("../Databases/ParameterDatabases/ParameterFirst10Batches.csv", index=False)
#merged.to_csv("../Databases/HeightsDatabases/First10BatchesHeightsDB.csv", index=False)




















