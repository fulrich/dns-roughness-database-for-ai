from concurrent.futures import ThreadPoolExecutor

import pandas as pd
import tensorflow as tf
import numpy as np
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
import os
from PIL import Image

# How to use this program and what it is about:
# The code trains a neuronal network to predict the maximum vorticity which
# Therefore we need a resolution with which the grid is resolved

from DatabaseGenerator import getTrainingsDataHeightsDatabase, coverHeightAreaWithMinusOne, \
    readFritzDataBase, getTrainingsDataParameterFile
from ModelEvaluation import evaluatePredictions


epochs = 40
# The resolution of the quadratic grid
domainResolution = 16
# If we use a convolutional network the data has to be reshaped
useConvolutionalModel = True


def createTestAndTrainingsDataSet2(pathToDataBase):
    dataFrame = pd.read_csv(pathToDataBase, converters={"heights": lambda x: x.strip("[]\"").split(", ")}, skipinitialspace=True)

    # xData

    dataFrame['heights'] = dataFrame['heights'].map(lambda heights: [float(x) for x in heights])
    arrayX = dataFrame['heights'].to_list()
    arrayX = np.asarray(arrayX)
    arrayX = normalize2dArrayToValuesBetweenMinus1and1(arrayX)
    arrayX = reshapeArray(arrayX)

    #yData
    arrayY = dataFrame["maxVorticity"]
    arrayY = np.asarray(arrayY) / 1000
    splitIndex = int(0.9 * len(arrayY))
    #np.save("../Databases/x_64_64", arrayX)
    #np.save("../Databases/y_64_64", arrayY)
    return arrayX[:splitIndex], arrayX[splitIndex:], arrayY[:splitIndex], arrayY[splitIndex:]

def readHeights(pathToHeightsFile):
    dataSet = pd.read_csv(pathToHeightsFile, header=None, skipinitialspace=True)
    return dataSet


# Reads the maximum vorticities out of the csv-file
def readMaxVorticities(pathToMaxVortictiesFile):
    column_names = ['maxVorticity', 'yPosition', 'zPosition', 'jPosition', 'kPosition', 'simulationName',
                    'pathToSimulation1']
    dataSet = pd.read_csv(pathToMaxVortictiesFile, names=column_names, header=None, skiprows=1, skipinitialspace=True)
    # print(dataSet.head)
    vorticityAndName = dataSet[['simulationName', 'maxVorticity']]
    array = vorticityAndName.to_numpy()
    simulationIDandMaxVorticities = []
    for simulationName, maxVorticity in array:
        simulationIDandMaxVorticities.append((simulationName[6:], maxVorticity))

    # resultDF = pd.DataFrame(simulationIDandMaxVorticities, columns=["simulationID","maxVorticity"])
    return simulationIDandMaxVorticities


def createTestAndTrainingSet(pathToHeightsFile, pathToMaxVorticitiesFile):
    heights = readHeights(pathToHeightsFile)
    heightsList = heights.values.tolist()
    maxVorticities = readMaxVorticities(pathToMaxVorticitiesFile)
    listX = []
    listY = []
    for i in range(len(heightsList)):
        if not int(heightsList[i][0]) == int(maxVorticities[i][0]):
            print("Error in DB-Generation process!")
            return ([], [], [], [])

        listX.append(heightsList[i][1:])
        listY.append(maxVorticities[i][1])

    # Use 80% for training
    splitIndex = int(0.8 * len(listX))
    arrayY = []
    arrayX = normalize2dArrayToValuesBetweenMinus1and1(np.array(listX))
    arrayX = reshapeArray(arrayX)
    arrayY = np.asarray(listY) / 1000
    np.save("../Databases/x_64_64", arrayX)
    np.save("../Databases/y_64_64", arrayY)
    return (arrayX[:splitIndex], arrayX[splitIndex:], arrayY[:splitIndex], arrayY[splitIndex:])


def normalize2dArrayToValuesBetweenMinus1and1(arrayInput):
    print(arrayInput.shape)

    for i in range(len(arrayInput)):
        minValue = np.min(arrayInput[i])
        maxValue = np.max(arrayInput[i])
        valueRange = maxValue - minValue
        for j in range(len(arrayInput[i])):
            arrayInput[i][j] = 2 * ((arrayInput[i][j] - minValue) / valueRange) - 1
    return arrayInput


def reshapeArray(array):
    if not useConvolutionalModel:
        return array
    # For convolutionalModels we have to reshape the array
    return array.reshape(len(array), domainResolution, domainResolution, 1)


def createBiggerModel():
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    model.add(layers.Conv2D(128, (3, 3), activation='relu', input_shape=(domainResolution, domainResolution, 1)))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((3, 3)))

    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.BatchNormalization())
    # model.add(layers.MaxPooling2D((3, 3)))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    # model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.BatchNormalization())
    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.BatchNormalization())
    model.add(layers.Flatten())
    model.add(layers.Dense(500, activation='relu'))
    model.add(layers.Dense(200, activation='relu'))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Dense(1))
    model.summary()

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
                  loss=tf.keras.losses.MeanSquaredLogarithmicError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError()])
    return model


def createConvModel():
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    model.add(layers.Conv2D(128, (3, 3), activation='relu', input_shape=(domainResolution, domainResolution, 1)))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    # model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.BatchNormalization())
    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.BatchNormalization())
    model.add(layers.Flatten())
    model.add(layers.Dense(500, activation='relu'))
    model.add(layers.Dense(200, activation='relu'))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Dense(1))
    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
                  loss=tf.keras.losses.MeanSquaredLogarithmicError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError()])

    return model

def createConvModel2():
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(domainResolution, domainResolution, 1)))
    model.add(layers.BatchNormalization())
    #model.add(layers.Dropout(0.3))
    model.add(layers.MaxPooling2D((2, 2)))


    # model.add(layers.Dropout(0.4))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    #model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.3))

    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.3))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation='relu'))
    model.add(layers.Dropout(0.3))
    model.add(layers.Dense(200, activation='relu'))
    model.add(layers.Dropout(0.3))

    model.add(layers.Dense(1))

    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.00025),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError(), tf.keras.metrics.MeanAbsolutePercentageError()])

    return model


def createSimpleModel():
    model = models.Sequential()
    model.add(layers.Dense(1000, activation='relu', input_shape=(domainResolution * domainResolution,)))
    model.add(layers.BatchNormalization())
    # model.add(layers.Dropout(0.5))
    model.add(layers.Dense(4000, activation='relu'))
    model.add(layers.BatchNormalization())
    # model.add(layers.Dropout(0.25))
    model.add(layers.Dense(1000, activation='relu'))
    model.add(layers.Dense(1000, activation='relu'))
    # model.add(layers.Dropout(0.25))
    model.add(layers.Dense(1, activation='relu'))
    model.summary()

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
                  loss=tf.keras.losses.MeanSquaredLogarithmicError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError()])
    return model


def evaluateModelOwnFunction(model, xTest, yTest):
    predictions = model.predict(xTest)
    sum = 0

    for i in range(len(yTest)):
        error = abs(predictions[i] - yTest[i])
        print("model: " + str(predictions[i]) + "   Y_test: " + str(yTest[i]) + " Error: " + str(error))
        sum += error
    # The Mean error
    return sum / len(yTest)


def evaluateModelUsingMean(yTrain, yTest):
    mean = np.mean(yTrain)
    sum = 0

    print("Mean: " + str(mean))
    for i in range(len(yTest)):
        error = abs(mean - yTest[i])
        # print("model: " + str(mean) + "   Y_test: " + str(yTest[i]) +  " Error: " + str(error))
        sum += error
    # The Mean error
    return sum / len(yTest)

def saveXdataToImage(xData, nameOfImage):
    xData = np.reshape(xData, xData.shape[:2])
    xDataTransformedToImage = ((xData - np.min(xData)) / (np.max(xData) - np.min(xData))) * 250
    img = Image.fromarray(np.uint8(xDataTransformedToImage), 'L')
    imgRescaled = img.resize(size=(512, 512), resample=Image.NEAREST)
    imgRescaled.save(nameOfImage + '.png')
    #imgRescaled.show()
    #plt.show()

def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    # plt.ylim([0, 10])
    plt.xlabel('Epoch')
    plt.ylabel('Error [maxVorticity]')
    plt.legend()
    plt.grid(True)
    plt.show()


def plot_Errors(test_predictions):
    # plot_loss(history)
    error = (test_predictions - yTest) / yTest
    plt.hist(error, bins=25)
    plt.xlabel('Prediction Error')
    _ = plt.ylabel('Count')
    plt.show()


def plot_Errors2(test_predictions):
    plt.scatter(yTest * 1000, test_predictions * 1000, label='Data')
    plt.xlabel('True Values max. Vorticity')
    plt.ylabel('Predictions max. Vorticity')
    lims = [20000, 200000]
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims, color='g')
    x = tf.linspace(20000, 200000, 351)
    y = x + x * 0.5
    x2 = tf.linspace(20000, 200000, 351)
    y2 = x - x * 0.5
    plt.plot(x, y, color='k', label='+ 50%')
    plt.plot(x2, y2, color='grey', label='- 50%')
    x3 = tf.linspace(20000, 200000, 351)
    y3 = x + x * 0.3
    x4 = tf.linspace(20000, 200000, 351)
    y4 = x - x * 0.3
    plt.plot(x3, y3, color='red', label='+ 30%')
    plt.plot(x4, y4, color='m', label='- 30%')
    plt.legend()
    plt.show()

def plot_Errors3(test_predictions,yTest):
    yScale = 200000
    plt.scatter(yTest*10000,test_predictions*10000, label='Data')
    plt.xlabel('True Values max. Vorticity')
    plt.ylabel('Predictions max. Vorticity')
    lims = [0, yScale]
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims,color='g')
    x = tf.linspace(0, yScale, 351)
    y = x+x*0.5
    x2 = tf.linspace(0, yScale, 351)
    y2 = x-x*0.5
    plt.plot(x, y,color='k', label='+ 50%')
    plt.plot(x2,y2,color='grey', label='- 50%')
    x3 = tf.linspace(0, yScale, 351)
    y3 = x+x*0.3
    x4 = tf.linspace(0, yScale, 351)
    y4 = x-x*0.3
    plt.plot(x3, y3,color='red', label='+ 30%')
    plt.plot(x4,y4,color='m', label='- 30%')
    plt.legend()
    plt.savefig("FritzPlot.png", dpi=400)
    plt.show()


# plot_Errors2(test_predictions)
# plot_Errors(test_predictions)

def plotTrainingHistory(history):
    plt.plot(history.history['mean_absolute_error'])
    plt.plot(history.history['val_mean_absolute_error'])
    #plt.title('Durchschnittlicher mittlerer Fehler')
    plt.ylabel('Durchschnittlicher mittlerer Fehler')
    plt.xlabel('Anzahl Epochen')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_MAF.png", dpi=400)
    plt.show()

    plt.plot(history.history['mean_absolute_percentage_error'])
    plt.plot(history.history['val_mean_absolute_percentage_error'])
    #plt.title('Durchschnittlicher prozentualer Fehler')
    plt.ylabel('Durchschnittlicher prozentualer Fehler')
    plt.xlabel('Anzahl Epochen')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_MPF.png", dpi=400)
    plt.show()

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    #plt.title('Loss')
    plt.ylabel('Loss')
    plt.xlabel('Anzahl Epochen')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_Loss.png", dpi=400)
    plt.show()

def trainModel(modelName):
    os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
    #xTrain, xTest, yTrain, yTest = createTestAndTrainingsDataSet2("../Databases/HeightsDatabases/First10BatchesHeightsDB.csv")
    pathToHeightsFile = "Heights_16_16.csv"
    pathToMaxVorticityFile = "1_6_0_All.csv"
    xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution)

    model = createConvModel2()
    earlyStoppingCallback = tf.keras.callbacks.EarlyStopping(monitor='val_mean_absolute_error', patience=10,
                                                             restore_best_weights=True, mode="min")
    # Can be used for saving checkpoints, so we need the whole structure of the network etc. So I prefer saving
    # the whole model using model save

    history = model.fit(xTrain, yTrain, epochs=50, validation_freq=1, batch_size=64,
                        validation_split=0.1, callbacks=[earlyStoppingCallback])
    plotTrainingHistory(history)

    print("Evaluation: ")
    model.evaluate(xTest, yTest)
    print("Average Error: " + str(evaluateModelUsingMean(yTrain, yTest)))
    #Save the best model with whole structure
    model.save("../Models/" + modelName)

    model.evaluate(xTest, yTest)
    predictions = model.predict(xTest)
    evaluatePredictions(yTest, predictions)

def trainModelWithSpecificData(modelName, xTrain, xTest, yTrain, yTest):
    os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
    # xTrain, xTest, yTrain, yTest = createTestAndTrainingsDataSet2("../Databases/HeightsDatabases/First10BatchesHeightsDB.csv")

    model = createConvModel2()
    #model = generateFritzModel()
    earlyStoppingCallback = tf.keras.callbacks.EarlyStopping(monitor='val_mean_absolute_percentage_error', patience=10,
                                                             restore_best_weights=True, mode="min")
    # Can be used for saving checkpoints, so we need the whole structure of the network etc. So I prefer saving
    # the whole model using model save
    # This model is used
    history = model.fit(xTrain, yTrain, epochs=100, validation_freq=1, batch_size=26,
                        validation_split=0.1, callbacks=[earlyStoppingCallback])
    #plotTrainingHistory(history)

    #print("Evaluation: ")
    #model.evaluate(xTest, yTest)
    #print("Average Error: " + str(evaluateModelUsingMean(yTrain, yTest)))
    # Save the best model with whole structure
    model.save("Models/" + modelName)

    #model.evaluate(xTest, yTest)
    predictions = model.predict(xTest)
    modelScore = evaluatePredictions(yTest, predictions)
    return model, modelScore

def generateFritzModel():
    model = models.Sequential()
    model.add(layers.Dense(140, input_shape=(30,), activation='elu'))
    #model.add(layers.BatchNormalization())
    layers.Dropout(0.3),
    #layers.Dense(800, activation='relu'),
    #layers.Dropout(0.5),

    model.add(layers.Dense(460, activation='elu'))
    #model.add(layers.Dense(200, activation=tf.keras.layers.LeakyReLU()))
    #model.add(layers.BatchNormalization())
    layers.Dropout(0.3)

    model.add(layers.Dense(360, activation='elu'))
    #model.add(layers.Dense(200, activation=tf.keras.layers.LeakyReLU()))
    #model.add(layers.BatchNormalization())
    layers.Dropout(0.3)

    model.add(layers.Dense(1))

    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.005),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError(), tf.keras.metrics.MeanAbsolutePercentageError()])
    model.summary()
    return model

def getHeatMap():
    pathToHeightsFile = "Heights_16_16.csv"
    pathToMaxVorticityFile = "1_6_0_All.csv"
    xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution)
    setValuesForReproducibility()
    model = createConvModel2()
    xData = coverHeightAreaWithMinusOne(xTrain, 12, 12, 4, 4)

def setValuesForReproducibility():
    seed_value = 10
    os.environ['PYTHONHASHSEED'] = str(seed_value)

    # 2. Set the `python` built-in pseudo-random generator at a fixed value
    import random
    random.seed(seed_value)

    # 3. Set the `numpy` pseudo-random generator at a fixed value
    import numpy as np
    np.random.seed(seed_value)

    # 4. Set the `tensorflow` pseudo-random generator at a fixed value
    import tensorflow as tf
    tf.random.set_seed(seed_value)
    # for later versions:
    # tf.compat.v1.set_random_seed(seed_value)

    # 5. Configure a new global `tensorflow` session
    #from keras import backend as K
    #session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
    #sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
    #K.set_session(sess)



    #test_predictions = model.predict(xTest)
    # savingCallback = tf.keras.callbacks.ModelCheckpoint(
    #    filepath=pathToModel,
    #    verbose=1,
    #    save_weights_only=True,
    #    monitor='mean_absolute_error',
    #    mode='min',
    #    save_best_only=True)
    # load best model
    # latestCheckpoint = tf.train.latest_checkpoint(pathToModel)
    # model = createSimpleModel()
    # model.load_weights(latestCheckpoint)

def trainModelWorker():
    modelName = "ninthModel"
    #pathToHeightsFile = "/home/thomas/Dokumente/HiWi/vorticity_prediction/Databases/HeightsProfiles/Heights_16_16.csv"
    #pathToMaxVorticityFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"

    xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile,
                                                                   domainResolution)
    model, score = trainModelWithSpecificData(modelName, xTrain, xTest, yTrain, yTest)
    return score


def trainModelInParallel():
    executor = ThreadPoolExecutor(max_workers=4)
    resultList = []
    for i in range(4):
        future = executor.submit(trainModelWorker)
        resultList.append(future.result())

    executor.shutdown()
    print(resultList)

def trainModelMultipleTimes(xTrain, xTest, yTrain, yTest):
    resultList = []
    for i in range(3):
        model, score = trainModelWithSpecificData("", xTrain, xTest, yTrain, yTest)
        resultList.append(score)

    print(resultList)
    return dict_mean(resultList)

def dict_mean(dict_list):
    mean_dict = {}
    for key in dict_list[0].keys():
        mean_dict[key] = sum(d[key] for d in dict_list) / len(dict_list)
    return mean_dict

def heatMapDataGeneration():
    pathToHeightsFile = "Heights_16_16.csv"
    pathToMaxVorticityFile = "1_6_0_All.csv"

    with open("logfile.csv", "a") as logFile:
        #Write Header
        logFile.write("xPos,yPos,MAE,MAXE,MPE\n")
        #xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution)
        for i in range(4):
            xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile,
                                                                           pathToHeightsFile, domainResolution)
            xDataTrain = coverHeightAreaWithMinusOne(xTrain, i * 4, 0, 4, 16)
            xDataTest = coverHeightAreaWithMinusOne(xTest, i * 4, 0, 4, 16)
            result = trainModelMultipleTimes(xDataTrain, xDataTest, yTrain, yTest)
            logFile.write(str(i) + "," + str(0) + "," + str(result["MAE"]) + "," +
                          str(result["MAXE"]) +  "," + str(result["MPE"]) + "\n")
            saveXdataToImage(xDataTrain[0], str(i) + "_" + str(0) + "covered")

def analysisOfDataBaseSize():
    listNumberTrainingsSamples = [25,50,100,250,500,1000,2000,4000,6000,8000]
    with open("LogFileAnalysisDBsizeFritz.csv", "a") as logFile:
        #Write Header
        logFile.write("numberTrainingsSamples,MAE,MAXE,MPE\n")
        xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(pathToMaxVorticityFile, pathToHeightsFile, domainResolution)
        #xTrain, xTest, yTrain, yTest = readFritzDataBase("/home/thomas/Downloads/heilbronn.csv")
        for numberTrainingsSamples in listNumberTrainingsSamples:
            result = trainModelMultipleTimes(xTrain[:numberTrainingsSamples], xTest, yTrain[:numberTrainingsSamples], yTest)
            logFile.write(str(numberTrainingsSamples) + "," + str(result["MAE"]) + "," +
                          str(result["MAXE"]) + "," + str(result["MPE"]) + "\n")

def plotAnalysisOfDataSize(fileName):
    df = pd.read_csv(fileName, skipinitialspace=True)
    df["$MPF$"] = df["$MPF$"]*100
    df.set_index('numberTrainingsSamples').plot(figsize=(10, 5), grid=True)
    plt.ylabel('Fehler')
    plt.xlabel('Anzahl Trainingsdaten')
    plt.savefig("FehlerInAbhaengigkeitDerDatenbankGroeße.png", dpi=400)
    plt.show()



os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
setValuesForReproducibility()

modelName = "FritzModel"
pathToHeightsFile = "Heights_16_16.csv"
pathToMaxVorticityFile = "1_6_0_All.csv"



xTrain, xTest, yTrain, yTest = getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution)
model, score = trainModelWithSpecificData(modelName, xTrain, xTest, yTrain, yTest)

#Use saved Model
model = tf.keras.models.load_model("Models/" + "BestConvModel4")
predictions = model.predict(xTest)
evaluatePredictions(yTest, predictions)

predictions2 = predictions.flatten()

df = pd.DataFrame({
    "Predictions" : predictions2,
    "trueValues" : yTest
})

df.to_csv('testfile.csv', index=False)

