from DatabaseGeneration.DatabaseGenerator import getTrainingsDataParameterFile, getParameterNames, \
    getTrainingDataWithoutCertainParameters
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import plot_tree
from matplotlib import pyplot as plt
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
import pandas as pd
import statsmodels.api as sm
from DatabaseGeneration.DatabaseGenerator import getTrainingsDataParameterFile
import numpy as np
from numpy import arange
from pandas import read_csv
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.model_selection import RepeatedKFold
from sklearn.linear_model import Ridge, ElasticNet, Lasso
from sklearn.linear_model import ElasticNetCV
from sklearn.model_selection import RepeatedKFold
from sklearn.datasets import make_regression
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedKFold
from sklearn.ensemble import RandomForestRegressor
from sklearn import metrics
from ForwardSelcection import performCrossValidation
from sklearn.ensemble import AdaBoostRegressor
from sklearn import tree
import graphviz

maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/1_6_0_Without_Duplicates.csv"
maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/1_6_0_All.csv"
maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"
pathToParameterFile = "../Databases/ParameterDatabases/ParameterFileForAll15kSimulations.csv"
pathToParameterFile2 = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/ParameterDatabases/AllParameters.csv"
pathToParameterFile3 = "../Databases/ParameterDatabases/ParametersNewAbsMean.csv"


def evaluateModel(model, xTest, yTest):
    predicitions = model.predict(xTest)
    from sklearn.metrics import mean_absolute_error, max_error, mean_absolute_percentage_error
    mean_absolute_error = mean_absolute_error(yTest, predicitions)
    maxError = max_error(yTest, predicitions)
    meanAbsolutePercentageError = mean_absolute_percentage_error(yTest, predicitions)

    print("Mean absolute error: " + str(mean_absolute_error))
    print("Max Error: " + str(maxError))
    print("Mean absolute percentage error: " + str(meanAbsolutePercentageError))
    maxPercentageError, wrongPredictedValue = calculateMaxPercentageError(yTest, predicitions)
    print("Max percentage error: " + str(maxPercentageError) + "most wrong predicted value: " + str(wrongPredictedValue))


def calculateMaxPercentageError(yTest, predictions):
    maxPercentageError = -100000
    wrongPredictedValue = 0
    for i in range(len(predictions)):
        percentageError = abs((predictions[i]-yTest[i]) / yTest[i])
        if percentageError > maxPercentageError:
            wrongPredictedValue = yTest[i]
            maxPercentageError = percentageError

    return maxPercentageError, wrongPredictedValue


def generateRandomForest(xTrain, yTrain):
    # define the model
    model = RandomForestRegressor(n_estimators=10)
    # evaluate the model
    #cv = RepeatedKFold(n_splits=10, n_repeats=1, random_state=1)
    #n_scores = cross_val_score(model, xTrain, yTrain, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1, error_score='raise')
    # report performance
    #print('MAE: %.3f (%.3f)' % (np.mean(n_scores), np.std(n_scores)))


def generateDecisionTree():
    return 0

def generateAdaBoostForest(xTrain, yTrain):
    adaBoost = AdaBoostRegressor(n_estimators=500, random_state=0)
    #adaBoost.fit(xTrain, yTrain)
    #print("Finished")
    print(performCrossValidation(xTrain, yTrain, adaBoost))

def performGridSearch(X_train, y_train):
    adaBoost = AdaBoostRegressor(random_state=0)
    g_cv = search_grid={'n_estimators':[500,1000,2000],'learning_rate':[.001,0.01,.1],'random_state':[1]}
    search = RandomizedSearchCV(estimator=adaBoost, n_iter=3, scoring='neg_mean_squared_error', n_jobs=-1, cv=5)
    search.fit(X_train, y_train)
    print(search.best_params_)

    #g_cv.fit(X_train, y_train)
    #print(g_cv.best_params_)
    #print(sorted(g_cv.cv_results_))

def performGridSearch(X_train, y_train):
    adaBoost = AdaBoostRegressor(random_state=0)
    g_cv = search_grid={'n_estimators':[500,1000,2000],'learning_rate':[.001,0.01,.1],'random_state':[1]}
    search = GridSearchCV(estimator=adaBoost, param_grid=search_grid, scoring='neg_mean_squared_error', n_jobs=-1, cv=5)
    search.fit(X_train, y_train)
    print(search.best_params_)

def polynomialFeaturesWithInteraction(xTrain, xTest):
    poly = PolynomialFeatures(2, interaction_only=False, include_bias=False)
    # npArray = np.asarray(values)
    newXTrain = poly.fit_transform(xTrain)
    newXTest = poly.fit_transform(xTest)
    return newXTrain, newXTest
#model = DecisionTreeRegressor(random_state=1, min_samples_leaf=27)


print("All Parameters:")
xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile2)
#xTrain, xTest = polynomialFeaturesWithInteraction(xTrain, xTest)
#performGridSearch(xTrain, yTrain.ravel())
#performGridSearch(xTrain, yTrain)
#model = AdaBoostRegressor(base_estimator=DecisionTreeRegressor(random_state=1, max_depth=12), n_estimators=350, learning_rate=0.5)
#model = RandomForestRegressor(n_estimators=350, min_samples_leaf=2, n_jobs=-1)
#print(performCrossValidation(xTrain, yTrain.ravel(), model))
#model = model.fit(xTrain, yTrain.ravel())
#evaluateModel(model, xTest, yTest.ravel())

model = DecisionTreeRegressor(random_state=1, min_samples_leaf=40)
#print("Reduced Parameters:")
#model = RandomForestRegressor(n_estimators=250, min_samples_leaf=1, n_jobs=-1)
xTrain, xTest, yTrain, yTest = getTrainingDataWithoutCertainParameters(maxVorticitiesFile, pathToParameterFile2)
model = model.fit(xTrain, yTrain.ravel())
evaluateModel(model, xTest, yTest.ravel())

#xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile2)
#model = DecisionTreeRegressor(random_state=77, min_samples_leaf=27)
#model.fit(xTrain, yTrain.ravel())
#evaluateModel(model, xTest, yTest.ravel())

#columnNames = getParameterNames(pathToParameterFile2)
#dot_data = tree.export_graphviz(model, out_file='tree.dot', feature_names=columnNames)
#print(model.get_n_leaves())



#print("")

#xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile3)
#model = DecisionTreeRegressor(random_state=77, min_samples_leaf=27)
#model.fit(xTrain, yTrain)
#evaluateModel(model, xTest, yTest)

#print("")

#model = RandomForestRegressor(n_estimators=1000, n_jobs=-1, min_samples_leaf=27)
#model.fit(xTrain, yTrain.ravel())
#evaluateModel(model, xTest, yTest.ravel())
#print(performCrossValidation(xTrain, yTrain, model))

#print("")

#generateAdaBoostForest(xTrain, yTrain.ravel())

#cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)
# define grid
#grid = dict()
#grid['min_samples_leaf'] = [x for x in range(10,30)]
# define search
#search = GridSearchCV(model, grid, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)
# perform the search
#results = search.fit(xTrain, yTrain)
# summarize
#print('MAE: %.3f' % results.best_score_)
#print('Config: %s' % results.best_params_)

#plt.figure(figsize=(10,8), dpi=150)
#plot_tree(model)
#plt.show()
#print("finished")