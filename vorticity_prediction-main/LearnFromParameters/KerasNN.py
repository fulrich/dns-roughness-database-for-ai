import sys
sys.path.insert(0,"../../vorticity_prediction-main")
import functools
from functools import wraps

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from sklearn.model_selection import train_test_split

from tensorflow.keras import datasets, layers, models
from tensorflow import keras
from tensorflow.keras import layers
from LearnFromParameters.ModelEvaluation import evaluatePredictions

from DataVisualization.Modelperformance import plot_Errors3
from DatabaseGeneration.DatabaseGenerator import getTrainingDataDNN, getTrainingDataWithoutCertainParameters

from LearnFromParameters.TrainModule import trainModel

test_results = {}


def evaluateModel(model, testX, testY):
    results = model.evaluate(testX, testY)
    return results

def evaluateModelOwnFunction(model, testXpandas, testYpandas):
    testY = testYpandas.to_numpy()
    predictions = model.predict(testXpandas)
    sum = 0

    for i in range(len(testY)):
        print("model: " + str(predictions[i]) + "   Y_test: " + str(testY[i]))
        sum += abs(predictions[i] - testY[i])
    #The Mean error
    return sum/len(testXpandas)

def generateBiggerModel(norm):
    model = keras.Sequential([
        norm,
        layers.Dense(1000, activation='relu'),
        layers.Dropout(0.3),
        layers.Dense(10000, activation='relu'),
        layers.Dropout(0.5),
        layers.Dense(1000, activation='relu'),
        layers.Dropout(0.5),
        layers.Dense(1)
    ])

    model.compile(loss='mean_absolute_error',
                    optimizer=tf.keras.optimizers.Adam(0.001))
    model.summary()
    return model

def generateDropoutModel():
    model = models.Sequential()
    model.add(layers.Dense(800, input_shape=(50,), activation=tf.keras.layers.LeakyReLU()))
    model.add(layers.BatchNormalization())
    layers.Dropout(0.5),
    #layers.Dense(800, activation='relu'),
    #layers.Dropout(0.5),
    model.add(layers.Dense(400, activation=tf.keras.layers.LeakyReLU()))
    #model.add(layers.Dense(200, activation=tf.keras.layers.LeakyReLU()))
    model.add(layers.BatchNormalization())
    layers.Dropout(0.5)
    model.add(layers.Dense(1, activation=tf.keras.activations.relu))

    model.summary()
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0007),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError(), tf.keras.metrics.MeanAbsolutePercentageError()])
    model.summary()
    return model



def generateSimpleModel(norm):
    model = keras.Sequential([
        norm,
        layers.Dense(600, activation='relu'),
        layers.Dense(400, activation='relu'),
        layers.Dense(1)
    ])

    model.compile(loss='mean_absolute_error',
                    optimizer=tf.keras.optimizers.Adam(0.001))
    model.summary()
    return model


def bestNetHyperparameterOptimization(inputNumber=77):
    model = keras.Sequential()
    model.add(layers.Dense(608, input_shape=(inputNumber,), activation="relu"))
    model.add(layers.Dropout(0.3))
    model.add(layers.BatchNormalization())

    model.add(layers.Dense(192, activation="relu"))
    model.add(layers.Dropout(0.3))
    model.add(layers.BatchNormalization())

    model.add(layers.Dense(922, activation="relu"))
    model.add(layers.Dropout(0.3))
    model.add(layers.BatchNormalization())

    model.add(layers.Dense(1))

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0025),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError(), tf.keras.metrics.MeanAbsolutePercentageError()])
    return model

def get_metric_with_normalization(metric, target_scaler):
    def custom_metric(y_true, y_pred):
        y_pred_scaled = target_scaler.inverse_transform(y_pred)
        y_true_scaled = target_scaler.inverse_transform(y_true)
        return metric(y_true_scaled, y_pred_scaled)

    return custom_metric

def get_metric_with_normalization2(metric, target_scaler):
    def custom_metric(y_true, y_pred):
        y_pred_scaled = target_scaler.inverse_transform(y_pred)
        y_true_scaled = target_scaler.inverse_transform(y_true)
        return functools.partial(metric, y_true_scaled, y_pred_scaled)

    return custom_metric


def transform_metric_values(func):
    @wraps(func)
    def wrapper(y_true, y_pred):
        # Apply your custom transformation logic here
        y_true_transformed = y_true + 1
        y_pred_transformed = y_pred + 1

        # Call the original metric function with the transformed values
        result = func(y_true_transformed, y_pred_transformed)
        return result

    return wrapper

@transform_metric_values
def custom_metric2(y_true, y_pred):
    # Some metric calculation
    return tf.keras.metrics.MeanAbsoluteError(y_true, y_pred)
def custom_metric(y_true, y_pred):
    y_pred_scaled = targetScaler.inverse_transform(y_pred)
    y_true_scaled = targetScaler.inverse_transform(y_true)
    return tf.keras.metrics.MeanAbsoluteError(y_true, y_pred)


def custom_metric3(targetScaler):
    # Define the custom metric function
    def metric(y_true, y_pred):
        # Ensure that y_pred has the correct shape (2D array)
        if len(y_pred.shape) == 1:
            y_pred = y_pred.reshape(-1, 1)

        y_pred_scaled = targetScaler.inverse_transform(y_pred)
        y_true_scaled = targetScaler.inverse_transform(y_true)

        mae_metric = tf.keras.metrics.MeanAbsoluteError()
        mae_metric.update_state(y_true_scaled, y_pred_scaled)
        return mae_metric.result()

    return metric


def modelPaper(inputNumber=77, targetScaler=None):
    model = keras.Sequential()
    model.add(layers.Dense(320, input_shape=(inputNumber,), activation="relu"))
    model.add(layers.Dropout(0.3))
    model.add(layers.BatchNormalization())

    model.add(layers.Dense(760, activation="relu"))
    model.add(layers.Dropout(0.3))
    model.add(layers.BatchNormalization())

    model.add(layers.Dense(960, activation="relu"))
    model.add(layers.Dropout(0.3))
    model.add(layers.BatchNormalization())
    model.add(layers.Dense(1))

    mean_absolute_error = get_metric_with_normalization2(tf.keras.metrics.MeanAbsoluteError(), targetScaler)
    #mean_percentage_error = get_metric_with_normalization(tf.keras.metrics.MeanAbsolutePercentageError(), targetScaler)

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0025), loss=tf.keras.losses.MeanSquaredError())
    return model

def get_metric_with_normalization(metric, target_scaler,y_true, y_pred):
    y_pred_scaled = target_scaler.inverse_transform(y_pred)
    y_true_scaled = target_scaler.inverse_transform(y_true)
    return metric(y_true_scaled, y_pred_scaled)




if __name__ == "__main__":
    print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
    experimentName = "onlyHeights"
    pathWhereModelsShouldBeSaved = "../savedModels/"
    maxVorticitiesFile = "../Databases/vort-dnn.csv"
    pathToParameterFile = "../Databases/database-paper.csv"
    pathToHeightsFile = "../Databases/Heights_32_32.csv"
    #pathToParameterFile = "/home/thomas/Downloads/heilbronn.csv"
    pathToHeightsFile=""
    #pathToParameterFile=""
    xTrain, xTest, yTrain, yTest, targetScaler = getTrainingDataDNN(maxVorticitiesFile, pathToParameterFile,
                                                                    pathToHeightsFile, inputScaler="std_scaler",
                                                                    targetScaler="std_scaler")
    #xTrain, xTest, yTrain, yTest = getTrainingDataWithoutCertainParameters(maxVorticitiesFile, pathToParameterFile2)
    model = modelPaper(inputNumber=xTrain.shape[1], targetScaler=targetScaler)
    #plot_model(simpleModel, to_file='model_plot.png', show_shapes=True, show_layer_names=True)
    model = trainModel(experimentName + "_model", model, xTrain, yTrain, targetScaler=targetScaler,
               pathToModel=pathWhereModelsShouldBeSaved)

    predictions = model.predict(xTest)
    predictions = targetScaler.inverse_transform(predictions)
    evaluatePredictions(yTest, predictions)
    #print( targetScaler.inverse_transform(predictions))
    plot_Errors3(predictions, yTest)

    #simpleModel.evaluate(xTest,yTest)
    # save Predictions / true values
    predictions2 = predictions.flatten()
    yTestFlattened = yTest.flatten()
    df = pd.DataFrame({
        "Predictions" : predictions2,
        "trueValues" : yTestFlattened
    })
    df.to_csv('predictions-true-values.csv')

