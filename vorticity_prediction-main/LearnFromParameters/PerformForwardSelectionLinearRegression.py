from DatabaseGeneration.DatabaseGenerator import getTrainingsDataParameterFile
from sklearn.linear_model import LinearRegression

from LearnFromParameters.ForwardSelcection import performForwardSelectionWorker, performCrossValidation

maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"
pathToParameterFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/ParameterDatabases/AllParameters.csv"

xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile)
linearModel = LinearRegression()
performForwardSelectionWorker(xTrain, yTrain, linearModel, 2, 8)
#print(performCrossValidation(xTrain, yTrain, linearModel))
