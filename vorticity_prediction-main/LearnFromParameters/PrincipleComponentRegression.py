import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale, PolynomialFeatures
from sklearn import model_selection
from sklearn.model_selection import RepeatedKFold
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn import metrics
from DatabaseGeneration.DatabaseGenerator import getTrainingsDataParameterFile, getTrainingDataWithoutCertainParameters
import statsmodels.api as sm
from sklearn.linear_model import Ridge, ElasticNet, Lasso

# Code adapted from https://www.statology.org/principal-components-regression-in-python/
from LearnFromParameters.ModelEvaluation import evaluatePredictions


def getData(pathToParameterFile):
    parameterDf = pd.read_csv("../Databases/ParameterDatabases/ParameterFirst10Batches.csv", skipinitialspace=True)
    yData = parameterDf[["maxVorticity"]]
    parameterDf = parameterDf.drop(columns="maxVorticity")
    parameterDf = parameterDf.drop(columns="simulationName")
    # xData = parameterDf.to_numpy()
    # xData = xData.transpose()
    return parameterDf, yData

def test(pc1Value, pc1Values, parameterValues):
    myPc1Value = sum(pc1Values * parameterValues)
    print("My pc1-value:      " + str(myPc1Value))
    print("Program-pc1-value: " + str(pc1Value))

def getMostImportantParamterOfPC(threshold, pc, parmeterNames):
    pc = list(map(abs, pc))
    sumValue = sum(pc)
    pc = list(map(lambda value: value / sumValue, pc))
    pairs = list(map(list, zip(pc, parameterNames)))
    filtered = list(filter(lambda argument: argument[0] > threshold, pairs))
    sortedValues = sorted(filtered, key=lambda pair: pair[0], reverse=True)
    print(sortedValues)


def getMostImportantParamtersOfPCs(xData, threshold, parameterNames):
    pca = PCA()
    xAnalyzed = pca.fit_transform(xData)
    pcs = pca.components_
    for i in range(len(pcs)):
        print("PC"+ str(i) +  " : ")
        getMostImportantParamterOfPC(threshold, pcs[i], parameterNames)

def trainLinearRegression(xTrain, xTest, yTrain, yTest):
    regr = LinearRegression()
    regr.fit(xTrain, yTrain)
    pred = regr.predict(xTest)
    meanAbsoluteError = metrics.mean_absolute_error(yTest, pred)
    meanAbsoluteErrors.append(meanAbsoluteError)
    print("Mean absolute Error Linear-Regression: " + str(meanAbsoluteError))

def fitLinearRegressionWithStatsModel(xTrain, xTest, yTrain, yTest):
    model = sm.OLS(yTrain, xTrain).fit()
    print_model = model.summary()
    print(print_model)
    pred = model.predict(xTest)

    meanAbsoluteError = metrics.mean_absolute_percentage_error(yTest, pred)
    meanAbsoluteErrors.append(meanAbsoluteError)
    print("Mean absolute Error Linear-Regression: " + str(meanAbsoluteError))

def plotExplainedVarianceOfPC():
    test(X_reduced[0][0], pca.components_[0], xData[0][:])
    # print("weights: " + str(pca.components_[0]))
    per_var = np.round(pca.explained_variance_ratio_ * 100, decimals=1)[:50]
    labels = [str(x) for x in range(1, len(per_var) + 1)]
    plt.figure(figsize=(9, 5), dpi=400)
    plt.rc('xtick', labelsize=7)
    plt.bar(x=range(1, len(per_var - 30) + 1), height=per_var, tick_label=labels)
    plt.ylabel('Erklärte Varianz pro Hauptkomponente [%]')

    plt.xlabel('Hauptkomponenten [-]')
    # plt.title('Scree Plot')
    plt.savefig("ExplainedVariancePerPC.png", dpi=400)
    plt.show()

    print("Explained Variance:")
    print(np.cumsum(np.round(pca.explained_variance_ratio_, decimals=12) * 100))

def polynomialFeaturesWithInteraction(xTrain, xTest):
    poly = PolynomialFeatures(2, interaction_only=False, include_bias=False)
    # npArray = np.asarray(values)
    newXTrain = poly.fit_transform(xTrain)
    newXTest = poly.fit_transform(xTest)
    return pd.DataFrame(newXTrain), pd.DataFrame(newXTest)

#get old dataset
xData, yData = getData("../Databases/MaxVorticityFiles/")
#get new dataset
maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"
pathToParameterFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/ParameterDatabases/AllParameters.csv"
xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile)
#xTrain, xTest = polynomialFeaturesWithInteraction(xTrain, xTest)
print(xTrain.shape)
xData = xTrain
yData = yTrain

pca = PCA()
X_reduced = pca.fit_transform(xData)
parameterNames = xData.columns.tolist()
xData = scale(xData)

#getMostImportantParamtersOfPCs(xData, 0.1, parameterNames)

plotExplainedVarianceOfPC()

#print(xData.shape)
#for i in range(len(xData)):
#    xData[i][0] = 0
#    xData[i][1] = 0
#    xData[i][2] = 2 * xData[i][3]



#loading_scores = pca.components_.T * np.sqrt(pca.explained_variance_)
#pcaComponent = pca.components_.T
#pcaComponentDf = pd.DataFrame(pcaComponent, columns=["PC_" + str(x) for x in range(1, len(per_var) + 1)])
#pcaComponentDf["variable"] = parameterNames
#pcaComponentDf.to_csv("PrincipalComponents.csv", index=False)

#print("Training a linear Regression Model")
#cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)
#regr = LinearRegression()
#mse = []
# Calculate MSE with only the intercept
#score = -1 * model_selection.cross_val_score(regr,
#                                             np.ones((len(X_reduced), 1)), yData, cv=cv,
#                                             scoring='neg_mean_absolute_percentage_error').mean()
#mse.append(score)

# Calculate MSE using cross-validation, adding one component at a time
#for i in np.arange(1, 77):
#    trainDataX = X_reduced[:, :i]
#    score = -1 * model_selection.cross_val_score(regr, trainDataX, yData, cv=cv,
#                                                 scoring='neg_mean_absolute_percentage_error').mean()
#    print(score)
#    mse.append(score)

# Plot cross-validation results
#plt.plot(mse)
#plt.xlabel('Number of Principal Components')
#plt.ylabel('MSE')
#plt.title('Cross-validation')
#plt.show()


#xTrain, xTest, yTrain, yTest = train_test_split(xData, yData, test_size=0.2, random_state=0)
# scale the training and testing data
X_reduced_train = pca.fit_transform(xTrain)

X_reduced_test = pca.transform(xTest)[:, :50]
regr = LinearRegression()
regr.fit(X_reduced_train[:, :50], yTrain)
pred = regr.predict(X_reduced_test)
evaluatePredictions(yTest, pred)
#meanAbsoluteError = metrics.mean_absolute_percentage_error(yTest, pred)
#print(meanAbsoluteError)

exit(1)

meanAbsoluteErrors = []
for i in np.arange(1, 77):
    X_reduced_test = pca.transform(xTest)[:, :i]
    regr = LinearRegression()
    regr.fit(X_reduced_train[:, :i], yTrain)
    pred = regr.predict(X_reduced_test)
    meanAbsoluteError = metrics.mean_absolute_percentage_error(yTest, pred)
    print(meanAbsoluteError)
    meanAbsoluteErrors.append(meanAbsoluteError)

plt.plot(meanAbsoluteErrors)
plt.xlabel('Number of Principal Components')
plt.ylabel('meanAbsoluteError')
plt.title('Result on test data set')
plt.show()
print("Mean absolute Error PC-Regression: " + str(meanAbsoluteErrors[-1]))

print("Scipy Linear Regression:")
trainLinearRegression(xTrain, xTest, yTrain, yTest)
print("Statsmodel linear regression:")
fitLinearRegressionWithStatsModel(xTrain, xTest, yTrain, yTest)
