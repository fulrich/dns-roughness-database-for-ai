import sklearn
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
import pandas as pd
import statsmodels.api as sm
from DatabaseGeneration.DatabaseGenerator import getTrainingDataDNN

import numpy as np
from numpy import arange
from pandas import read_csv
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RepeatedKFold
from sklearn.linear_model import Ridge, ElasticNet, Lasso
from sklearn.linear_model import ElasticNetCV
from sklearn.model_selection import RepeatedKFold
from sklearn import preprocessing
import statsmodels.api as sm
from sklearn import metrics

def evaluatePredictions(yTest, predictions, printMetricResults=True):
    mean_absolute_error = metrics.mean_absolute_error(yTest, predictions)
    maxError = metrics.max_error(yTest, predictions)
    meanPercentageError = sklearn.metrics.mean_absolute_percentage_error(yTest, predictions) * 100
    maxPercentageError, wrongPredictedValue = calculateMaxPercentageError(yTest, predictions)

    metricsDict = {"MAE": mean_absolute_error, "MAXE": maxError, "MPE": meanPercentageError}

    if printMetricResults:
        #print("Mean absolute error: " + str(mean_absolute_error))
        #print("Max Error: " + str(maxError))
        #print("Mean percentage Error: " + str(meanPercentageError))
        #print("Max percentage error: " + str(maxPercentageError) +
        #      " most wrong predicted value: " + str(wrongPredictedValue))
        printString = ""
        for name, value in metricsDict.items():
            if name == "MPE":
                printString += name + ": " + str(round(value, 4)) + "%"
            else:
                printString += name + ": " + str(round(value, 4)) + "\t"
        print(printString)
    return metricsDict


def calculateMaxPercentageError(yTest, predictions):
    maxPercentageError = -100000
    wrongPredictedValue = 0
    for i in range(len(predictions)):
        percentageError = abs((predictions[i]-yTest[i]) / yTest[i])
        if percentageError > maxPercentageError:
            wrongPredictedValue = yTest[i]
            maxPercentageError = percentageError

    return maxPercentageError, wrongPredictedValue

def evaluateModel(model, xTest, yTest):
    predictions = model.predict(xTest)
    return evaluatePredictions(yTest, predictions)

def evaluateMeanPredictor(yTrain, yTest):
    trainMean = np.mean(yTrain)
    predictions = np.full(len(yTest), trainMean)
    return evaluatePredictions(yTest, predictions)

def hyperparemterOptimization2(xTrain, xTest, yTrain, yTest, degree):
    poly = PolynomialFeatures(degree, interaction_only=False)

    transformedTrainingData = poly.fit_transform(xTrain)
    transformedTestingData = poly.transform(xTest)
    # define model
    model = ElasticNet()
    # define model evaluation method
    cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)
    # define grid
    grid = dict()
    grid['alpha'] = [1e-6, 1e-5, 1e-5,  1e-3]
    grid['l1_ratio'] = arange(0, 1, 0.05)
    # define search
    search = GridSearchCV(model, grid, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)
    # perform the search
    results = search.fit(transformedTrainingData, yTrain)
    # summarize
    print('MAE: %.3f' % results.best_score_)
    print('Config: %s' % results.best_params_)

    model = ElasticNet(alpha=results.best_params_['alpha'], l1_ratio=results.best_params_['l1_ratio'])
    model = model.fit(transformedTrainingData, yTrain)
    evaluateModel(model, transformedTestingData, yTest)

