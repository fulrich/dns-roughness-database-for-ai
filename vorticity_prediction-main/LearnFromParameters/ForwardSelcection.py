import pickle
from audioop import reverse

from sklearn.metrics import make_scorer
from sklearn.model_selection import cross_validate, RepeatedKFold
from sklearn.model_selection import KFold

import numpy as np
from collections import Counter


def performCrossValidation(xTrain, yTrain, model):
    scoring = {'mean_absolute_percentage_error': 'neg_mean_absolute_percentage_error',
               'mean_squared_error': 'neg_mean_squared_error',
               'mean_absolute_error': 'neg_mean_absolute_error',
               'max_error': 'max_error'}
    kf = RepeatedKFold(n_splits=5, n_repeats = 1, random_state=2022)
    #result = cross_validate(estimator=model, X=xTrain, y=yTrain, scoring=make_scorer(score_func=evaluateModel2, greater_is_better=False), cv=kf, n_jobs=-1)
    result = cross_validate(estimator=model, X=xTrain, y=yTrain,
                            scoring=scoring, cv=kf, n_jobs=-1)

    myResult = dict()

    myResult["mean_absolute_percentage_error"] = result["test_mean_absolute_percentage_error"].mean() * -1
    myResult["mean_squared_error"] = result["test_mean_squared_error"].mean() * -1
    myResult["mean_absolute_error"] = result["test_mean_absolute_error"].mean() * -1
    myResult["max_error"] = result["test_max_error"].mean() * -1

    #print(myResult)
    return myResult


def performForwardSelection(xTrain, yTrain, model, numPermutations):
    numberColumns = len(xTrain.columns)
    permutations = getPermutations(numberColumns, numPermutations, 2022)

    improvementList = []
    errorsBefore = None


    omittedParametersCounter = np.zeros(numberColumns)
    for i in range(numberColumns):
        emptyImprovementDict = {
            "mean_absolute_percentage_error": 0,
            "mean_squared_error": 0,
            "mean_absolute_error": 0,
            "max_error": 0
        }
        improvementList.append(emptyImprovementDict)

    for i in range(numPermutations):
        print("Permutation: " + str(i))
        removedIndices = []

        for j in range(numberColumns):
            indicesOfAddedParameters = permutations[i][:j+1]
            currentParameterIndex = permutations[i][j]
            indicesOfAddedParameters = [index for index in indicesOfAddedParameters if index not in removedIndices]
            subset = xTrain.iloc[:, indicesOfAddedParameters]
            crossValidationResult = performCrossValidation(subset, yTrain, model)
            #devideDictByCertainValue(crossValidationResult, numPermutations)

            if j == 0:
                currentMeanSquaredError = crossValidationResult["mean_squared_error"]
                meanSquaredErrorBefore = currentMeanSquaredError
                errorsBefore = crossValidationResult
            else:
                multiplyDictByCertainValue(crossValidationResult, -1)
                improvement = addTwoDicts(errorsBefore, crossValidationResult)

                multiplyDictByCertainValue(crossValidationResult, -1)
                currentMeanSquaredError = crossValidationResult["mean_squared_error"]
                improvementList[currentParameterIndex] = addTwoDicts(improvementList[currentParameterIndex], improvement)

                #If the model gets worse the parameter gets omitted
                if meanSquaredErrorBefore < currentMeanSquaredError:
                    omittedParametersCounter[currentParameterIndex] += 1
                    removedIndices.append(currentParameterIndex)
                else:
                    meanSquaredErrorBefore = currentMeanSquaredError
                    errorsBefore = crossValidationResult


            #print(resultList[j]["mean_absolute_percentage_error"])


    #print(xTrain.columns)

    #improvementList = sorted(improvementList, key=lambda x: x["mean_absolute_percentage_error"], reverse=True)
    improvementsDict = dict(zip(xTrain.columns, improvementList))
    improvementsDict = dict(sorted(improvementsDict.items(), key=lambda item: item[1]["mean_absolute_percentage_error"], reverse=True))
    print("improvements:")
    print(improvementsDict)
    print(improvementsDict.keys())


    omittedParametersDict = dict(zip(xTrain.columns, omittedParametersCounter))
    omittedParametersDict = dict(sorted(omittedParametersDict.items(), key=lambda item: item[1]))
    print(omittedParametersDict)

    print("better Model:")
    bestParametersKeys = omittedParametersDict.keys()
    bestParameters = list(bestParametersKeys)[:numberColumns-22]
    print(bestParameters)
    bestInputs = xTrain[bestParameters]
    crossValidationResult = performCrossValidation(bestInputs, yTrain, model)
    print(crossValidationResult)

    print("bad Model:")
    bestParameters = list(bestParametersKeys)[22:]
    print(bestParameters)
    bestInputs = xTrain[bestParameters]
    crossValidationResult = performCrossValidation(bestInputs, yTrain, model)
    print(crossValidationResult)

    return improvementList, omittedParametersCounter


def performForwardSelectionWorker(xTrain, yTrain, model, numPermutations, workerIndex):
    numberColumns = len(xTrain.columns)
    permutations = getPermutations(numberColumns, numPermutations, workerIndex)

    improvementList = []
    errorsBefore = None


    omittedParametersCounter = np.zeros(numberColumns)
    for i in range(numberColumns):
        emptyImprovementDict = {
            "mean_absolute_percentage_error": 0,
            "mean_squared_error": 0,
            "mean_absolute_error": 0,
            "max_error": 0
        }
        improvementList.append(emptyImprovementDict)

    for i in range(numPermutations):
        print("Permutation: " + str(i))
        removedIndices = []

        for j in range(numberColumns):
            indicesOfAddedParameters = permutations[i][:j+1]
            currentParameterIndex = permutations[i][j]
            indicesOfAddedParameters = [index for index in indicesOfAddedParameters if index not in removedIndices]
            subset = xTrain.iloc[:, indicesOfAddedParameters]
            crossValidationResult = performCrossValidation(subset, yTrain, model)
            #devideDictByCertainValue(crossValidationResult, numPermutations)

            if j == 0:
                currentMeanSquaredError = crossValidationResult["mean_squared_error"]
                meanSquaredErrorBefore = currentMeanSquaredError
                errorsBefore = crossValidationResult
            else:
                multiplyDictByCertainValue(crossValidationResult, -1)
                improvement = addTwoDicts(errorsBefore, crossValidationResult)

                multiplyDictByCertainValue(crossValidationResult, -1)
                currentMeanSquaredError = crossValidationResult["mean_squared_error"]
                improvementList[currentParameterIndex] = addTwoDicts(improvementList[currentParameterIndex], improvement)

                #If the model gets worse the parameter gets omitted
                if meanSquaredErrorBefore < currentMeanSquaredError:
                    omittedParametersCounter[currentParameterIndex] += 1
                    removedIndices.append(currentParameterIndex)
                else:
                    meanSquaredErrorBefore = currentMeanSquaredError
                    errorsBefore = crossValidationResult



    improvementsDict = dict(zip(xTrain.columns, improvementList))
    improvementsDict = dict(sorted(improvementsDict.items(), key=lambda item: item[1]["mean_absolute_percentage_error"], reverse=True))



    omittedParametersDict = dict(zip(xTrain.columns, omittedParametersCounter))
    omittedParametersDict = dict(sorted(omittedParametersDict.items(), key=lambda item: item[1]))

    saveDict(improvementsDict, "ImprovementsDict_" + str(workerIndex))
    saveDict(omittedParametersDict, "OmittedParametersDict_" + str(workerIndex))

    return improvementList, omittedParametersCounter

def devideDictByCertainValue(inputDict, value):
    for k, v in inputDict.items():
        inputDict[k] = v / value

def multiplyDictByCertainValue(inputDict, value):
    for k, v in inputDict.items():
        inputDict[k] = v * value


def saveDict(dictionary, filePath):
    with open(filePath + '.pkl', 'wb') as f:
        pickle.dump(dictionary, f)

def readDict(filePathToPicleDic):
    with open(filePathToPicleDic + '.pkl', 'rb') as f:
        loaded_dict = pickle.load(f)
        return loaded_dict

def addTwoDicts(dict1, dict2):
    counterDict1 = Counter(dict1)
    counterDict2 = Counter(dict2)
    counterDict1.update(counterDict2)
    return dict(counterDict1)


def getPermutations(numValues, numPermutations, seed):
    permutations = []
    np.random.seed(seed)
    #The first iteration is the normal order:
    first = np.empty(numValues, int)
    for i in range(numValues):
        first[i] = int(i)
    permutations.append(first)

    for i in range(numPermutations-1):
        permutations.append(np.random.permutation(numValues))

    return permutations