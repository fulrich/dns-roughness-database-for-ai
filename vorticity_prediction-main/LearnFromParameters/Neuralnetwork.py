from DatabaseGeneration.DatabaseGenerator import getTrainingsDataParameterFile
from sklearn.neural_network import MLPRegressor
from sklearn.datasets import make_regression

from LearnFromParameters.ModelEvaluation import evaluatePredictions

maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"
pathToParameterFile = "../Databases/ParameterDatabases/ParameterFileForAll15kSimulations.csv"
pathToParameterFile2 = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/ParameterDatabases/AllParameters.csv"
xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile2)

regr = MLPRegressor(random_state=1, max_iter=50, hidden_layer_sizes=(500,100),
                    early_stopping=True, n_iter_no_change=10, learning_rate="adaptive", batch_size=30).fit(xTrain.values,yTrain.values)
predictions = regr.predict(xTest.values)
evaluatePredictions(yTest.values, predictions)