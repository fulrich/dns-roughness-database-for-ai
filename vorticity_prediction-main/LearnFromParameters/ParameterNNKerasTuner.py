from tensorflow import keras
import keras_tuner as kt
import tensorflow as tf
from tensorflow.keras import layers

from DatabaseGeneration.DatabaseGenerator import getTrainingsDataParameterFile, getTrainingDataWithoutCertainParameters
from LearnFromParameters.ModelEvaluation import evaluatePredictions

from tensorflow.keras import Model
from tensorflow.keras.utils import plot_model

numberParameters = 77


def model_builder(hp):
    model = keras.Sequential()
    model.add(keras.layers.Flatten(input_shape=(numberParameters,)))

    # Tune the number of units in the first Dense layer
    dropout = hp.Float("dropout", min_value=0.2, max_value=0.6)
    hp_units_1 = hp.Int('hidden_units_1', min_value=32, max_value=1024, step=32)
    hp_units_2 = hp.Int('hidden_units_2', min_value=32, max_value=1024, step=32)
    hp_units_3 = hp.Int('hidden_units_3', min_value=32, max_value=1024, step=32)

    hp_learning_rate = hp.Float("lr", min_value=1e-5, max_value=1e-2, sampling="linear")
    activationFunction = "relu"

    model.add(layers.Dense(hp_units_1, input_shape=(77,), activation=activationFunction))
    model.add(layers.Dropout(dropout))
    model.add(layers.BatchNormalization())

    model.add(layers.Dense(hp_units_2, activation=activationFunction))
    model.add(layers.Dropout(dropout))
    model.add(layers.BatchNormalization())

    model.add(layers.Dense(hp_units_3, activation=activationFunction))
    model.add(layers.Dropout(dropout))
    model.add(layers.BatchNormalization())

    model.add(layers.Dense(1))

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=hp_learning_rate),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.MeanAbsoluteError(), tf.keras.metrics.MeanAbsolutePercentageError()])

    return model


def trainModel(model, xTrain, xTest, yTrain, yTest):
    # xTrain, xTest, yTrain, yTest = createTestAndTrainingsDataSet2("../Databases/HeightsDatabases/First10BatchesHeightsDB.csv")

    # model = generateDropoutModel()
    earlyStoppingCallback = tf.keras.callbacks.EarlyStopping(monitor='val_mean_absolute_percentage_error', patience=10,
                                                             restore_best_weights=True, mode="min")

    history = model.fit(xTrain, yTrain, epochs=100, validation_freq=1, batch_size=64,
                        validation_split=0.1, callbacks=[earlyStoppingCallback])
    # plotTrainingHistory(history)

    print("Evaluation: ")
    model.evaluate(xTest, yTest)
    return model


def getTrainingsData(xData, yData):
    numberOfTrainingsData = None


tuner = kt.BayesianOptimization(model_builder,
                                objective='val_mean_absolute_percentage_error',
                                directory='my_dir',
                                seed=100,
                                max_trials=30)

maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"
pathToParameterFile = "../Databases/ParameterDatabases/ParameterFileForAll15kSimulations.csv"
pathToParameterFile2 = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/ParameterDatabases/AllParameters.csv"
xTrain, xTest, yTrain, yTest = getTrainingDataWithoutCertainParameters(maxVorticitiesFile, pathToParameterFile2)

stop_early = tf.keras.callbacks.EarlyStopping(monitor='val_mean_absolute_percentage_error', patience=10)
tuner.search(xTrain, yTrain, epochs=50, validation_split=0.2, callbacks=[stop_early], batch_size=64)
best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]
#best_hps.values["dropout"] = 0.3
#best_hps.values["lr"] = 0.005
print(best_hps.values)
print(type(best_hps))

hypermodel = tuner.hypermodel.build(best_hps)
#plot_model(hypermodel, to_file='model_plot.png', show_shapes=True, show_layer_names=True)
hypermodel.layers.pop(0)
newInput = layers.Dense(608, input_shape=(50,), activation="relu")
newOutputs = hypermodel(newInput)
hypermodel = Model(newInput, newOutputs)

bestmodel = trainModel(hypermodel, xTrain, xTest, yTrain, yTest)
predictions = bestmodel.predict(xTest)
evaluatePredictions(yTest, predictions)