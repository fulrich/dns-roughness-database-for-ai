import tensorflow as tf
from sklearn.model_selection import train_test_split


from DataVisualization.Modelperformance import plotTrainingHistory2, plot_Errors3
from LearnFromParameters.ModelEvaluation import evaluatePredictions
from keras.callbacks import Callback


class ValidationMetricCallback(Callback):
    def __init__(self, target_scaler, validation_data=None):
        super(ValidationMetricCallback, self).__init__()
        self.target_scaler = target_scaler
        self.validation_data = validation_data

    def on_epoch_end(self, epoch, logs=None):
        # Scale the target values after each epoch
        y_pred = self.model.predict(self.validation_data[0])
        # Apply your custom transformation to the predictions
        transformed_predictions = self.target_scaler.inverse_transform(y_pred)
        metrics_results = evaluatePredictions(self.validation_data[1], transformed_predictions)

        for metric_name, metric_value in metrics_results.items():
            logs['val_' + metric_name] = metric_value
def trainModel(modelName, model, xTrain, yTrain, targetScaler=None, pathToModel=""):
    # xTrain, xTest, yTrain, yTest = createTestAndTrainingsDataSet2("../Databases/HeightsDatabases/First10BatchesHeightsDB.csv")

    #model = generateDropoutModel(targetScaler)

    xTrain, xVal, yTrain, yVal = train_test_split(xTrain, yTrain, test_size=0.1, random_state=0)
    yVal = targetScaler.inverse_transform(yVal)
    validationCallback = ValidationMetricCallback(targetScaler, validation_data=(xVal, yVal))

    earlyStoppingCallback = tf.keras.callbacks.EarlyStopping(monitor='val_MPE', patience=10,
                                                             restore_best_weights=True, mode="min")


    history = model.fit(xTrain, yTrain, epochs=100, validation_freq=1, batch_size=64,
                        validation_data=(xVal, yVal), callbacks=[validationCallback, earlyStoppingCallback])
    plotTrainingHistory2(history)

    print("Evaluation on test data")

    # Save the best model with whole structure
    model.save(pathToModel + modelName)

    return model