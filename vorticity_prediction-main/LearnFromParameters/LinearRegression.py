from pandas import DataFrame
import statsmodels.api as sm

from sklearn.preprocessing import PolynomialFeatures, StandardScaler
from sklearn import linear_model
import pandas as pd
import statsmodels.api as sm
from DatabaseGeneration.DatabaseGenerator import getTrainingsDataParameterFile, getTrainingDataWithoutCertainParameters, \
    getParameterNames
from DatabaseGeneration.DatabaseGenerator import getTrainingsDataOfTwoParameterFiles
from ModelEvaluation import evaluateModel, evaluateMeanPredictor
import numpy as np
from numpy import arange
from pandas import read_csv
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RepeatedKFold
from sklearn.linear_model import Ridge, ElasticNet, Lasso
from sklearn.linear_model import ElasticNetCV
from sklearn.model_selection import RepeatedKFold
from sklearn import preprocessing
import statsmodels.api as sm
from sklearn import metrics
from sklearn.preprocessing import scale

from ForwardSelcection import performForwardSelection, performCrossValidation

def fitLinearModel(xTrain, yTrain):
    linearModel2 = linear_model.LinearRegression()
    linearModel2.fit(xTrain, yTrain)
    return linearModel2


def polynomialFeaturesTest():
    X=np.array([[1,2]])
    poly = PolynomialFeatures(2, interaction_only=False)
    #npArray = np.asarray(values)
    X_poly=poly.fit_transform(X)
    #transformedTrainingData = poly.fit_transform(values)
    print(poly.get_feature_names(["x_1", "x_2", "x_3", "x_4"]))
    print(X_poly)
    print(X_poly.shape[1])

def calculateMeanAbsoluteDeviation(yTrain, yTest):
    trainMean = np.mean(yTrain)
    meanAbsoluteDeviation = np.mean(np.absolute(yTest - trainMean))
    maxAbsoluteDeviation = np.max(np.absolute(yTest - trainMean))

    print("Mean absoulte Deviation: " + str(meanAbsoluteDeviation))
    print("Max absoulte Deviation: " + str(maxAbsoluteDeviation))

def polynomialRegression():
    #model = sm.ols(formula = 'a ~ np.power(b, 2) + b + c', data = data).fit()
    return 0

def hyperparemterOptimization(xTrain, xTest, yTrain, yTest, degree):
    #poly = PolynomialFeatures(degree, interaction_only=False)

    #transformedTrainingData = poly.fit_transform(xTrain)
    #transformedTestingData = poly.transform(xTest)
    # define model
    #model = Ridge()
    model = Lasso()
    # define model evaluation method
    cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)
    # define grid
    grid = dict()
    grid['alpha'] = [0.01, 0.001, 0.0001]
    # define search
    search = GridSearchCV(model, grid, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)
    # perform the search
    results = search.fit(xTrain, yTrain)
    print(results.best_params_['alpha'])
    # summarize
    print('MAE: %.3f' % results.best_score_)
    print('Config: %s' % results.best_params_)

    #model = Ridge(alpha=results.best_params_['alpha'])
    model = Lasso(alpha=results.best_params_['alpha'])
    model = model.fit(xTrain, yTrain)
    evaluateModel(model, xTest, yTest)
    zipped = list(zip(getParameterNames("/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/"
                                        "RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"), model.coef_))
    print(zipped)

def hyperparemterOptimization2(xTrain, xTest, yTrain, yTest, degree):
    poly = PolynomialFeatures(degree, interaction_only=False)

    transformedTrainingData = poly.fit_transform(xTrain)
    transformedTestingData = poly.transform(xTest)
    # define model
    model = ElasticNet()
    # define model evaluation method
    cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)
    # define grid
    grid = dict()
    grid['alpha'] = [1e-6, 1e-5, 1e-5,  1e-3]
    grid['l1_ratio'] = arange(0, 1, 0.05)
    # define search
    search = GridSearchCV(model, grid, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)
    # perform the search
    results = search.fit(transformedTrainingData, yTrain)
    # summarize
    print('MAE: %.3f' % results.best_score_)
    print('Config: %s' % results.best_params_)

    model = ElasticNet(alpha=results.best_params_['alpha'], l1_ratio=results.best_params_['l1_ratio'])
    model = model.fit(transformedTrainingData, yTrain)
    evaluateModel(model, transformedTestingData, yTest)

def elasticNetRegression(xTrain, xTest, yTrain, yTest, degree):
    #poly = PolynomialFeatures(degree, interaction_only=False)
    #poly = PolynomialFeatures(degree, interaction_only=False)

    #transformedTrainingData = poly.fit_transform(xTrain)
    #transformedTestingData = poly.transform(xTest)

    # define model
    cv = RepeatedKFold(n_splits=5, n_repeats=1, random_state=1)
    # define model
    ratios = arange(0, 1, 0.1)
    alphas = [0.0001, 0.001, 0.01, 0.1, 0.3, 0.5, 0.7, 1]
    model = ElasticNetCV(l1_ratio=ratios, alphas=alphas, cv=cv, n_jobs=-1, max_iter=10000)
    # fit model
    model.fit(xTrain, yTrain)
    # summarize chosen configuration
    print('alpha: %f' % model.alpha_)
    print('l1_ratio_: %f' % model.l1_ratio_)
    evaluateModel(model, xTest, yTest)


def fitLinearRegressionWithStatsModel(xTrain, xTest, yTrain, yTest):
    model = sm.OLS(yTrain, xTrain).fit()
    print_model = model.summary()
    print(print_model)
    pred = model.predict(xTest)

    meanAbsoluteError = metrics.mean_absolute_error(yTest, pred)
    #meanAbsoluteErrors.append(meanAbsoluteError)
    print("Mean absolute Error Linear-Regression: " + str(meanAbsoluteError))

def polynomialFeaturesWithoutAnyInteraction(xTrain, xTest):
    scaler = StandardScaler()
    newXTrain = np.hstack((xTrain, xTrain**2, xTrain**3, xTrain**4))
    newXTest = np.hstack((xTest, xTest**2, xTest**3, xTest**4))
    return newXTrain, newXTest
    #return scaler.fit_transform(newXTrain), scaler.fit(newXTest)



def polynomialFeaturesWithInteraction(xTrain, xTest):
    poly = PolynomialFeatures(2, interaction_only=False, include_bias=False)
    # npArray = np.asarray(values)
    newXTrain = poly.fit_transform(xTrain)
    newXTest = poly.fit_transform(xTest)
    return newXTrain, newXTest

maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/MaxVorticityDatabases/1_6_0_All.csv"
pathToParameterFile = "../Databases/ParameterDatabases/ParameterFileForAll15kSimulations.csv"
pathToParameterFile2 = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/ParameterDatabases/AllParameters.csv"

xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile)
#print(xTrain.head())

#lassoModel = linear_model.Lasso(max_iter=100000)
#createPolynomialRegression(xTrain, xTest, yTrain, yTest, 2, lassoModel)

#ridgeRegression = linear_model.Ridge(alpha=0.5)
#createPolynomialRegression(xTrain, xTest, yTrain, yTest, 2, ridgeRegression)

#linearModel = fitLinearModel(xTrain, yTrain)
#evaluateModel(linearModel, xTest, yTest)
#fitLinearRegressionWithStatsModel(xTrain, xTest, yTrain, yTest)


#xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile)

#maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/1_6_0_Without_Duplicates.csv"
print("All Parameters:")
xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile2)
xTrainPoly, xTestPoly = polynomialFeaturesWithoutAnyInteraction(xTrain, xTest)
#model = Lasso(alpha=0.001)
#model = Ridge(alpha=0.1)
#model = linear_model.LinearRegression()
#ratios = arange(0, 1, 0.5)
#alphas = [0.001, 0.01, 0.1, 1.0]
#model = ElasticNetCV(l1_ratio=ratios, alphas=alphas, cv=None, n_jobs=-1, max_iter=10000)

#model = model.fit(xTrainPoly, yTrain)
#evaluateModel(model, xTestPoly, yTest)


#print("Reduced Parameters:")
xTrain, xTest, yTrain, yTest = getTrainingDataWithoutCertainParameters(maxVorticitiesFile, pathToParameterFile2)
#xTrainPoly, xTestPoly = polynomialFeaturesWithoutAnyInteraction(xTrain, xTest)
model = linear_model.LinearRegression()
model = model.fit(xTrain, yTrain)
#evaluateModel(model, xTestPoly, yTest)

#linearModel = fitLinearModel(xTrain, yTrain)
#evaluateModel(linearModel, xTest, yTest)
#elasticNetRegression(xTrain,xTest,yTrain,yTest, 1)
#hyperparemterOptimization(scale(xTrain), scale(xTest), yTrain, yTest, 1)

#model = Ridge(alpha=0.1)
#model = model.fit(xTrainPoly, yTrain)
#evaluateModel(model, xTestPoly, yTest)
parameterNames = xTrain.columns
coefitients = model.coef_

zipped = list(zip(parameterNames, coefitients[0]))
print(sorted(zipped, key=lambda tup: abs(tup[1]), reverse=True))

dfs = sorted(zipped, key=lambda tup: tup[0])
for pair in dfs:
    print(pair)
#for k in dfs:
#    print(k)
#evaluateModel(model, xTest, yTest)

#fitLinearRegressionWithStatsModel(xTrain, xTest, yTrain, yTest)
#print(xTrain["meanDhdx"].max())
#print(xTrain["maxHeight"].max())
#print(xTrain["maximumSumSlopeChanges"].min())
#print(xTrain["Iz"].max())

#print(xTrain.loc[xTrain["meanDhdx"].idxmin()])



#print("Reduced Parameters:")
#xTrain, xTest, yTrain, yTest = getTrainingDataWithoutCertainParameters(maxVorticitiesFile, pathToParameterFile2)
#hyperparemterOptimization(xTrain, xTest, yTrain, yTest, 1)
#linearModel = fitLinearModel(xTrain, yTrain)
#evaluateModel(linearModel, xTest, yTest)
#print(performCrossValidation(xTrain, yTrain,linearModel))

#evaluateMeanPredictor(yTrain, yTest)

#linearModel = linear_model.LinearRegression()
#evaluateModel(xTrain, yTrain, linearModel)

#fitLinearRegressionWithStatsModel(xTrain, xTest, yTrain, yTest)

#xTrain = xTrain.iloc[:, :20]
#performForwardSelection(xTrain, yTrain, linearModel, 10)

#polynomialFeaturesTest()
#calculateMeanAbsoluteDeviation(yTrain, yTest)

#hyperparemterOptimization(xTrain, xTest, yTrain, yTest, 2)
#hyperparemterOptimization2(xTrain, xTest, yTrain, yTest, 1)
#elasticNetRegression(xTrain, xTest, yTrain, yTest, 2)

#calculateMeanAbsoluteDeviation(yTrain, yTest)

#new_X = np.hstack((X, X**2))