from DatabaseGeneration.DatabaseGenerator import getTrainingsDataParameterFile
from ModelEvaluation import evaluateMeanPredictor

maxVorticitiesFileOld = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/1_6_0_Without_Duplicates.csv"
maxVorticitiesFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/1_6_0_All.csv"
pathToParameterFile = "../Databases/ParameterDatabases/ParameterFileForAll15kSimulations.csv"
xTrain, xTest, yTrain, yTest = getTrainingsDataParameterFile(maxVorticitiesFile, pathToParameterFile)
xTrainOld, xTestOld, yTrainOld, yTestOld = getTrainingsDataParameterFile(maxVorticitiesFileOld, pathToParameterFile)

evaluateMeanPredictor(yTrain, yTest)
#evaluateMeanPredictor(yTrainOld, yTest)