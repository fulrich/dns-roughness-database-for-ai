import pandas as pd
from sklearn import metrics


def analyseDifferenceBetweenLinearAndParabola(pathToMaxVorticityFile):
    maxVorticitiesDf = pd.read_csv(pathToMaxVorticityFile, skipinitialspace=True)

    maxVorticityParabola = maxVorticitiesDf["maxVorticityParabola"]
    maxVorticityLinear = maxVorticitiesDf["maxVorticityLinear"]

    #maxVorticityParabola = [x for x in maxVorticityParabola if x > 150000]

    mean_absolute_error = metrics.mean_absolute_error(maxVorticityParabola, maxVorticityLinear)
    maxError = metrics.max_error(maxVorticityParabola, maxVorticityLinear)
    meanPercentageError = metrics.mean_absolute_percentage_error(maxVorticityParabola, maxVorticityLinear)

    #print("maxValue Linear: " + str(max(maxVorticitiesDf["maxVorticityLinear"])))

    print("Mean absolute error: " + str(mean_absolute_error))
    print("Max Error: " + str(maxError))
    print("Mean percentage Error: " + str(meanPercentageError))

    highDeviationList = []
    for i, row in maxVorticitiesDf.iterrows():
        #print(row["maxVorticityParabola"])
        #print(row["maxVorticityLinear"])
        #print(row["simulationName"])
        deviation = abs(row["maxVorticityLinear"] - row["maxVorticityParabola"])
        #print("Deviation: " + str(deviation))

        if(deviation > 15000):
            highDeviationList.append((row["simulationName"], deviation))
    #print(highDeviationList)
    highDeviationList.sort(key=lambda x: x[1], reverse=True)
    print("SimulationName, Abweichung")
    for element in highDeviationList:
        print(str(element[0]) + "," + str(element[1]))
    #print(highDeviationList)
    print(len(highDeviationList))

pathToMaxVorticityFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/RoughnessDatabases/1_6_0_All.csv"
analyseDifferenceBetweenLinearAndParabola(pathToMaxVorticityFile)