import pandas as pd
import tensorflow as tf
import numpy as np
import keras_tuner as kt
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
import os

os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
domainResolution = 64


def createTunerModel(hp):
    # normLayer = Normalization()
    # normLayer.adapt(xTrain)
    model = models.Sequential()
    # model.add(normLayer)
    numFilters1 = hp.Int('numFilters1', min_value=5, max_value=50, step=5)
    model.add(layers.Conv2D(numFilters1, (3, 3), activation='relu', input_shape=(domainResolution, domainResolution, 1)))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPooling2D((2, 2)))
    # model.add(layers.Dropout(0.4))
    numFilters2 = hp.Int('numFilters2', min_value=5, max_value=50, step=5)
    model.add(layers.Conv2D(numFilters2, (3, 3), activation='relu'))
    # model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.BatchNormalization())
    # model.add(layers.Dropout(0.4))
    numFilters3 = hp.Int('numFilters3', min_value=5, max_value=50, step=5)
    model.add(layers.Conv2D(3, (3, 3), activation='relu'))
    model.add(layers.BatchNormalization())
    model.add(layers.Flatten())
    neuronsFirstHiddenLayer = hp.Choice('neuronsHiddenLayer1', [100, 200, 500, 1000, 2000, 4000])
    model.add(layers.Dense(neuronsFirstHiddenLayer, activation='relu'))
    neuronsFirstHiddenLayer2 = hp.Choice('neuronsHiddenLayer2', [100, 200, 500, 1000, 2000, 4000])
    model.add(layers.Dense(neuronsFirstHiddenLayer2, activation='relu'))
    # model.add(layers.Dropout(0.4))
    model.add(layers.Dense(1))
    #model.summary()
    hp_learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4, 1e-5, 1e-6])
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=hp_learning_rate),
                  loss=tf.keras.losses.MeanSquaredLogarithmicError(),
                  metrics=[tf.keras.metrics.MeanAbsolutePercentageError()])
    return model


tuner = kt.Hyperband(
    createTunerModel,
    objective='val_mean_absolute_error',
    max_epochs=10,
    project_name='tunerProject',
    directory='my_dir',
    overwrite=True
)

xTraining = np.load("../Databases/NumpyDatabases/x_64_64.npy")
yTraining = np.load("../Databases/NumpyDatabases/y_64_64.npy")

splitIndex = int(0.8 * xTraining.shape[0])
print(splitIndex)
xTest = xTraining[splitIndex:]
xTraining = xTraining[:splitIndex]
yTest = yTraining[splitIndex:]
yTraining = yTraining[:splitIndex]

print(xTraining.shape)
print(yTraining.shape)
print(xTest.shape)
print(yTest.shape)

stop_early = tf.keras.callbacks.EarlyStopping(monitor='val_mean_absolute_error', patience=2)

tuner.search_space_summary()
#Mit diesem Befehl kann man Tensorboard angeben wo sich die tensorboard Logs befinden
#tensorboard --logdir="/home/thomas/Dokumente/HiWi/vorticity_prediction/KerasTuner/tensorboard_logs" --port=6006

tuner.search(xTraining, yTraining, epochs=10, validation_split=0.2, callbacks=[stop_early, tf.keras.callbacks.TensorBoard("tensorboard_logs")])
best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]
print("result summary")
tuner.results_summary()
#print(f"""
#The hyperparameter search is complete. The optimal number of units in the first densely-connected
#layer is {best_hps.get('units')} and the optimal learning rate for the optimizer
#is {best_hps.get('learning_rate')}.
#""")
