import tensorflow as tf

import matplotlib.pyplot as plt


def plotModelPerformanceInPercent(yTest, predictions):
    yScale = 200000
    plt.scatter(yTest*10000,predictions*10000, label='Data')
    plt.xlabel('True Values max. Vorticity')
    plt.ylabel('Predictions max. Vorticity')
    lims = [0, yScale]
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims,color='g')
    x = tf.linspace(0, yScale, 351)
    y = x+x*0.5
    x2 = tf.linspace(0, yScale, 351)
    y2 = x-x*0.5
    plt.plot(x, y,color='k', label='+ 50%')
    plt.plot(x2,y2,color='grey', label='- 50%')
    x3 = tf.linspace(0, yScale, 351)
    y3 = x+x*0.3
    x4 = tf.linspace(0, yScale, 351)
    y4 = x-x*0.3
    plt.plot(x3, y3,color='red', label='+ 30%')
    plt.plot(x4,y4,color='m', label='- 30%')
    plt.legend()
    plt.savefig("FritzPlot.png", dpi=400)
    plt.show()

def plotTrainingHistory(history):
    plt.plot(history.history['mean_absolute_error'])
    plt.plot(history.history['val_mean_absolute_error'])
    #plt.title('average mean error')
    plt.ylabel('average mean error')
    plt.xlabel('Epochs')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_MAF.png", dpi=400)
    plt.show()

    plt.plot(history.history['mean_absolute_percentage_error'])
    plt.plot(history.history['val_mean_absolute_percentage_error'])
    #plt.title('average percentage error')
    plt.ylabel('average percentage error')
    plt.xlabel('Epochs')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_MPF.png", dpi=400)
    plt.show()

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    #plt.title('Loss')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_Loss.png", dpi=400)
    plt.show()

def plotTrainingHistory2(history):
    plt.plot(history.history['val_MAE'])
    plt.title('average mean error')
    plt.ylabel('average mean error')
    plt.xlabel('Epochs')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_MAF.png", dpi=400)
    plt.show()

    plt.plot(history.history['val_MPE'])
    #plt.title('average percentage error')
    plt.ylabel('average percentage error')
    plt.xlabel('Epochs')
    plt.legend(['Training', 'Validierung'], loc='upper left')
    plt.savefig("CNN_MPF.png", dpi=400)
    plt.show()

def plot_Errors3(test_predictions,yTest):
    yScale = 200000
    plt.scatter(yTest,test_predictions, label='Data')
    plt.xlabel('True Values max. Vorticity')
    plt.ylabel('Predictions max. Vorticity')
    lims = [0, yScale]
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims,color='g')
    x = tf.linspace(0, yScale, 351)
    y = x+x*0.5
    x2 = tf.linspace(0, yScale, 351)
    y2 = x-x*0.5
    plt.plot(x, y,color='k', label='+ 50%')
    plt.plot(x2,y2,color='grey', label='- 50%')
    x3 = tf.linspace(0, yScale, 351)
    y3 = x+x*0.3
    x4 = tf.linspace(0, yScale, 351)
    y4 = x-x*0.3
    plt.plot(x3, y3,color='red', label='+ 30%')
    plt.plot(x4,y4,color='m', label='- 30%')
    plt.legend()
    plt.savefig("FritzPlot.png", dpi=400)
    plt.show()