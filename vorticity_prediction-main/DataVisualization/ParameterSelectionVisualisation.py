from LearnFromParameters.ForwardSelcection import readDict
import matplotlib.pyplot as plt


def readOneDictFromWorker(workerIndex, pathWhereDictsAreStored):
    improvements = readDict(pathWhereDictsAreStored + "ImprovementsDict_" + str(workerIndex))
    omittedParameters = readDict(pathWhereDictsAreStored + "OmittedParametersDict_" + str(workerIndex))
    return improvements, omittedParameters

def extractSpecificErrorFromResultDict(errorName, resultDict):
    errors = []
    parameterNames = []
    for key in resultDict.keys():
        parameterNames.append(key)
        errors.append(resultDict[key][errorName])

    return dict(zip(parameterNames, errors))


def concatResultsOfWorkers(numberWorkers, pathWhereWorkersAreStored):
    improvementsDictList = []
    omittedParametersDictList = []

    for i in range(numberWorkers):
        improvementDict, omittedParameters = readOneDictFromWorker(i, pathWhereWorkersAreStored)
        mape = extractSpecificErrorFromResultDict("mean_absolute_percentage_error", improvementDict)
        improvementsDictList.append(mape)

        omittedParametersDictList.append(omittedParameters)


    averageImprovements = getSumOfDict(improvementsDictList)
    sumOmittedParameters = getSumOfDict(omittedParametersDictList)

    return averageImprovements, sumOmittedParameters

def getSumOfDict(dict_list):
    mean_dict = {}
    for key in dict_list[0].keys():
        mean_dict[key] = sum(d[key] for d in dict_list)
    return mean_dict

def getSumOfDict(dict_list):
    sumDict = {}
    for key in dict_list[0].keys():
        sumDict[key] = sum(d[key] for d in dict_list)
    return sumDict


def plotDict(dictionary, ylabel):
    #data = {'C': 20, 'C++': 15, 'Java': 30,
    #        'Python': 35}
    #xPosOfMinimumAfterMaximumInAreaBehind, yPosOfMinimumAfterMaximumInAreaBehind

    dictionary["xPosMinAfterMaxInAreaBehind"] = dictionary.pop("xPosOfMinimumAfterMaximumInAreaBehind")
    dictionary["yPosMinAfterMaxInAreaBehind"] = dictionary.pop("yPosOfMinimumAfterMaximumInAreaBehind")
    dictionary["minAfterMaxInAreaBehind"] = dictionary.pop("minimumAfterMaximumInAreaBehind")
    dictionary["maxSlopeAfterMinAfterMax"] = dictionary.pop("maxSlopeAfterMinimumAfterMaximum")

    dictionary = dict(sorted(dictionary.items(), key=lambda item: item[1], reverse=True))
    badParameters = {k: v for k, v in dictionary.items() if v > 1000}
    print(badParameters.keys())
    parameters = list(dictionary.keys())
    values = list(dictionary.values())
    #values = list(map(lambda x: x/20, values))
    print(sum(values))

    #fig = plt.figure(figsize=(10, 5))

    # creating the bar plot
    plt.figure(figsize=(4,7), dpi=400)
    plt.rc('ytick', labelsize=4)
    plt.rc('xtick', labelsize=6)
    plt.rc('axes', labelsize=6)

    #plt.xticks(rotation='vertical')
    plt.xlabel(ylabel)

    #plt.ylabel("Parameter")
    plt.gcf().subplots_adjust(left=0.4)
    plt.barh(parameters, values)
    plt.savefig("Durchschnittliche_Aenderung_des_MPF.png", dpi=400)
    #plt.savefig("Anzahl_Verschlechterungen.png", dpi=400)
    plt.show()


numberWorkers = 6
pathWhereWorkersAreStored = "/home/thomas/Downloads/ParameterAnalysis/"
averageImprovements, sumOmittedParameters = concatResultsOfWorkers(numberWorkers, pathWhereWorkersAreStored)
#plotDict(averageImprovements, "Durchschnittliche Änderung des MPF [%]")
plotDict(sumOmittedParameters, "Anzahl Verschlechterungen [-]")