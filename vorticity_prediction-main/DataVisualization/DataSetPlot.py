import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from pandas.plotting import table
import math

from PIL import Image
from sklearn.preprocessing import MinMaxScaler

from DatabaseGeneration.DatabaseGenerator import createMergedDataBase


def histPlotMaxVorticitity():
    maxVorticitiesFile = "/home/fulrich/Promotion/CEAS-Journal/vorticity_prediction-main/CNN_Training_Fritz/1_6_0_All.csv"

    maxVorticitiesDf = pd.read_csv(maxVorticitiesFile, skipinitialspace=True)
    described = maxVorticitiesDf["maxVorticityParabola"].describe()
    print(described)


    maxVorticitiesList = list(maxVorticitiesDf["maxVorticityParabola"])
    highVorticities = list(filter(lambda x: x > 200000, maxVorticitiesList))
    print(highVorticities)
    print("Anzahl vorticities: " + str(len(highVorticities)))

    array = np.array(maxVorticitiesList)
    hist, bin_edges = np.histogram(array, bins=150)
    #plt.title("Histogramm der maximalen Wirbelstärke für alle Simulationen")
    plt.xlabel(r"Maximale Wirbelstärke $[\frac{1}{s}]$ ")
    plt.ylabel("Anzahl Simulationen [-]")
    #print(hist)
    #print(bin_edges)
    plt.hist(array, bins=200)
    plt.savefig("Histogramm_Datenbank.png", dpi=400)

def plotParameters(maxVorticitiesFile,pathToParameterFile):
    maxVorticitiesDf = pd.read_csv(maxVorticitiesFile, skipinitialspace=True)
    usedSimulations = maxVorticitiesDf["simulationName"].to_frame()
    parameters = pd.read_csv(pathToParameterFile, skipinitialspace=True)
    mergedDf = usedSimulations.merge(parameters, how='left', on='simulationName')
    parameters = mergedDf.drop(columns="simulationName")

    scaler = MinMaxScaler()
    xScaled = pd.DataFrame(
        scaler.fit_transform(parameters),
        columns=parameters.columns)

    #ax = xScaled.plot.hist(bins=100)
    fig, axes = plt.subplots(math.ceil(len(xScaled.columns) / 4), 4, figsize=(20, 50), squeeze=False)

    numColumns = len(xScaled.columns)

    i = 0
    for triaxis in axes:
        for axis in triaxis:
            xScaled.hist(column=xScaled.columns[i], bins=150, ax=axis, figsize=(10,20))
            i = i + 1
            if(numColumns == i):
                break

    print("Number parameters: " + str(len(xScaled.columns)))
    print("Number simulations: " + str(xScaled.shape[0]))
    plt.savefig("Histogramm_Parameter.png", dpi=400)


def saveRoughnessXdataToImage(xData, nameOfImage):
    xData = np.reshape(xData, xData.shape[:2])
    xDataTransformedToImage = ((xData - np.min(xData)) / (np.max(xData) - np.min(xData))) * 255
    img = Image.fromarray(np.uint8(xDataTransformedToImage), 'L')
    imgRescaled = img.resize(size=(512, 512), resample=Image.NEAREST)
    imgRescaled.save(nameOfImage + '.png')
    # imgRescaled.show()
    # plt.show()




#pathToParameterFile = "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/AllParameters.csv"
#plotParameters(maxVorticitiesFile, pathToParameterFile)
#histPlotMaxVorticitity()