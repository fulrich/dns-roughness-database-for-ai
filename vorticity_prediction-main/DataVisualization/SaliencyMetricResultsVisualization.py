import pickle
import seaborn as sns

import matplotlib.pyplot as plt

def plotResutsOfDifferentMetrics(resultDict):
    dictWhichShouldBePlotted = {}
    for key in resultDict:
        if key != "original_score":
            value = (resultDict[key]["MPE"] - resultDict["original_score"]["MPE"]) * 100
            dictWhichShouldBePlotted.update({key : value})

    keys = list(dictWhichShouldBePlotted.keys())
    # get values in the same order as keys, and parse percentage values
    plt.figure(figsize=(6, 10))
    plt.title("gradCAM", fontsize=20)
    vals = [dictWhichShouldBePlotted[k] for k in keys]
    ax = sns.barplot(x=keys, y=vals)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha="right")
    plt.savefig("../Output/metrics_grad_cam.png")
    plt.show()


with open('../Output/resultDict_grad_cam.pkl', 'rb') as f:
    resultDict = pickle.load(f)
plotResutsOfDifferentMetrics(resultDict)
