import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import csv

def readOneTimeSeriesFileAnalysisFile(pathToFile):
    dataSet = pd.read_csv(pathToFile, skipinitialspace=True)
    print(dataSet)
    column = dataSet.iloc[:, 0].tolist()
    print(column)
    groundTruth = float(column[-1])
    myList = [1,2,2,3]
    #percentageOfDeviation =column
    #percentageOfDeviation = list(map(lambda x: float(x), column))
    percentageOfDeviation = list(map(lambda x: 100* abs(groundTruth - float(x))/groundTruth,column))
    # / groundTruth
    print(percentageOfDeviation[0])
    print(percentageOfDeviation)
    #xCoordinates = readConvergenceDatFile("/home/thomas/Downloads/Convergence.txt")
    return percentageOfDeviation[:19]

def readConvergenceDatFile(pathToFile):
    with open(pathToFile, 'r+') as f:
        lines = f.read().splitlines()
        print(lines)
    result = []
    for line in lines[1:]:
        line = line.split()
        result.append((int(line[0]), float(line[2])))

    #only get specific strings
    result = list(filter(lambda x: x[0] % 3000 == 0 or x[0] == 1, result))
    result = [x[1] for x in result]
    return result[:19]

def getConvergenceInformationAtSpecificTimeStep(pathToFile, timeStep):
    with open(pathToFile, 'r+') as f:
        lines = f.read().splitlines()
        print(lines)
    result = []
    for line in lines[1:]:
        line = line.split()
        result.append((int(line[0]), line[2]))

    #only get specific strings
    result = list(filter(lambda x: x[0] == timeStep, result))
    result = [x[1] for x in result]
    return result[0]

def plotOneLineDeviationInPercentage(index, resultList):
    y = readOneTimeSeriesFileAnalysisFile("/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/TimeSeriesAnalysis/Vorticity/TimerSeries2_"+str(index+14500)+".csv")
    y = y[:9]
    x = ["0","3000","6000","9000","12000","15000","18000","21000","24000"]
    sns.lineplot(x, y)
    resultList.append(y)

def plotOneLineL2Norm(index, resultList):
    y = readConvergenceDatFile("/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/TimeSeriesAnalysis/ConvergenceFiles/convergence"+ str(index))
    y = y[:9]
    x = ["0","3000","6000","9000","12000","15000","18000","21000","24000"]
    sns.lineplot(x, y)
    resultList.append(y)


def plotDiviationInPercentage():
    resultList = []
    for i in range(10):
        plotOneLineDeviationInPercentage(i + 1, resultList)
    array = np.asarray(resultList)
    array = np.transpose(array)
    df = pd.DataFrame(data=array)
    df = df.set_axis(["Simulation 1", "Simulation 2", "Simulation 3", "Simulation 4", "Simulation 5",
                      "Simulation 6", "Simulation 7", "Simulation 8", "Simulation 9", "Simulation 10"], axis=1,
                     inplace=False)

    sns.set_style('whitegrid')
    g_results = sns.lineplot(data=df, dashes=0, markers=['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'])
    g_results.set(yscale='log')
    plt.xlabel("Simulationsschritte")
    plt.ylabel("Prozentuale Abweichung")
    plt.title("Prozentuale Abweichung in Abhängigkeit der Simulationsschritte")
    plt.savefig(
        "/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/TimeSeriesAnalysis/Plots/Prozentuale_Abweichung_Plot.png",
        dpi=300)
    plt.show()

def plotL2Norm():
    resultList = []
    for i in range(10):
        plotOneLineL2Norm(i+1, resultList)
    array = np.asarray(resultList)
    array = np.transpose(array)
    df = pd.DataFrame(data=array)
    df = df.set_axis(["Simulation 1", "Simulation 2", "Simulation 3", "Simulation 4", "Simulation 5",
                      "Simulation 6", "Simulation 7", "Simulation 8", "Simulation 9", "Simulation 10"], axis=1,
                     inplace=False)

    sns.set_style('whitegrid')
    g_results = sns.lineplot(data=df, dashes=0, markers=['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'])
    g_results.set(yscale='log')
    plt.xlabel("Simulationsschritte")
    plt.ylabel("ε")
    plt.title("ε in Abhängigkeit der Simulationsschritte")
    plt.savefig("/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten_Bachelorarbeit/TimeSeriesAnalysis/Plots/Epsilon_Plot.png", dpi=300)
    plt.show()
    plt.show()


def writeToCSVfile():
    maxVorticityList = []
    convergenceList = []
    for i in range(10):
        plotOneLineDeviationInPercentage(i+1, maxVorticityList)
        plotOneLineL2Norm(i+1, convergenceList)

    concatenatedList = maxVorticityList + convergenceList
    array = np.asarray(concatenatedList)
    array = np.transpose(array)
    print(array.shape)
    df = pd.DataFrame(data=array)
    df = df.set_axis(["maxVorticity_Simulation_1", "maxVorticity_Simulation_2", "maxVorticity_Simulation_3", "maxVorticity_Simulation_4", "maxVorticity_Simulation_5",
                      "maxVorticity_Simulation_6", "maxVorticity_Simulation_7", "maxVorticity_Simulation_8", "maxVorticity_Simulation_9", "maxVorticity_Simulation_10",
                      "L2_Simulation_1", "L2_Simulation_2", "L2_Simulation_3",
                      "L2_Simulation_4", "L2_Simulation_5",
                      "L2_Simulation_6", "L2_Simulation_7", "L2_Simulation_8",
                      "L2_Simulation_9", "L2_Simulation_10"],
                     axis=1, inplace=False)
    df.to_csv("/home/thomas/Dokumente/Studium/Semester_6/Bachelorarbeit/Daten/TimeSeriesAnalysis/CSV_data.csv")

#writeToCSVfile()
plotDiviationInPercentage()
plotL2Norm()
#result = readConvergenceDatFile("/home/thomas/Downloads/Convergence.txt")
#print(result)
#readOneTimeSeriesFileAnalysisFile("/home/thomas/Downloads/TimeSeriesAnalysis/TimerSeries2_14501.csv")