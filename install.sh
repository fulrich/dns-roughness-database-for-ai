cd vorticity_prediction-main
python3 -m venv env
source env/bin/activate
pip install pandas
pip install tensorflow
pip install matplotlib
pip install seaborn
pip install sklearn
pip install scikit-learn
pip install statsmodels
