# DNS Roughness Database for AI

Understanding the laminar-turbulent transition is crucial in designing re-entry vehicles. This repository focuses on the impact of random distributed roughness patches, forming cross-flow-like vortices that amplify unstable acoustic modes, ultimately leading to transition. A key metric is the magnitude of streamwise vorticity behind the roughness patch.

## Overview

This repository houses a comprehensive database of Direct Numerical Simulations (DNS) aimed at predicting the streamwise vorticity magnitude induced by roughness patches. Additionally, it implements two machine-learning methods: a feed-forward Deep Neural Network (DNN) and a Convolutional Neural Network (CNN).

## Getting started

Clone this repository and set up a Python environment:   

    python3 -m venv env
    source env/bin/activate


Install the required packages:


    pip install pandas  
    pip install tensorflow  
    pip install matplotlib  
    pip install seaborn  
    pip install sklearn  
    pip install scikit-learn  
    pip install statsmodels


Please note that upgrading your pip to a more recent version might be necessary:  

    pip install --upgrade pip

A GPU unit speeds up the training process significantly and is recomended for
an extensive hyper parameter study. For an initial test CPU is fine.

## Project Structure

The project is organized in the following structure:

* **vorticity_prediction-main**: Main project folder.
    * **Databases**: contains all relevant database files. Here the maximum streamwise vorticity information is stored, the roughness patch height parameters (for the CNN) and the statistical / geometrical roughness parameter (for the DNN)
    * **LearnFromParameters**: All relevant files for the DNN training, validation and testing
    * **LearnFromHeights**: All relevant files for the CNN training, validation and testing
    * **DatabaseGeneration**: auxiliary routines for database generation
    * **KerasTuner**: auxiliary routines for the hyperparameter study
    * **GlobalCode**: contains information about the local system path of the project
    * **DataVisualization**: auxiliary routines for DataVisualization  
    * **savedModels**: pretrained models





### DNN

First, move to LearnFromParameters folder 

    cp LearnFromParameters


#### DNN Training

The training of the DNN is performed in the file **KerasNN.py** .

Before the training process the correct paths for the training/test/validation data need to specified.

The maxVorticitiesFile contains all the maximum vorticity values (output values)  

    maxVorticitiesFile = "../Databases/Vorticities.csv"

The training is started with:


    python3.9 KerasNN.py


At the end of the training, metrics such as mean absolute error (MAE), maximum error (MAXE), and mean prediction error (MPE) will be displayed.



#### Databases

There are several different options for the parameter database for the DNN:

* AllParameters-combined.csv : 

* AllParameters.csv : This database contains ALL parameters, we have ever tested. (More parameters than in the preprint).

Then set up the correct absolute path for the databases used.



#### CNN

First, move to **LearnFromHeights** folder  

    cp LearnFromHeights

For training start:

    python3.9 ModelTraining.py

The **ModelTraining.py** routine trains a CNN and saves the model in the folder **savedModels**. The user can specify the name of the saved model (**experimentName**) as well as the location of the saved model (**pathWhereModelsShouldBeSaved**). Once the model is trained its prediction capability is tested. In a second run, one can also load a pretrained model with  

    model = tf.keras.models.load_model("../savedModels/32_32_model_std/" )

At the end of **ModelTraining.py** (Line 425) the user can adapt the described settings:


    experimentName = "cnn_test"
    pathWhereModelsShouldBeSaved = "../savedModels/"
    model = createConvModelBest(lrRate=0.00025)
    targetScaler, xTrain, xTest, yTrain, yTest = \
    getTrainingsDataHeightsDatabase(pathToMaxVorticityFile, pathToHeightsFile, domainResolution,
                              regressionParameter="maxVorticityParabola", inputNormalization="std",
                              outputNormalization="std")

    model = trainModel(experimentName + "_model", model, xTrain, yTrain, targetScaler=targetScaler,
               pathToModel=pathWhereModelsShouldBeSaved)
    # Load already trained model
    #model = tf.keras.models.load_model("../savedModels/32_32_model_std/" )
    #plotModelPerformanceInPercent(yTest, predictions)
    predictions = model.predict(xTest)
    predictions = targetScaler.inverse_transform(predictions)
    evaluatePredictions(yTest, predictions)
    #print( targetScaler.inverse_transform(predictions))
    plot_Errors3(predictions, yTest)

#### Changes in the CNN model

To test a different CNN setup, follow these steps:

* Open the file **UsedCNNs.py** in your preferred code editor.

* Here, you can modify parameters such as the number of layers, filter sizes, and activation functions.

If you want to change from a 32x32 input tile to a 16x16 tile, follow these steps:

* Open the file **UsedCNNs.py** in your preferred code editor.

* Adjust the **domainResolution** variable to your desired dimensions, currently 16x16, 32x32 and 64x64 are supported.

Line 10-11,**UsedCNNs.py**:

    # The resolution of the quadratic grid
    domainResolution = 32

Further, select in **ModelTraining.py** the correct heights file: 

    pathToHeightsFile = "../Databases/Heights_32_32.csv"

**The domain resolution also needs to bechanged in the Modeltraining.py file, Line 28:**

    domainResolution = 32


## Authors and acknowledgment
Friedrich Ulrich, <fritz.ulrich@tum.de>  
Thomas Sedlmeyr  
Christian Stemmer  

## License
This work and the data is licensed under Attribution-NonCommercial-ShareAlike 4.0 International.

